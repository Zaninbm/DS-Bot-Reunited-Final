﻿using System.IO;
using System.Net;
using System.Threading;

namespace Ds_Bot_Reunited_NEWUI.TwoCaptcha_API {
    public class TwoCaptchaClient {
        public TwoCaptchaClient(string apiKey) {
            ApiKey = apiKey;
        }

        public string ApiKey { get; }

        public bool SolveRecaptchaV2(string googleKey, string pageUrl, out string result) {
            var requestUrl = "http://2captcha.com/in.php?key=" + ApiKey + "&method=userrecaptcha&googlekey=" + googleKey + "&pageurl=" + pageUrl;

            try {
                var req = WebRequest.Create(requestUrl);

                using (var resp = req.GetResponse())
                using (var read = new StreamReader(resp.GetResponseStream())) {
                    var response = read.ReadToEnd();

                    if (response.Length < 3) {
                        result = response;
                        return false;
                    }
                    if (response.Substring(0, 3) == "OK|") {
                        var captchaId = response.Remove(0, 3);

                        for (var i = 0; i < 24; i++) {
                            var getAnswer = WebRequest.Create("http://2captcha.com/res.php?key=" + ApiKey + "&action=get&id=" + captchaId);

                            using (var answerResp = getAnswer.GetResponse())
                            using (var answerStream = new StreamReader(answerResp.GetResponseStream())) {
                                var answerResponse = answerStream.ReadToEnd();

                                if (answerResponse.Length < 3) {
                                    result = answerResponse;
                                    return false;
                                }
                                if (answerResponse.Substring(0, 3) == "OK|") {
                                    result = answerResponse.Remove(0, 3);
                                    return true;
                                }
                                if (answerResponse != "CAPCHA_NOT_READY") {
                                    result = answerResponse;
                                    return false;
                                }
                            }

                            Thread.Sleep(10000);
                        }

                        result = "Timeout";
                        return false;
                    }
                    result = response;
                    return false;
                }
            } catch {
                // ignored
            }

            result = "Unknown error";
            return false;
        }
    }
}
