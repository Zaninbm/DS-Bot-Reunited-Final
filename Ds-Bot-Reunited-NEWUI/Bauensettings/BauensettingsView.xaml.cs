﻿using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.BauensettingsDorf;

namespace Ds_Bot_Reunited_NEWUI.Bauensettings {
    public partial class BauensettingsView {
        public BauensettingsView(Window owner, BauensettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGridSelectionChanged(object sender, SelectionChangedEventArgs e) {
            ((BauensettingsViewModel) DataContext)?.SelectedVillages?.Clear();
            foreach (var item in DataGrid.SelectedItems)
                ((BauensettingsViewModel) DataContext)?.SelectedVillages.Add((BauensettingsDorfViewModel) item);
        }
    }
}
