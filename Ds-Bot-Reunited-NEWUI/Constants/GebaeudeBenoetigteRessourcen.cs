﻿using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.Constants {
    public static class GebaeudeBenoetigteRessourcen {
        public static Beute Get(Gebaeude gebaeude, int targetlevel) {
            switch (gebaeude) {
                case Gebaeude.Hauptgebaeude:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(90, 80, 70);
                        case 2:
                            return new Beute(113, 102, 88);
                        case 3:
                            return new Beute(143, 130, 111);
                        case 4:
                            return new Beute(180, 166, 140);
                        case 5:
                            return new Beute(227, 211, 176);
                        case 6:
                            return new Beute(286, 270, 222);
                        case 7:
                            return new Beute(360, 344, 280);
                        case 8:
                            return new Beute(454, 438, 353);
                        case 9:
                            return new Beute(572, 559, 445);
                        case 10:
                            return new Beute(720, 712, 560);
                        case 11:
                            return new Beute(908, 908, 706);
                        case 12:
                            return new Beute(1144, 1158, 890);
                        case 13:
                            return new Beute(1441, 1476, 1121);
                        case 14:
                            return new Beute(1816, 1882, 1412);
                        case 15:
                            return new Beute(2288, 2400, 1779);
                        case 16:
                            return new Beute(2883, 3060, 2242);
                        case 17:
                            return new Beute(3632, 3902, 2825);
                        case 18:
                            return new Beute(4577, 4975, 3560);
                        case 19:
                            return new Beute(5767, 6343, 4485);
                        case 20:
                            return new Beute(7266, 8087, 5651);
                        case 21:
                            return new Beute(9155, 10311, 7120);
                        case 22:
                            return new Beute(11535, 13146, 8972);
                        case 23:
                            return new Beute(14534, 16762, 11304);
                        case 24:
                            return new Beute(18313, 21371, 14244);
                        case 25:
                            return new Beute(23075, 27248, 17947);
                        case 26:
                            return new Beute(29074, 34741, 22613);
                        case 27:
                            return new Beute(36633, 44295, 28493);
                        case 28:
                            return new Beute(46158, 56476, 35901);
                        case 29:
                            return new Beute(58159, 72007, 45235);
                        case 30:
                            return new Beute(73280, 91809, 56996);
                        default:
                            return new Beute();
                    }
                case Gebaeude.Kaserne:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(200, 170, 90);
                        case 2:
                            return new Beute(252, 218, 113);
                        case 3:
                            return new Beute(318, 279, 143);
                        case 4:
                            return new Beute(400, 357, 180);
                        case 5:
                            return new Beute(504, 456, 227);
                        case 6:
                            return new Beute(635, 584, 286);
                        case 7:
                            return new Beute(800, 748, 360);
                        case 8:
                            return new Beute(1008, 957, 454);
                        case 9:
                            return new Beute(1271, 1225, 572);
                        case 10:
                            return new Beute(1601, 1568, 720);
                        case 11:
                            return new Beute(2017, 2007, 908);
                        case 12:
                            return new Beute(2542, 2569, 1144);
                        case 13:
                            return new Beute(3202, 3288, 1441);
                        case 14:
                            return new Beute(4035, 4209, 1816);
                        case 15:
                            return new Beute(5084, 5388, 2288);
                        case 16:
                            return new Beute(6406, 6896, 2883);
                        case 17:
                            return new Beute(8072, 8827, 3632);
                        case 18:
                            return new Beute(10170, 11298, 4577);
                        case 19:
                            return new Beute(12814, 14462, 5767);
                        case 20:
                            return new Beute(16146, 18511, 7266);
                        case 21:
                            return new Beute(20344, 23695, 9155);
                        case 22:
                            return new Beute(25634, 30329, 11535);
                        case 23:
                            return new Beute(32298, 38821, 14534);
                        case 24:
                            return new Beute(51277, 63605, 23075);
                        case 25:
                            return new Beute(40696, 49691, 18313);
                        default:
                            return new Beute();
                    }
                case Gebaeude.Stall:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(270, 240, 260);
                        case 2:
                            return new Beute(340, 307, 328);
                        case 3:
                            return new Beute(429, 393, 413);
                        case 4:
                            return new Beute(540, 503, 520);
                        case 5:
                            return new Beute(681, 644, 655);
                        case 6:
                            return new Beute(857, 825, 826);
                        case 7:
                            return new Beute(1080, 1056, 1040);
                        case 8:
                            return new Beute(1361, 1351, 1311);
                        case 9:
                            return new Beute(1715, 1729, 1652);
                        case 10:
                            return new Beute(2161, 2214, 2081);
                        case 11:
                            return new Beute(2723, 2833, 2622);
                        case 12:
                            return new Beute(3431, 3627, 3304);
                        case 13:
                            return new Beute(4323, 4642, 4163);
                        case 14:
                            return new Beute(5447, 5942, 5246);
                        case 15:
                            return new Beute(6864, 7606, 6609);
                        case 16:
                            return new Beute(8648, 9736, 8328);
                        case 17:
                            return new Beute(10897, 12462, 10493);
                        case 18:
                            return new Beute(13730, 15951, 13221);
                        case 19:
                            return new Beute(17300, 20417, 16659);
                        case 20:
                            return new Beute(21797, 26134, 20990);

                        default:
                            return new Beute();
                    }
                case Gebaeude.Werkstatt:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(300, 240, 260);
                        case 2:
                            return new Beute(378, 307, 328);
                        case 3:
                            return new Beute(476, 393, 413);
                        case 4:
                            return new Beute(600, 503, 520);
                        case 5:
                            return new Beute(756, 644, 655);
                        case 6:
                            return new Beute(953, 825, 826);
                        case 7:
                            return new Beute(1200, 1056, 1040);
                        case 8:
                            return new Beute(1513, 1351, 1311);
                        case 9:
                            return new Beute(1906, 1729, 1652);
                        case 10:
                            return new Beute(2401, 2214, 2081);
                        case 11:
                            return new Beute(3026, 2833, 2622);
                        case 12:
                            return new Beute(3812, 3627, 3304);
                        case 13:
                            return new Beute(4804, 4642, 4163);
                        case 14:
                            return new Beute(6053, 5942, 5246);
                        case 15:
                            return new Beute(7626, 7606, 6609);

                        default:
                            return new Beute();
                    }
                case Gebaeude.Kirche:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(16000, 20000, 5000);
                        case 2:
                            return new Beute(20160, 25600, 6300);
                        case 3:
                            return new Beute(25402, 32768, 7938);
                        default:
                            return new Beute();
                    }
                case Gebaeude.Erstekirche:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(160, 200, 50);
                        default:
                            return new Beute();
                    }
                case Gebaeude.WatchTower:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(12000, 14000, 10000);
                        case 2:
                            return new Beute(14040, 16380, 11800);
                        case 3:
                            return new Beute(16427, 19165, 13924);
                        case 4:
                            return new Beute(19219, 22423, 16430);
                        case 5:
                            return new Beute(22487, 26234, 19388);
                        case 6:
                            return new Beute(26309, 30694, 22878);
                        case 7:
                            return new Beute(30782, 35912, 26996);
                        case 8:
                            return new Beute(36015, 42017, 31855);
                        case 9:
                            return new Beute(42137, 49160, 37589);
                        case 10:
                            return new Beute(49301, 57518, 44355);
                        case 11:
                            return new Beute(57682, 67296, 52338);
                        case 12:
                            return new Beute(67488, 78736, 61759);
                        case 13:
                            return new Beute(78961, 92121, 72876);
                        case 14:
                            return new Beute(92384, 107782, 85994);
                        case 15:
                            return new Beute(108089, 126104, 101472);
                        case 16:
                            return new Beute(126465, 147542, 119737);
                        case 17:
                            return new Beute(147964, 172624, 141290);
                        case 18:
                            return new Beute(173117, 201970, 166722);
                        case 19:
                            return new Beute(202547, 236305, 196733);
                        case 20:
                            return new Beute(236981, 276477, 232144);
                        default:
                            return new Beute();
                    }
                case Gebaeude.Adelshof:
                    if (App.Settings.Weltensettings.AhTillLevel3) {
                        switch (targetlevel) {
                            case 1:
                                return new Beute(15000, 25000, 10000);
                            case 2:
                                return new Beute(30000, 50000, 20000);
                            case 3:
                                return new Beute(60000, 100000, 40000);
                        }
                    } else {
                        switch (targetlevel) {
                            case 1:
                                return new Beute(15000, 25000, 10000);
                            default:
                                return new Beute();
                        }
                    }
                    return new Beute();
                case Gebaeude.Schmiede:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(220, 180, 240);
                        case 2:
                            return new Beute(277, 230, 302);
                        case 3:
                            return new Beute(349, 293, 381);
                        case 4:
                            return new Beute(440, 373, 480);
                        case 5:
                            return new Beute(555, 476, 650);
                        case 6:
                            return new Beute(699, 606, 762);
                        case 7:
                            return new Beute(880, 773, 960);
                        case 8:
                            return new Beute(1109, 986, 1210);
                        case 9:
                            return new Beute(1398, 1257, 1525);
                        case 10:
                            return new Beute(1761, 1603, 1921);
                        case 11:
                            return new Beute(2219, 2043, 2421);
                        case 12:
                            return new Beute(2796, 2605, 3050);
                        case 13:
                            return new Beute(3523, 3322, 3843);
                        case 14:
                            return new Beute(4439, 4236, 4842);
                        case 15:
                            return new Beute(5593, 5400, 6101);
                        case 16:
                            return new Beute(7047, 6885, 7687);
                        case 17:
                            return new Beute(8879, 8779, 9686);
                        case 18:
                            return new Beute(11187, 11193, 12204);
                        case 19:
                            return new Beute(14096, 14271, 15377);
                        case 20:
                            return new Beute(17761, 18196, 19375);

                        default:
                            return new Beute();
                    }
                case Gebaeude.Versammlungsplatz:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(10, 40, 30);
                        default:
                            return new Beute();
                    }
                case Gebaeude.Statue:
                    if (App.Settings.Weltensettings.PaladinActive)
                        return new Beute(220, 220, 220);
                    return new Beute();
                case Gebaeude.Marktplatz:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(100, 100, 100);
                        case 2:
                            return new Beute(126, 128, 126);
                        case 3:
                            return new Beute(159, 163, 159);
                        case 4:
                            return new Beute(200, 207, 200);
                        case 5:
                            return new Beute(252, 264, 252);
                        case 6:
                            return new Beute(318, 337, 318);
                        case 7:
                            return new Beute(400, 430, 400);
                        case 8:
                            return new Beute(504, 548, 504);
                        case 9:
                            return new Beute(635, 698, 635);
                        case 10:
                            return new Beute(800, 890, 800);
                        case 11:
                            return new Beute(1009, 1135, 1009);
                        case 12:
                            return new Beute(1271, 1447, 1271);
                        case 13:
                            return new Beute(1601, 1846, 1601);
                        case 14:
                            return new Beute(2018, 2353, 2018);
                        case 15:
                            return new Beute(2542, 3000, 2542);
                        case 16:
                            return new Beute(3203, 3825, 3203);
                        case 17:
                            return new Beute(4036, 4877, 4036);
                        case 18:
                            return new Beute(5085, 6218, 5085);
                        case 19:
                            return new Beute(6407, 7928, 6407);
                        case 20:
                            return new Beute(8073, 10109, 8073);
                        case 21:
                            return new Beute(10172, 12889, 10172);
                        case 22:
                            return new Beute(12817, 16433, 12817);
                        case 23:
                            return new Beute(16149, 20952, 16149);
                        case 24:
                            return new Beute(20348, 26714, 20348);
                        case 25:
                            return new Beute(25639, 34060, 25639);

                        default:
                            return new Beute();
                    }
                case Gebaeude.Holzfaeller:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(50, 60, 40);
                        case 2:
                            return new Beute(63, 77, 50);
                        case 3:
                            return new Beute(78, 98, 62);
                        case 4:
                            return new Beute(98, 124, 77);
                        case 5:
                            return new Beute(122, 159, 96);
                        case 6:
                            return new Beute(153, 202, 120);
                        case 7:
                            return new Beute(191, 258, 149);
                        case 8:
                            return new Beute(238, 329, 185);
                        case 9:
                            return new Beute(298, 419, 231);
                        case 10:
                            return new Beute(373, 534, 287);
                        case 11:
                            return new Beute(466, 681, 358);
                        case 12:
                            return new Beute(582, 868, 446);
                        case 13:
                            return new Beute(728, 1107, 555);
                        case 14:
                            return new Beute(909, 1412, 691);
                        case 15:
                            return new Beute(1137, 1800, 860);
                        case 16:
                            return new Beute(1421, 2295, 1071);
                        case 17:
                            return new Beute(1776, 2926, 1333);
                        case 18:
                            return new Beute(2220, 3731, 1659);
                        case 19:
                            return new Beute(2776, 4757, 2066);
                        case 20:
                            return new Beute(3469, 6065, 2572);
                        case 21:
                            return new Beute(4337, 7733, 3202);
                        case 22:
                            return new Beute(5421, 9860, 3987);
                        case 23:
                            return new Beute(6776, 12571, 4963);
                        case 24:
                            return new Beute(8470, 16028, 6180);
                        case 25:
                            return new Beute(10588, 20436, 7694);
                        case 26:
                            return new Beute(13235, 26056, 9578);
                        case 27:
                            return new Beute(16544, 33221, 11925);
                        case 28:
                            return new Beute(20680, 42357, 14847);
                        case 29:
                            return new Beute(25849, 54005, 18484);
                        case 30:
                            return new Beute(32312, 68857, 23013);

                        default:
                            return new Beute();
                    }
                case Gebaeude.Lehmgrube:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(65, 50, 40);
                        case 2:
                            return new Beute(83, 63, 50);
                        case 3:
                            return new Beute(105, 80, 62);
                        case 4:
                            return new Beute(133, 101, 76);
                        case 5:
                            return new Beute(169, 128, 95);
                        case 6:
                            return new Beute(215, 162, 117);
                        case 7:
                            return new Beute(273, 205, 145);
                        case 8:
                            return new Beute(346, 259, 180);
                        case 9:
                            return new Beute(440, 328, 224);
                        case 10:
                            return new Beute(559, 415, 277);
                        case 11:
                            return new Beute(709, 525, 344);
                        case 12:
                            return new Beute(901, 664, 426);
                        case 13:
                            return new Beute(1144, 840, 529);
                        case 14:
                            return new Beute(1453, 1062, 655);
                        case 15:
                            return new Beute(1846, 1343, 813);
                        case 16:
                            return new Beute(2344, 1700, 1008);
                        case 17:
                            return new Beute(2977, 2150, 1250);
                        case 18:
                            return new Beute(3781, 2720, 1550);
                        case 19:
                            return new Beute(4802, 3440, 1922);
                        case 20:
                            return new Beute(6098, 4352, 2383);
                        case 21:
                            return new Beute(7744, 5505, 2955);
                        case 22:
                            return new Beute(9835, 6964, 3664);
                        case 23:
                            return new Beute(12491, 8810, 4543);
                        case 24:
                            return new Beute(15863, 11144, 5633);
                        case 25:
                            return new Beute(20147, 14098, 6985);
                        case 26:
                            return new Beute(25586, 17833, 8662);
                        case 27:
                            return new Beute(32495, 22559, 10740);
                        case 28:
                            return new Beute(41268, 28537, 13318);
                        case 29:
                            return new Beute(52410, 36100, 16515);
                        case 30:
                            return new Beute(66561, 45666, 20478);


                        default:
                            return new Beute();
                    }
                case Gebaeude.Eisenmine:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(75, 65, 70);
                        case 2:
                            return new Beute(94, 83, 87);
                        case 3:
                            return new Beute(118, 106, 108);
                        case 4:
                            return new Beute(147, 135, 133);
                        case 5:
                            return new Beute(184, 172, 165);
                        case 6:
                            return new Beute(231, 219, 205);
                        case 7:
                            return new Beute(289, 279, 254);
                        case 8:
                            return new Beute(362, 356, 316);
                        case 9:
                            return new Beute(453, 454, 391);
                        case 10:
                            return new Beute(567, 579, 485);
                        case 11:
                            return new Beute(710, 738, 602);
                        case 12:
                            return new Beute(889, 941, 746);
                        case 13:
                            return new Beute(1113, 1200, 925);
                        case 14:
                            return new Beute(1393, 1529, 1147);
                        case 15:
                            return new Beute(1744, 1950, 1422);
                        case 16:
                            return new Beute(2183, 2486, 1764);
                        case 17:
                            return new Beute(2734, 3170, 2187);
                        case 18:
                            return new Beute(3422, 4042, 2712);
                        case 19:
                            return new Beute(4285, 5153, 3363);
                        case 20:
                            return new Beute(5365, 6571, 4170);
                        case 21:
                            return new Beute(6717, 8378, 5170);
                        case 22:
                            return new Beute(8409, 10681, 6411);
                        case 23:
                            return new Beute(10528, 13619, 7950);
                        case 24:
                            return new Beute(13181, 17364, 9858);
                        case 25:
                            return new Beute(16503, 22139, 12224);
                        case 26:
                            return new Beute(20662, 28227, 15158);
                        case 27:
                            return new Beute(25869, 35990, 18796);
                        case 28:
                            return new Beute(32388, 45887, 23307);
                        case 29:
                            return new Beute(40549, 58506, 28900);
                        case 30:
                            return new Beute(50768, 74595, 35837);


                        default:
                            return new Beute();
                    }
                case Gebaeude.Bauernhof:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(45, 40, 30);
                        case 2:
                            return new Beute(59, 53, 39);
                        case 3:
                            return new Beute(76, 70, 50);
                        case 4:
                            return new Beute(99, 92, 64);
                        case 5:
                            return new Beute(129, 121, 83);
                        case 6:
                            return new Beute(167, 160, 107);
                        case 7:
                            return new Beute(217, 212, 138);
                        case 8:
                            return new Beute(282, 279, 178);
                        case 9:
                            return new Beute(367, 369, 230);
                        case 10:
                            return new Beute(477, 487, 297);
                        case 11:
                            return new Beute(620, 642, 383);
                        case 12:
                            return new Beute(806, 848, 494);
                        case 13:
                            return new Beute(1048, 1119, 637);
                        case 14:
                            return new Beute(1363, 1477, 822);
                        case 15:
                            return new Beute(1772, 1950, 1060);
                        case 16:
                            return new Beute(2303, 2574, 1368);
                        case 17:
                            return new Beute(2994, 3398, 1764);
                        case 18:
                            return new Beute(3893, 4486, 2276);
                        case 19:
                            return new Beute(5060, 5921, 2936);
                        case 20:
                            return new Beute(6579, 7816, 3787);
                        case 21:
                            return new Beute(8552, 10317, 4886);
                        case 22:
                            return new Beute(11118, 13618, 6302);
                        case 23:
                            return new Beute(14453, 17976, 8130);
                        case 24:
                            return new Beute(18789, 23728, 10488);
                        case 25:
                            return new Beute(24426, 31321, 13529);
                        case 26:
                            return new Beute(31754, 41344, 17453);
                        case 27:
                            return new Beute(41280, 54574, 22514);
                        case 28:
                            return new Beute(53664, 72037, 29043);
                        case 29:
                            return new Beute(69763, 95089, 37466);
                        case 30:
                            return new Beute(90692, 125517, 48331);


                        default:
                            return new Beute();
                    }
                case Gebaeude.Speicher:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(60, 50, 40);
                        case 2:
                            return new Beute(76, 64, 50);
                        case 3:
                            return new Beute(96, 81, 62);
                        case 4:
                            return new Beute(121, 102, 77);
                        case 5:
                            return new Beute(154, 130, 96);
                        case 6:
                            return new Beute(194, 165, 120);
                        case 7:
                            return new Beute(246, 210, 149);
                        case 8:
                            return new Beute(311, 266, 185);
                        case 9:
                            return new Beute(393, 338, 231);
                        case 10:
                            return new Beute(498, 430, 287);
                        case 11:
                            return new Beute(630, 546, 358);
                        case 12:
                            return new Beute(796, 693, 446);
                        case 13:
                            return new Beute(1007, 880, 555);
                        case 14:
                            return new Beute(1274, 1118, 691);
                        case 15:
                            return new Beute(1612, 1420, 860);
                        case 16:
                            return new Beute(2039, 1803, 1071);
                        case 17:
                            return new Beute(2580, 2290, 1333);
                        case 18:
                            return new Beute(3264, 2908, 1659);
                        case 19:
                            return new Beute(4128, 3693, 2066);
                        case 20:
                            return new Beute(5222, 4691, 2572);
                        case 21:
                            return new Beute(6606, 5957, 3202);
                        case 22:
                            return new Beute(8357, 7566, 3987);
                        case 23:
                            return new Beute(10572, 9608, 4963);
                        case 24:
                            return new Beute(13373, 12203, 6180);
                        case 25:
                            return new Beute(16917, 15497, 7694);
                        case 26:
                            return new Beute(21400, 19682, 9578);
                        case 27:
                            return new Beute(27071, 24996, 11925);
                        case 28:
                            return new Beute(34245, 31745, 14847);
                        case 29:
                            return new Beute(43320, 40316, 18484);
                        case 30:
                            return new Beute(54799, 51201, 23013);


                        default:
                            return new Beute();
                    }
                case Gebaeude.Versteck:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(50, 60, 50);
                        case 2:
                            return new Beute(63, 75, 63);
                        case 3:
                            return new Beute(78, 94, 78);
                        case 4:
                            return new Beute(98, 117, 98);
                        case 5:
                            return new Beute(122, 146, 122);
                        case 6:
                            return new Beute(153, 183, 153);
                        case 7:
                            return new Beute(191, 229, 191);
                        case 8:
                            return new Beute(238, 286, 238);
                        case 9:
                            return new Beute(298, 358, 298);
                        case 10:
                            return new Beute(373, 447, 373);

                        default:
                            return new Beute();
                    }
                case Gebaeude.Wall:
                    switch (targetlevel) {
                        case 1:
                            return new Beute(50, 100, 20);
                        case 2:
                            return new Beute(63, 128, 25);
                        case 3:
                            return new Beute(79, 163, 32);
                        case 4:
                            return new Beute(100, 207, 40);
                        case 5:
                            return new Beute(126, 264, 50);
                        case 6:
                            return new Beute(159, 337, 64);
                        case 7:
                            return new Beute(200, 430, 80);
                        case 8:
                            return new Beute(252, 548, 101);
                        case 9:
                            return new Beute(318, 698, 127);
                        case 10:
                            return new Beute(400, 890, 160);
                        case 11:
                            return new Beute(504, 1135, 202);
                        case 12:
                            return new Beute(635, 1447, 254);
                        case 13:
                            return new Beute(801, 1846, 320);
                        case 14:
                            return new Beute(1009, 2353, 404);
                        case 15:
                            return new Beute(1271, 3000, 508);
                        case 16:
                            return new Beute(1602, 3825, 641);
                        case 17:
                            return new Beute(2018, 4877, 807);
                        case 18:
                            return new Beute(2543, 6218, 1017);
                        case 19:
                            return new Beute(3204, 7928, 1281);
                        case 20:
                            return new Beute(4037, 10109, 1615);

                        default:
                            return new Beute();
                    }
                default:
                    return new Beute();
            }
        }
    }
}
