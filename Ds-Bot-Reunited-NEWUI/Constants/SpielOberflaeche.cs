﻿namespace Ds_Bot_Reunited_NEWUI.Constants {
    public class SpielOberflaeche {
        public class Raubzug {
            public const string FREISCHALTENBUTTON_SELECTOR_XPATH = "//a[@class='btn btn-default unlock-button']";
            public const string STARTBUTTON_SELECTOR_XPATH = "//a[@class='btn btn-default free_send_button']";
            public const string SPEERTRAEGER_SELECTOR_XPATH = "//input[@name='spear']";
            public const string SCHWERTKAEMPFER_SELECTOR_XPATH = "//input[@name='sword']";
            public const string AXTKAEMPFER_SELECTOR_XPATH = "//input[@name='axe']";
            public const string BOGENSCHUETZEN_SELECTOR_XPATH = "//input[@name='archer']";
            public const string LEICHTEKAVALLERIE_SELECTOR_XPATH = "//input[@name='light']";
            public const string BERITTENEBOGENSCHUETZEN_SELECTOR_XPATH = "//input[@name='marcher']";
            public const string SCHWEREKAVALLERIE_SELECTOR_XPATH = "//input[@name='heavy']";
            public const string PALADIN_SELECTOR_XPATH = "//input[@name='knight']";

            public const string MAX_SPEERTRAEGER_SELECTOR_XPATH = "//a[@data-unit='spear']";
            public const string MAX_SCHWERTKAEMPFER_SELECTOR_XPATH = "//a[@data-unit='sword']";
            public const string MAX_AXTKAEMPFER_SELECTOR_XPATH = "//a[@data-unit='axe']";
            public const string MAX_BOGENSCHUETZEN_SELECTOR_XPATH = "//a[@data-unit='archer']";
            public const string MAX_LEICHTEKAVALLERIE_SELECTOR_XPATH = "//a[@data-unit='light']";
            public const string MAX_BERITTENEBOGENSCHUETZEN_SELECTOR_XPATH = "//a[@data-unit='marcher']";
            public const string MAX_SCHWEREKAVALLERIE_SELECTOR_XPATH = "//a[@data-unit='heavy']";
            public const string MAX_PALADIN_SELECTOR_XPATH = "//a[@data-unit='knight']";

            public const string ALLETRUPPEN_SELECTOR_XPATH = "//a[@class='fill-all']";

            public const string LAUFZEIT = "//span[@class='duration']";

            public const string COLLECTED_WOOD = "//span[@class='wood-value']";
            public const string COLLECTED_STONE = "//span[@class='stone-value']";
            public const string COLLECTED_IRON = "//span[@class='iron-value']";
        }

        public class Dorfuebersicht {
            public const string HAUPTGEBAEUDEBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=main')]";
            public const string KASERNEBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=barracks')]";
            public const string STALLBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=stable')]";
            public const string WERKSTATTBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=garage')]";
            public const string SCHMIEDEBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=smith')]";
            public const string ADELSHOFBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=snob')]";
            public const string VERSAMMLUNGSPLATZBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=place')]";
            public const string STATUEBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=statue')]";
            public const string MARKTPLATZBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=market')]";
            public const string HOLZFAELLERBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=wood')]";
            public const string LEHMGRUBEBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=stone')]";
            public const string EISENMINEBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=iron')]";
            public const string BAUERNHOFBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=farm')]";
            public const string SPEICHERBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=storage')]";
            public const string VERSTECKBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=hide')]";
            public const string WALLBUTTON_NONGRAPHIC_SELECTOR_XPATH = "//a[contains(@href,'&screen=wall')]";

            public const string SPEICHER_SELECTOR_XPATH = "//span[@id='storage']";
            public const string AKTUELLBH_SELECTOR_XPATH = "//span[@id='pop_current_label']";
            public const string MAXBH_SELECTOR_XPATH = "//span[@id='pop_max_label']";

            public const string HAUPTGEBAEUDEBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=main')]";
            public const string KASERNEBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=barracks')]";
            public const string STALLBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=stable')]";
            public const string WERKSTATTBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=garage')]";
            public const string SCHMIEDEBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=smith')]";
            public const string ADELSHOFBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=snob')]";
            public const string VERSAMMLUNGSPLATZBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=place')]";
            public const string STATUEBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=statue')]";
            public const string MARKTPLATZBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=market')]";
            public const string HOLZFAELLERBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=wood')]";
            public const string LEHMGRUBEBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=stone')]";
            public const string EISENMINEBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=iron')]";
            public const string BAUERNHOFBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=farm')]";
            public const string SPEICHERBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=storage')]";
            public const string VERSTECKBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=hide')]";
            public const string WALLBUTTON_GRAPHIC_SELECTOR_XPATH = "//area[contains(@href,'&screen=wall')]";


            public const string UNITAMOUNT_SELECTOR_XPATH = "//div[id='show_units']";
            public const string PRODUCTIONAMOUNT_SELECTOR_XPATH = "//div[id='show_prod']";
            public const string EFFEKTE_SELECTOR_XPATH = "//div[id='show_effects']";

            public const string SPEICHER_HOLZ_SELECTOR_XPATH = "//span[@id='wood']";
            public const string SPEICHER_LEHM_SELECTOR_XPATH = "//span[@id='stone']";
            public const string SPEICHER_EISEN_SELECTOR_XPATH = "//span[@id='iron']";

            public const string ANGRIFFE_SELECTOR_XPATH = "//div[@id='commands_outgoings']/descendant::tr[@class='command-row']";
        }


        public class Kaserne {
            // * müssen ersetzt werden!

            public const string URL = "/game.php?village=*&screen=barracks";

            public const string SPEERTRAEGERANZAHLFELD_SELECTOR_XPATH = "//input[name='spear']";
            public const string SCHWERTKAEMPFERANZAHLFELD_SELECTOR_XPATH = "//input[name='sword']";
            public const string AXTKAEMPFERANZAHLFELD_SELECTOR_XPATH = "//input[name='axe']";
            public const string BOGENSCHUETZENANZAHLFELD_SELECTOR_XPATH = "//input[name='archer']";

            public const string SPEERTRAEGER_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='spear_0_a']";
            public const string SCHWERTKAEMPFER_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='sword_0_a']";
            public const string AXTKAEMPFER_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='axe_0_a']";
            public const string BOGENSCHUETZEN_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='archer_0_a']";

            public const string REKRUTIEREN_BUTTON_SELECTOR_XPATH = "//input[class='btn btn-recruit']";
        }

        public class Hauptgebaeude {
            // * müssen ersetzt werden!

            public const string URL = "/game.php?village=*&screen=main";

            public const string ABBRECHENBUTTON_SELECTOR_XPATH = "//a[@class='btn btn-cancel']";

            public const string BAUSCHLEIFE_SELECTOR_XPATH = "//*[contains(@class,'buildorder')]";

            public const string HAUPTGEBAEUDEBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_main')]";
            public const string KASERNEBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_barracks')]";
            public const string STALLBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_stable')]";
            public const string WERKSTATTBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_garage')]";
            public const string SCHMIEDEBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_smith')]";
            public const string ADELSHOFBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_smith')]";
            public const string VERSAMMLUNGSPLATZBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_place')]";
            public const string STATUEBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_statue')]";
            public const string MARKTPLATZBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_market')]";
            public const string HOLZFAELLERBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_wood')]";
            public const string LEHMGRUBEBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_stone')]";
            public const string EISENMINEBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_iron')]";
            public const string BAUERNHOFBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_farm')]";
            public const string SPEICHERBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_storage')]";
            public const string VERSTECKBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_hide')]";
            public const string WALLBAUEN_SELECTOR_XPATH = "//a[contains(@id,'main_buildlink_wall')]";

            public const string DORFUMBENENNENFELD_SELECTOR_XPATH = "//input[@name='name']";
            public const string DORFUMBENENNENBUTTON_SELECTOR_XPATH = "//input[@value='Ändern']";


            public const string HAUPTGEBAEUDE_VERKUERZEN_BUTTON_SELECTOR_XPATH = "//a[@class='order_feature btn btn-btr btn-instant-free']";
        }

        public class Stall {
            public const string URL = "/game.php?village=*&screen=stable";

            public const string SPAEHERANZAHLFELD_SELECTOR_XPATH = "//input[name='spy']";
            public const string LEICHTEKAVALLERIEANZAHLFELD_SELECTOR_XPATH = "//input[name='light']";
            public const string BERITTENEBOGENSCHUETZENANZAHLFELD_SELECTOR_XPATH = "//input[name='marcher']";
            public const string SCHWEREKAVALLERIEANZAHLFELD_SELECTOR_XPATH = "//input[name='heavy']";

            public const string SPAEHER_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='spy_0_a']";
            public const string LEICHTEKAVALLERIE_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='light_0_a']";
            public const string BERITTENEBOGENSCHUETZEN_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='marcher_0_a']";
            public const string SCHWEREKAVALLERIE_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='heavy_0_a']";

            public const string REKRUTIEREN_BUTTON_SELECTOR_XPATH = "//input[class='btn btn-recruit']";
        }

        public class Uebersicht {
            public class Angriffe {
                public const string ANGRIFFECOUNTER_SELECTOR_XPATH = "//span[@id='incomings_amount']";
                public const string ANGRIFFZEICHEN_SELECTOR_XPATH = "//td[@id='incomings_cell']";
                public const string INC_ANGREIFERPART_SELECTOR_XPATH = "//*[@id='content_value']/table/tbody/tr[3]/td[2]/span/a[1]";
                public const string ANKUNFT_SELECTOR_XPATH = "//*[@id='content_value']/table/tbody/tr[6]/td[2]";
                public const string ANGRIFF_SELECTOR_XPATH = "//tr[@class='command-row no_ignored_command']";
            }

            public const string RENAMEICON_SELECTOR_XPATH = "//a[@class='rename-icon']";
            public const string VERFUEGBAREHAENDLER_SELECTOR_XPATH = "//span[@id='market_merchant_available_count']";
        }

        public class Bauernhof {
            public const string MILIZEINBERUFENBUTTON_SELECTOR_XPATH = "//a[class='evt-confirm btn']";
        }

        public class Schmiede {
            public const string SCHWERTER_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'sword')]";
            public const string AXTKAEMPFER_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'axe')]";
            public const string BOGENSCHUETZEN_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'archer')]";
            public const string SPAEHER_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'spy')]";
            public const string LEICHTEKAVALLERIE_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'light')]";
            public const string BERITTENEBOGENSCHUETZEN_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'marcher')]";
            public const string SCHWEREKAVALLERIE_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'heavy')]";
            public const string RAMMBOECKE_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'ram')]";
            public const string KATAPULTE_ERFORSCHEN_SELECTOR_XPATH = "//a[contains(@onclick,'catapult')]";

            public const string AXTKAEMPFERERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('axe')')]";

            public const string BOGENSCHUETZENERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('archer')')]";

            public const string SPAEHERERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('spy')')]";

            public const string LEICHTEKAVALLERIEERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('light')')]";

            public const string BERITTENEBOGENSCHUETZENERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('marcher')')]";

            public const string SCHWEREKAVALLERIEERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('heavy')')]";

            public const string RAMMBOECKEERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('ram')')]";

            public const string KATAPULTEERFORSCHENBUTTON_SELECTOR_XPATH =
                "//a[contains(@onclick,'return BuildingSmith.research('catapult')')]";
        }

        public class Versammlungsplatz {
            public const string BEFEHLE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=place&mode=command')]";
            public const string TRUPPEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=place&mode=units')]";
            public const string SIMULATOR_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=place&mode=sim')]";
            public const string TRUPPENVORLAGEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=place&mode=template')]";
            public const string BEFEHLE_SELECTOR_XPATH = "//div[@id='commands_outgoings']";
            public const string TRUPPENTEXTBOXEN_SELECTOR_XPATH = "//input[contains(@id,'unit_input_')]";

            public class Befehle {
                public const string URL = "/game.php?village=*&screen=place&mode=command";

                public const string SPEERTRAEGER_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_spear']";
                public const string SCHWERTKAEMPFER_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_sword']";
                public const string AXTKAEMPFER_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_axe']";
                public const string BOGENSCHUETZEN_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_archer']";
                public const string SPAEHER_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_spy']";
                public const string LEICHTEKAVALLERIE_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_light']";
                public const string BERITTENEBOGENSCHUETZEN_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_marcher']";
                public const string SCHWEREKAVALLERIE_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_heavy']";
                public const string RAMMBOECKE_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_ram']";
                public const string KATAPULTE_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_catapult']";
                public const string PALADIN_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_knight']";
                public const string ADELSGESCHLECHT_ANZAHLFELD_SELECTOR_XPATH = "//input[@id='unit_input_snob']";

                public const string SPEERTRAEGER_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_spear']";
                public const string SCHWERTKAEMPFER_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_sword']";
                public const string AXTKAEMPFER_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_axe']";
                public const string BOGENSCHUETZEN_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_archer']";
                public const string SPAEHER_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_spy']";
                public const string LEICHTEKAVALLERIE_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_light']";
                public const string BERITTENEBOGENSCHUETZEN_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_marcher']";
                public const string SCHWEREKAVALLERIE_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_heavy']";
                public const string RAMMBOECKE_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_ram']";
                public const string KATAPULTE_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_catapult']";
                public const string PALADIN_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_knight']";
                public const string ADELSGESCHLECHT_ALLE_SELECTOR_XPATH = "//a[@id='units_entry_all_snob']";

                public const string BUTTON_SELECTALLUNITS_SELECTOR_XPATH = "//a[@id='selectAllUnits']";
                public const string TEMPLATE_SELECTOR_XPATH_FORALL = "//a[@class='troop_template_selector']";

                public const string KOORDINATENFELD_SELECTOR_XPATH =
                    "//input[@class='target-input-field target-input-autocomplete ui-autocomplete-input']";

                public const string LASTZIEL_SELECTOR_XPATH = "//a[@class='target-quickbutton target-last-attacked']";
                public const string ANGREIFEN_SELECTOR_XPATH = "//input[@id='target_attack']";
                public const string VERTEIDIGEN_SELECTOR_XPATH = "//input[@id='target_support']";


                public class Befehlbestaetigung {
                    public const string BESTAETIGUNGSBUTTON_SELECTOR_XPATH = "//input[@id='troop_confirm_go']";

                    public const string ANKUNFT_SELECTOR_XPATH = "//span[@class='relative_time']";

                    public const string LAUFZEIT_SELECTOR_XPATH = "//*[@id='command-data-form']/table[1]/tbody/tr[4]/td[2]";
                    public const string ANGREIFEN_BUTTON_SELECTOR_XPATH = "//input[@id='target_attack']";
                    public const string UNTERSTUETZEN_BUTTON_SELECTOR_XPATH = "//input[@id='target_support']";

                    public const string NOBELTRAIN_BUTTON_SELECTOR_XPATH = "//a[@id='troop_confirm_train']";
                }
            }
        }

        public class Werkstatt {
            public const string URL = "/game.php?village=*&screen=workshop";

            public const string RAMMBOECKEANZAHLFELD_SELECTOR_XPATH = "//input[name='ram']";
            public const string KATAPULTEANZAHLFELD_SELECTOR_XPATH = "//input[name='catapult']";

            public const string RAMMBOECKE_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='ram_0_a']";
            public const string KATAPULTE_ALLESAUSWAEHLEN_SELECTOR_XPATH = "//a[id='catapult_0_a']";

            public const string REKRUTIEREN_BUTTON_SELECTOR_XPATH = "//input[class='btn btn-recruit']";
        }

        public class Startseite {
            public const string ACCOUNTNAME_TEXTBOX_SELECTOR_XPATH = "//input[@id='user']";
            public const string PASSWORT_TEXTBOX_SELECTOR_XPATH = "//input[@id='password']";

            public const string ANMELDEN_BUTTON_SELECTOR_XPATH = "//a[@class='btn-login']";
            public const string WELT_BUTTON_SELECTOR_XPATH = "//span[contains(@class, 'world_button_active') and contains(text(),'*')]";
            public const string WELT_INAKTIV_BUTTON_SELECTOR_XPATH = "//span[contains(@class,'world_button_inactive') and contains(text(),'*')]";
            public const string TEILNEHMEN_BUTTON_SELECTOR_XPATH = "//button[@class='btn']";
        }

        public class Farmassistent {
            public const string SPEERTRAEGER_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-spear']";
            public const string SCHWERTKAEMPFER_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-sword']";
            public const string AXTKAEMPFER_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-axe']";
            public const string BOGENSCHUETZEN_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-archer']";
            public const string SPAEHER_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-spy']";
            public const string LEICHTEKAVALLERIE_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-light']";
            public const string BERITTENEBOGENSCHUETZEN_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-marcher']";
            public const string SCHWEREKAVALLERIE_VERFUEGBAR_SELECTOR_XPATH = "//td[@class='unit-item unit-item-heavy']";

            public const string SPEERTRAEGER_TEMPLATE_SELECTOR_XPATH = "//input[@name='spear']";
            public const string SCHWERTKAEMPFER_TEMPLATE_SELECTOR_XPATH = "//input[@name='sword']";
            public const string AXTKAEMPFER_TEMPLATE_SELECTOR_XPATH = "//input[@name='axe']";
            public const string BOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH = "//input[@name='archer']";
            public const string SPAEHER_TEMPLATE_SELECTOR_XPATH = "//input[@name='spy']";
            public const string LEICHTEKAVALLERIE_TEMPLATE_SELECTOR_XPATH = "//input[@name='light']";
            public const string BERITTENEBOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH = "//input[@name='marcher']";
            public const string SCHWEREKAVALLERIE_TEMPLATE_SELECTOR_XPATH = "//input[@name='heavy']";

            public const string TEMPLATE_BESTAETIGUNGSBUTTON_SELECTOR_XPATH = "//input[@class='btn']";
        }
        


        public class Allgemein {
            public const string FENSTER_SELECTOR_XPATH = "//div[contains(@id,'popup_box')]";
            public const string QUESTFENSTER_SELECTOR_XPATH = "//div[contains(@id,'popup_box_quest')]";
            public const string ERRORBOX_SELECTOR_XPATH = "//div[@class='error_box']";
            
            public const string COMMANDROW_SELECTOR_XPATH = "//tr[@class='command-row']";
            public const string VILLAGESELECTOR_SELECTOR_XPATH = "//span[@class='village-info']";
            

            public const string HEADERMENU_KARTE_SELECTOR_XPATH = "//td[@id='header_menu_link_map']";
            public const string PREMIUMPUNKTE_SELECTOR_XPATH = "//span[@id='premium_points']";

            public class Weltenswitch {
                public const string WELTENAUSWAHL_OEFFNEN_BUTTON_SELECTOR_XPATH = "//a[@class='world_button_active evt-world-selection-toggle']";
                public const string WELT_BUTTON_SELECTOR_XPATH = "//span[@class='world_button_active' and contains(@text(),'*')]";
            }

            public const string DORFUEBERSICHT_SELECTOR_XPATH = "//a[@class='nowrap tooltip-delayed']";

            public const string UEBERSICHT_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=overview')]";
            public const string KARTE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=map')]";
            public const string BERICHTE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=report')]";
            public const string NACHRICHTEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=mail')]";
            public const string FARMASSISTENT_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=am_farm')]";
            public const string ACCOUNTMANAGER_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=accountmanager')]";
            public const string RANGLISTE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=ranking')]";
            public const string STAMM_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=ally')]";
            public const string STAMMFORUM_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=ally')]";
            public const string PROFIL_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player')]";
            public const string PREMIUM_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=premium')]";
            public const string EINSTELLUNGEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=settings')]";

            public const string AUSLOGGEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'logout')]";


            public const string NEW_BERICHT_SELECTOR_XPATH = "//span[@class='icon header new_report']";
            public const string NEW_NACHRICHT_SELECTOR_XPATH = "//span[@class='icon header new_mail']";
            public const string NEW_FORUM_SELECTOR_XPATH = "//span[@class='icon header new_post']";
            public const string INVENTAR_COUNTER_SELECTOR_XPATH = "//span[@id='menu_counter_profile']";
        }

        public class Quest {
            public const string ANNEHMEN_BUTTON_SELECTOR_XPATH = "//a[@class='btn btn-confirm-yes']";
            public const string QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH = "//a[@class='popup_box_close tooltip-delayed']";
            public const string UEBERSPRINGEN_BUTTON_SELECTOR_XPATH = "//a[@class='btn btn-confirm-no']";

            public const string DAILYPRICE_SELECTOR_XPATH = "//div[@id='popup_box_daily_bonus']";
            public const string WARNUNG_SELECTOR_XPATH = "//span[contains(@class,'small warn')]";
        }

        public class Karte {
            public const string MAPCONTAINER_SELECTOR_XPATH = "//div[@id='map_container']";

            public const string SPIELERNAME_DORF_TEXTBOX_SELECTOR_XPATH = "//input[@class='target-input-field target-input-autocomplete ui-autocomplete-input']";

            public const string TOGGLE_NONATTACKABLE_DOERFER_SELECTOR_XPATH = "//input[@id='non_attackable_hide']";
            public const string TOGGLE_WACHTURM_SELECTOR_XPATH = "//input[@id='show_watchtower']";

            public const string XCOORDINATE_TEXTBOX_SELECTOR_XPATH = "//input[@id='mapx']";
            public const string YCOORDINATE_TEXTBOX_SELECTOR_XPATH = "//input[@id='mapy']";

            public const string DORF_MINI_IMAGE_LINK_XPATH = "//img[contains(@src,'1.png')]";
            public const string DORF_300_IMAGE_LINK_XPATH = "//img[contains(@src,'2.png')]";
            public const string DORF_1500_IMAGE_LINK_XPATH = "//img[contains(@src,'3.png')]";
            public const string DORF_3000_IMAGE_LINK_XPATH = "//img[contains(@src,'4.png')]";
            public const string DORF_10000_IMAGE_LINK_XPATH = "//img[contains(@src,'5.png')]";
            public const string DORF_12000_IMAGE_LINK_XPATH = "//img[contains(@src,'6.png')]";

            public const string BARBARENDORF_MINI_IMAGE_LINK_XPATH = "//img[contains(@src,'1_left.png')]";
            public const string BARBARENDORF_300_IMAGE_LINK_XPATH = "//img[contains(@src,'2_left.png')]";
            public const string BARBARENDORF_1500_IMAGE_LINK_XPATH = "//img[contains(@src,'3_left.png')]";
            public const string BARBARENDORF_3000_IMAGE_LINK_XPATH = "//img[contains(@src,'4_left.png')]";
            public const string BARBARENDORF_10000_IMAGE_LINK_XPATH = "//img[contains(@src,'5_left.png')]";
            public const string BARBARENDORF_12000_IMAGE_LINK_XPATH = "//img[contains(@src,'6_left.png')]";

            public class Kontextmenu {
                public const string DORF_INFO_SELECTOR_XPATH = "//a[@id='mp_info']";
                public const string DORF_SENDRESSOURCES_SELECTOR_XPATH = "//a[@id='mp_res']";
                public const string DORF_ANGREIFEN_SELECTOR_XPATH = "//a[@id='mp_att']";
                public const string DORF_NACHRICHTSENDEN_SELECTOR_XPATH = "//a[@id='mp_msg']";
                public const string DORF_PROFILOEFFNEN_SELECTOR_XPATH = "//a[@id='mp_profile']";
                public const string FARMASSISTENT_A_SELECTOR_XPATH = "//a[@id='mp_farm_a']";
                public const string FARMASSISTENT_B_SELECTOR_XPATH = "//a[@id='mp_farm_b']";

            }
        }

        public class Berichte {
            public const string ALLE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=all')]";
            public const string ANGRIFFE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=attack')]";
            public const string VERTEIDIGUNG_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=defense')]";
            public const string UNTERSTUETZUNG_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=support')]";
            public const string HANDEL_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=trade')]";
            public const string EVENTS_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=event')]";
            public const string SONSTIGES_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=other')]";
            public const string WEITERGELEITET_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=forwarded')]";
            public const string OEFFENTLICH_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=public')]";
            public const string FILTER_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=filter')]";

            public const string REPORTLIST_SELECTOR_XPATH = "//table[@id='report_list']";

            public const string SELECTALL_SELECTOR_XPATH = "//input[@id='select_all']";
            public const string REMOVE_REPORTS_SELECTOR_XPATH = "//input[@class='btn btn-cancel']";

            public const string BERICHT_SELECTOR_XPATH = "//a[contains(@href,'&view=')]";

            public const string ERSPAEHTEGEBAEUDE_SELECTOR_XPATH = "//table[contains(@id,'attack_spy_buildings')]";
            public const string ERSPAEHTETRUPPENAUSERHALB_SELECTOR_XPATH = "//table[@id='attack_spy_away']";
            public const string ANGRIFFERGEBNIS_SELECTOR_XPATH = "//table[@id='attack_results']";

            public class BerichteUebersicht {
                public const string BERICHTUEBERSICHT_SELECTOR_XPATH = "//td[@id='content_value']";
                public const string BERICHT_VORHER_BUTTON_SELECTOR_XPATH = "//a[@id='report-prev']";
                public const string BERICHT_NAECHSTER_BUTTON_SELECTOR_XPATH = "//a[@id='report-next']";
            }
        }

        public class Botschutz {
            public const string IFRAME_SELECTOR_CSS = "iframe[title='reCAPTCHA-Widget']";
        }

        public class Nachrichten {
            public const string NACHRICHTEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=in')]";
            public const string RUNDSCHREIBEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=mass_out')]";
            public const string NACHRICHTSCHREIBEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=new')]";

            public class Nachrichtschreiben {
                public const string EMPFAENGER_TEXTBOX_SELECTOR_XPATH = "//input[@id='to']";
                public const string BETREFF_TEXTBOX_SELECTOR_XPATH = "//input[@name='subject']";
                public const string NACHRICHT_TEXTBOX_SELECTOR_XPATH = "//textarea[@id='message']";

                public const string VORSCHAU_BUTTON_SELECTOR_XPATH = "//input[@name='preview']";
                public const string ABSENDEN_BUTTON_SELECTOR_XPATH = "//input[@name='send']";
            }
        }

        public class Rangliste {
            public const string RANGLISTE_SPIELER_SELECTOR_XPATH = "//table[@id='player_ranking_table']";

            public const string STAEMME_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=ally')]";
            public const string SPIELER_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=player')]";
            public const string DOMINANZ_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=dominance')]";
            public const string KONTINENTSTAEMME_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=con_ally')]";
            public const string KONTINENTSPIELER_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=con_player')]";
            public const string BESIEGTEGEGNERSTAMM_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=kill_ally')]";
            public const string BESIEGTEGEGNER_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=kill_player')]";
            public const string ERFOLGE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=awards')]";
            public const string ANEINEMTAG_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=in_a_day')]";
            public const string STAMMESKRIEGE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&mode=wars')]";

            public const string RANK_TEXTBOX_SELECTOR_XPATH = "//input[@name='rank']";
            public const string RANKNAME_TEXTBOX_SELECTOR_XPATH = "//input[@name='name']";
        }

        public class Markt {
            public const string BUY_WOOD_SELECTOR_XPATH = "//input[@name='buy_wood']";
            public const string BUY_STONE_SELECTOR_XPATH = "//input[@name='buy_stone']";
            public const string BUY_IRON_SELECTOR_XPATH = "//input[@name='buy_iron']";
            public const string SELL_WOOD_SELECTOR_XPATH = "//input[@name='sell_wood']";
            public const string SELL_STONE_SELECTOR_XPATH = "//input[@name='sell_stone']";
            public const string SELL_IRON_SELECTOR_XPATH = "//input[@name='sell_iron']";

            public const string BUY_PREMIUM_BUTTON_SELECTOR_XPATH = "//input[@class='btn float_right btn-premium-exchange-buy']";

            public const string CANCELBUTTON_SELECTOR_XPATH = "//button[@class='btn evt-cancel-btn btn-confirm-no']";
        }

        public class EigenesProfil {
            public const string EIGENESPROFIL_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player')]";
            public const string INVENTAR_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=inventory')]";
            public const string ERFOLGE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player&mode=awards')]";
            public const string STATISTIK_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player&mode=stats_own')]";
            public const string FREUNDE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=buddies')]";
            public const string TAEGLICHERBONUS_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player&mode=daily_bonus')]";
            public const string MENTORENPROGRAMM_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=mentor')]";
            public const string IGNORIERLISTE_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player&mode=block')]";


            public const string PROFILBEARBEITEN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'&screen=info_player&edit=1&')]";

            public const string PROFILTEXT_TEXTBOX_SELECTOR_XPATH = "//textarea[@id='message']";

            public const string PROFILTEXT_BESTAETIGEN_SELECTOR_XPATH = "//input[@name='send']";
            public const string PROFILTEXT_VORSCHAU_SELECTOR_XPATH = "//input[@name='preview']";

            public const string AVATARE_SELECTOR_XPATH = "//img[@class='generic-avatar']";

            public class Inventar {
                public const string INVENTAR_LISTE_SELECTOR_XPATH = "//div[@class='inventory_items']";
                public const string ITEM_AKTIVIEREN_BUTTON_SELECTOR_XPATH = "//a[contains(@href,'javascript:Inventory.consumeItem')]";

                public const string ITEM_BESTAETIGEN_BUTTON_SELECTOR_XPATH = "//button[@class='btn evt-confirm-btn btn-confirm-yes']";
                public const string ITEM_ABLEHNEN_BUTTON_SELECTOR_XPATH = "//button[@class='btn evt-confirm-btn btn-confirm-no']";
            }
        }
    }
}