﻿namespace Ds_Bot_Reunited_NEWUI.Constants {
    public static class Production {
        public static int GetProduction(int level) {
            if (level == 0)
                return (int) Produktion.Level1;
            if (level == 1)
                return (int) Produktion.Level1;
            if (level == 2)
                return (int) Produktion.Level2;
            if (level == 3)
                return (int) Produktion.Level3;
            if (level == 4)
                return (int) Produktion.Level4;
            if (level == 5)
                return (int) Produktion.Level5;
            if (level == 6)
                return (int) Produktion.Level6;
            if (level == 7)
                return (int) Produktion.Level7;
            if (level == 8)
                return (int) Produktion.Level8;
            if (level == 9)
                return (int) Produktion.Level9;
            if (level == 10)
                return (int) Produktion.Level10;
            if (level == 11)
                return (int) Produktion.Level11;
            if (level == 12)
                return (int) Produktion.Level12;
            if (level == 13)
                return (int) Produktion.Level13;
            if (level == 14)
                return (int) Produktion.Level14;
            if (level == 15)
                return (int) Produktion.Level15;
            if (level == 16)
                return (int) Produktion.Level16;
            if (level == 17)
                return (int) Produktion.Level17;
            if (level == 18)
                return (int) Produktion.Level18;
            if (level == 19)
                return (int) Produktion.Level19;
            if (level == 20)
                return (int) Produktion.Level20;
            if (level == 21)
                return (int) Produktion.Level21;
            if (level == 22)
                return (int) Produktion.Level22;
            if (level == 23)
                return (int) Produktion.Level23;
            if (level == 24)
                return (int) Produktion.Level24;
            if (level == 25)
                return (int) Produktion.Level25;
            if (level == 26)
                return (int) Produktion.Level26;
            if (level == 27)
                return (int) Produktion.Level27;
            if (level == 28)
                return (int) Produktion.Level28;
            if (level == 29)
                return (int) Produktion.Level29;
            if (level == 30)
                return (int) Produktion.Level30;
            return 0;
        }
    }


    public enum Produktion {
        Level0 = 5,
        Level1 = 30,
        Level2 = 35,
        Level3 = 41,
        Level4 = 47,
        Level5 = 55,
        Level6 = 64,
        Level7 = 74,
        Level8 = 86,
        Level9 = 100,
        Level10 = 117,
        Level11 = 136,
        Level12 = 158,
        Level13 = 184,
        Level14 = 214,
        Level15 = 249,
        Level16 = 289,
        Level17 = 337,
        Level18 = 391,
        Level19 = 455,
        Level20 = 530,
        Level21 = 616,
        Level22 = 717,
        Level23 = 833,
        Level24 = 969,
        Level25 = 1127,
        Level26 = 1311,
        Level27 = 1525,
        Level28 = 1774,
        Level29 = 2063,
        Level30 = 2400
    }
}