﻿namespace Ds_Bot_Reunited_NEWUI.Constants {
    public class RAMMECONSTANTS {
        public const int WALL20 = 219;
        public const int WALL19 = 191;
        public const int WALL18 = 166;
        public const int WALL17 = 143;
        public const int WALL16 = 124;
        public const int WALL15 = 106;
        public const int WALL14 = 91;
        public const int WALL13 = 77;
        public const int WALL12 = 65;
        public const int WALL11 = 55;
        public const int WALL10 = 45;
        public const int WALL9 = 37;
        public const int WALL8 = 30;
        public const int WALL7 = 24;
        public const int WALL6 = 19;
        public const int WALL5 = 14;
        public const int WALL4 = 10;
        public const int WALL3 = 7;
        public const int WALL2 = 4;
        public const int WALL1 = 2;

        public static int GetAmountForWall(int level) {
            if (level == 1) return WALL1;
            if (level == 2) return WALL2;
            if (level == 3) return WALL3;
            if (level == 4) return WALL4;
            if (level == 5) return WALL5;
            if (level == 6) return WALL6;
            if (level == 7) return WALL7;
            if (level == 8) return WALL8;
            if (level == 9) return WALL9;
            if (level == 10) return WALL10;
            if (level == 11) return WALL11;
            if (level == 12) return WALL12;
            if (level == 13) return WALL13;
            if (level == 14) return WALL14;
            if (level == 15) return WALL15;
            if (level == 16) return WALL16;
            if (level == 17) return WALL17;
            if (level == 18) return WALL18;
            if (level == 19) return WALL19;
            if (level == 20) return WALL20;
            return -1;
        }
    }
}