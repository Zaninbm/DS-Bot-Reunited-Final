﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.Angreifensettings {
    [Serializable]
    public class TruppenTemplateViewModel : INotifyPropertyChanged {
        public TruppenTemplate TruppenTemplate { get; set; }

        public string TemplateName {
            get => TruppenTemplate.TemplateName;
            set => TruppenTemplate.TemplateName = value;
        }

        public int SpeertraegerAnzahl {
            get => TruppenTemplate.SpeertraegerAnzahl;
            set {
                TruppenTemplate.SpeertraegerAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int SchwertkaempferAnzahl {
            get => TruppenTemplate.SchwertkaempferAnzahl;
            set {
                TruppenTemplate.SchwertkaempferAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int AxtkaempferAnzahl {
            get => TruppenTemplate.AxtkaempferAnzahl;
            set {
                TruppenTemplate.AxtkaempferAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int BogenschuetzenAnzahl {
            get => TruppenTemplate.BogenschuetzenAnzahl;
            set {
                TruppenTemplate.BogenschuetzenAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int SpaeherAnzahl {
            get => TruppenTemplate.SpaeherAnzahl;
            set {
                TruppenTemplate.SpaeherAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int LeichteKavallerieAnzahl {
            get => TruppenTemplate.LeichteKavallerieAnzahl;
            set {
                TruppenTemplate.LeichteKavallerieAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int BeritteneBogenschuetzenAnzahl {
            get => TruppenTemplate.BeritteneBogenschuetzenAnzahl;
            set {
                TruppenTemplate.BeritteneBogenschuetzenAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int SchwereKavallerieAnzahl {
            get => TruppenTemplate.SchwereKavallerieAnzahl;
            set {
                TruppenTemplate.SchwereKavallerieAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int RammboeckeAnzahl {
            get => TruppenTemplate.RammboeckeAnzahl;
            set {
                TruppenTemplate.RammboeckeAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int KatapulteAnzahl {
            get => TruppenTemplate.KatapulteAnzahl;
            set {
                TruppenTemplate.KatapulteAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int PaladinAnzahl {
            get => TruppenTemplate.PaladinAnzahl;
            set {
                TruppenTemplate.PaladinAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int AdelsgeschlechterAnzahl {
            get => TruppenTemplate.AdelsgeschlechterAnzahl;
            set {
                TruppenTemplate.AdelsgeschlechterAnzahl = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplateViewModel() {
            if (TruppenTemplate == null) {
                TruppenTemplate = new TruppenTemplate();
            }
        }

        public TruppenTemplateViewModel(TruppenTemplate truppenTemplate) {
            TruppenTemplate = truppenTemplate;
        }

        public bool BogenschuetzenEnabled => App.Settings != null && App.Settings.Weltensettings.BogenschuetzenActive;
        public bool PaladinEnabled => App.Settings != null && App.Settings.Weltensettings.PaladinActive;


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}