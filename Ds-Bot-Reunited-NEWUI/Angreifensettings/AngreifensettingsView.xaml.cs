﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Ds_Bot_Reunited_NEWUI.Angreifensettings {
    public partial class AngreifensettingsView {
        public AngreifensettingsView(Window owner) {
            Owner = owner;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((AngreifenSettingsViewModel) DataContext)?.SelectedBewegungen != null) {
                ((AngreifenSettingsViewModel) DataContext)?.SelectedBewegungen.Clear();
                ((AngreifenSettingsViewModel) DataContext)?.SelectedBewegungen.AddRange(DataGrid.SelectedItems.Cast<TruppenbewegungViewModel>());
            }
        }
    }
}
