﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.SearchPlayer;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Framework.UI.Controls;
using Window = System.Windows.Window;

// ReSharper disable LoopCanBeConvertedToQuery
// ReSharper disable ForCanBeConvertedToForeach

#pragma warning disable 4014

namespace Ds_Bot_Reunited_NEWUI.Angreifensettings {
    public class AngreifenSettingsViewModel : INotifyPropertyChanged {
        private readonly Accountsetting _accountsettings;

        [Obfuscation(Exclude = true, Feature = "renaming")]
        private readonly AsyncObservableCollection<TruppenbewegungViewModel> _bewegungen;

        [Obfuscation(Exclude = true, Feature = "renaming")]
        private readonly AsyncObservableCollection<DorfViewModel> _villagelistActualAccount;

        [Obfuscation(Exclude = true, Feature = "renaming")]
        private readonly AsyncObservableCollection<DorfViewModel> _villagelistSelectedAccount;

        private readonly Window _window;
        private ICollectionView _accountlistView;
        private ICommand _addNewBewegungCommand;
        private int _attacksCount;
        private int _attacksQueuedCount;
        private int _attacksSentCount;
        private int _attacksToSendCount;
        private ICollectionView _bewegungenView;
        private ICollectionView _bewegungsartenView;
        private bool _canAddNewBewegung = true;
        private ICommand _copyActualBefehlCommand;
        private ICommand _deleteAllCommand;
        private string _empfaengerCoords = "";
        private int _listMaxHeight = 500;
        private int _mindestAbstand;
        private bool _progressRing2Active;
        private bool _progressRingActive;
        private ICommand _removeCommand;
        private DateTime _sameAnkunftszeit = DateTime.Now.AddMinutes(3);
        private ICommand _searchPlayerCommand;
        private SpielerViewModel _selectedEmpfaengerAccount;
        private DorfViewModel _selectedEmpfaengerVillage;
        private GruppeDoerfer _selectedSenderGruppe;
        private DorfViewModel _selectedSenderVillage;
        private string _senderCoords = "";
        private ICommand _setAllExactTimingCommand;
        private ICommand _setAllToActiveCommand;
        private ICommand _setAllToFalseCommand;
        private ICommand _setSameAbsendezeitCommand;
        private ICommand _setSameAdelsgeschlechtAnzahlCommand;
        private ICommand _setSameAnkunftszeitCommand;
        private ICommand _setSameAxtkaempferAnzahlCommand;
        private ICommand _setSameBeritteneBogenschuetzenAnzahlCommand;
        private ICommand _setSameBewegungsartCommand;
        private ICommand _setSameBogenschuetzenAnzahlCommand;
        private ICommand _setSameKatapulteAnzahlCommand;
        private ICommand _setSameLeichteKavallerieAnzahlCommand;
        private ICommand _setSamePaladinAnzahlCommand;
        private ICommand _setSameRammboeckeAnzahlCommand;
        private ICommand _setSameSchwereKavallerieAnzahlCommand;
        private ICommand _setSameSchwertkaempferAnzahlCommand;
        private ICommand _setSameSpaeherAnzahlCommand;
        private ICommand _setSameSpeertraegerAnzahlCommand;
        private TruppenTemplateViewModel _template;
        private bool _truppenBewegungIsVisible;
        private ICollectionView _villagelistSelectedAccountView;
        private ICollectionView _villagelistView;


        public AngreifenSettingsViewModel(Accountsetting accountsettings, Window window) {
            _window = window;
            _accountsettings = accountsettings;
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            _villagelistSelectedAccount = new AsyncObservableCollection<DorfViewModel>();
            VillagelistSelectedAccount = CollectionViewSource.GetDefaultView(_villagelistSelectedAccount);
            var spielerViewModel = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName.Equals(!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname));
            _bewegungen = new AsyncObservableCollection<TruppenbewegungViewModel>();
            if (spielerViewModel != null) {
                var playeridx = spielerViewModel.PlayerId;
                _villagelistActualAccount = new AsyncObservableCollection<DorfViewModel>();
                Villagelist = CollectionViewSource.GetDefaultView(_villagelistActualAccount);
                _bewegungen = new AsyncObservableCollection<TruppenbewegungViewModel>(accountsettings.GeplanteBewegungen.Any() ? accountsettings.GeplanteBewegungen.Select(bewegung => new TruppenbewegungViewModel(bewegung, playeridx))
                                                                                          : new List<TruppenbewegungViewModel>());
            }
            SelectedBewegungen = new AsyncObservableCollection<TruppenbewegungViewModel>();
            foreach (var bewegung in _bewegungen)
                bewegung.Vm = this;
            BindingOperations.EnableCollectionSynchronization(SelectedBewegungen, SelectedBewegungenLock);
            BindingOperations.EnableCollectionSynchronization(_villagelistSelectedAccount, VillagelistSelectedAccountLock);
            BindingOperations.EnableCollectionSynchronization(_villagelistActualAccount, VillagelistActualAccountLock);
            Bewegungen = CollectionViewSource.GetDefaultView(_bewegungen);
            Accountlist = CollectionViewSource.GetDefaultView(App.PlayerlistWholeWorld);
            SelectedSenderGruppe = Groups.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
            UpdateGesamteTruppenverwendet();
        }

        [Obfuscation(Exclude = true, Feature = "renaming")]
        public AsyncObservableCollection<TruppenbewegungViewModel> SelectedBewegungen { get; set; }

        public List<GruppeDoerfer> Groups => App.Settings.Gruppen;


        public bool AngreifenActive {
            // ReSharper disable once InconsistentlySynchronizedField
            get => _accountsettings.SendingActive;
            set {
                // ReSharper disable once InconsistentlySynchronizedField
                _accountsettings.SendingActive = value;
                OnPropertyChanged();
            }
        }


        private object VillagelistActualAccountLock { get; } = new object();
        public int SpeertraegerGesamt => _template.SpeertraegerAnzahl;
        public int SchwertkaempferGesamt => _template.SchwertkaempferAnzahl;
        public int AxtkaempferGesamt => _template.AxtkaempferAnzahl;
        public int BogenschuetzenGesamt => _template.BogenschuetzenAnzahl;
        public int SpaeherGesamt => _template.SpaeherAnzahl;
        public int LeichteKavallerieGesamt => _template.LeichteKavallerieAnzahl;
        public int BeritteneBogenschuetzenGesamt => _template.BeritteneBogenschuetzenAnzahl;
        public int SchwereKavallerieGesamt => _template.SchwereKavallerieAnzahl;
        public int RammboeckeGesamt => _template.RammboeckeAnzahl;
        public int KatapulteGesamt => _template.KatapulteAnzahl;
        public int PaladineGesamt => _template.PaladinAnzahl;
        public int AdelsgeschlechterGesamt => _template.AdelsgeschlechterAnzahl;

        public int ListMaxHeight {
            get => _listMaxHeight;
            set {
                _listMaxHeight = value;
                OnPropertyChanged();
            }
        }

        public bool TruppenBewegungIsVisible {
            get => _truppenBewegungIsVisible;
            set {
                _truppenBewegungIsVisible = value;
                ListMaxHeight = value ? 0 : 500;
                OnPropertyChanged();
            }
        }

        public TruppenTemplateViewModel Template {
            get => _template;
            set {
                _template = value;
                OnPropertyChanged("");
            }
        }

        public bool CanAddNewBewegung {
            get => _canAddNewBewegung;
            set {
                _canAddNewBewegung = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView Villagelist {
            get => _villagelistView;
            set {
                _villagelistView = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView Accountlist {
            get => _accountlistView;
            set {
                _accountlistView = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView VillagelistSelectedAccount {
            get => _villagelistSelectedAccountView;
            set {
                _villagelistSelectedAccountView = value;
                OnPropertyChanged();
            }
        }

        public GruppeDoerfer SelectedSenderGruppe {
            get => _selectedSenderGruppe;
            set {
                _selectedSenderGruppe = value;
                LoadPlayerVillages();
                OnPropertyChanged("");
            }
        }

        public int GruppeAnzahl {
            get {
                if (_villagelistActualAccount != null)
                    return _villagelistActualAccount.Count;
                return 0;
            }
        }

        public SpielerViewModel SelectedEmpfaengerAccount {
            get => _selectedEmpfaengerAccount;
            set {
                _selectedEmpfaengerAccount = value;
                GetEmpfaengerVillages();
                OnPropertyChanged("");
            }
        }

        public bool ProgressRing2Active {
            get => _progressRing2Active;
            set {
                _progressRing2Active = value;
                OnPropertyChanged();
            }
        }

        public int Preparationtime {
            get => App.Settings.Preparationtime;
            set {
                App.Settings.Preparationtime = value;
                OnPropertyChanged();
            }
        }

        public DorfViewModel SelectedSenderVillage {
            get => _selectedSenderVillage;
            set {
                _selectedSenderVillage = value;
                if (_selectedSenderVillage != null && SenderCoords != null && !SenderCoords.Equals(_selectedSenderVillage.XCoordinate + "|" + _selectedSenderVillage.YCoordinate))
                    _senderCoords = _selectedSenderVillage.XCoordinate + "|" + _selectedSenderVillage.YCoordinate;
                else if (_selectedSenderVillage == null)
                    _senderCoords = "";
                OnPropertyChanged("");
            }
        }

        public DorfViewModel SelectedEmpfaengerVillage {
            get => _selectedEmpfaengerVillage;
            set {
                _selectedEmpfaengerVillage = value;
                if (_selectedEmpfaengerVillage != null && EmpfaengerCoords != null && !EmpfaengerCoords.Equals(_selectedEmpfaengerVillage.XCoordinate + "|" + _selectedEmpfaengerVillage.YCoordinate))
                    _empfaengerCoords = _selectedEmpfaengerVillage.XCoordinate + "|" + _selectedEmpfaengerVillage.YCoordinate;
                else if (_selectedEmpfaengerVillage == null)
                    _empfaengerCoords = "";
                OnPropertyChanged("");
            }
        }

        public ICollectionView Bewegungen {
            get => _bewegungenView;
            set {
                _bewegungenView = value;
                OnPropertyChanged();
            }
        }

        public string SenderCoords {
            get => _senderCoords;
            set {
                _senderCoords = value;
                try {
                    var splitted = value.Split('|');
                    var searchTuple = new VillageTuple<int, int>(int.Parse(splitted[0]), int.Parse(splitted[1]));
                    var village = App.VillagelistWholeWorld[searchTuple];
                    if (village != null && (SelectedSenderVillage == null || !SelectedSenderVillage.VillageId.Equals(village.VillageId)))
                        SelectedSenderVillage = village;
                } catch {
                    // ignored
                }
                OnPropertyChanged();
            }
        }

        public string EmpfaengerCoords {
            get => _empfaengerCoords;
            set {
                _empfaengerCoords = value;
                try {
                    var splitted = value.Split('|');
                    var searchTuple = new VillageTuple<int, int>(int.Parse(splitted[0]), int.Parse(splitted[1]));
                    var village = App.VillagelistWholeWorld[searchTuple];
                    if (village != null && (SelectedEmpfaengerVillage == null || !SelectedEmpfaengerVillage.VillageId.Equals(village.VillageId))) {
                        SelectedEmpfaengerVillage = village;
                        if (!village.PlayerId.Equals(0))
                            SelectedEmpfaengerAccount = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerId.Equals(village.PlayerId));
                        else
                            SelectedEmpfaengerAccount = null;
                    }
                } catch {
                    // ignored
                }
                OnPropertyChanged();
            }
        }

        public int AttacksToSendCount {
            get => _attacksToSendCount;
            set {
                _attacksToSendCount = value;
                OnPropertyChanged();
            }
        }

        public int AttacksCount {
            get => _attacksCount;
            set {
                _attacksCount = value;
                OnPropertyChanged();
            }
        }

        public int AttacksSentCount {
            get => _attacksSentCount;
            set {
                _attacksSentCount = value;
                OnPropertyChanged();
            }
        }

        public int AttacksQueuedCount {
            get => _attacksQueuedCount;
            set {
                _attacksQueuedCount = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView Bewegungsarten {
            get => _bewegungsartenView;
            set {
                _bewegungsartenView = value;
                OnPropertyChanged();
            }
        }

        public string SameBewegungsart { get; set; }

        public ICommand SetSameBewegungsartCommand => _setSameBewegungsartCommand ?? (_setSameBewegungsartCommand = new RelayCommand(SetSameBewegungsart));

        public string SameSpeertraegerAnzahl { get; set; }

        public ICommand SetSameSpeertraegerAnzahlCommand => _setSameSpeertraegerAnzahlCommand ?? (_setSameSpeertraegerAnzahlCommand = new RelayCommand(SetSameSpeertraegerAnzahl));

        public string SameSchwertkaempferAnzahl { get; set; }

        public ICommand SetSameSchwertkaempferAnzahlCommand => _setSameSchwertkaempferAnzahlCommand ?? (_setSameSchwertkaempferAnzahlCommand = new RelayCommand(SetSameSchwertkaempferAnzahl));

        public string SameAxtkaempferAnzahl { get; set; }

        public ICommand SetSameAxtkaempferAnzahlCommand => _setSameAxtkaempferAnzahlCommand ?? (_setSameAxtkaempferAnzahlCommand = new RelayCommand(SetSameAxtkaempferAnzahl));

        public string SameBogenschuetzenAnzahl { get; set; }

        public ICommand SetSameBogenschuetzenAnzahlCommand => _setSameBogenschuetzenAnzahlCommand ?? (_setSameBogenschuetzenAnzahlCommand = new RelayCommand(SetSameBogenschuetzenAnzahl));

        public string SameSpaeherAnzahl { get; set; }

        public ICommand SetSameSpaeherAnzahlCommand => _setSameSpaeherAnzahlCommand ?? (_setSameSpaeherAnzahlCommand = new RelayCommand(SetSameSpaeherAnzahl));

        public string SameLeichteKavallerieAnzahl { get; set; }

        public ICommand SetSameLeichteKavallerieAnzahlCommand => _setSameLeichteKavallerieAnzahlCommand ?? (_setSameLeichteKavallerieAnzahlCommand = new RelayCommand(SetSameLeichteKavallerieAnzahl));

        public string SameBeritteneBogenschuetzenAnzahl { get; set; }

        public ICommand SetSameBeritteneBogenschuetzenAnzahlCommand => _setSameBeritteneBogenschuetzenAnzahlCommand ?? (_setSameBeritteneBogenschuetzenAnzahlCommand = new RelayCommand(SetSameBeritteneBogenschuetzenAnzahl));

        public string SameSchwereKavallerieAnzahl { get; set; }

        public ICommand SetSameSchwereKavallerieAnzahlCommand => _setSameSchwereKavallerieAnzahlCommand ?? (_setSameSchwereKavallerieAnzahlCommand = new RelayCommand(SetSameSchwereKavallerieAnzahl));

        public string SameRammboeckeAnzahl { get; set; }

        public ICommand SetSameRammboeckeAnzahlCommand => _setSameRammboeckeAnzahlCommand ?? (_setSameRammboeckeAnzahlCommand = new RelayCommand(SetSameRammboeckeAnzahl));

        public string SameKatapulteAnzahl { get; set; }

        public ICommand SetSameKatapulteAnzahlCommand => _setSameKatapulteAnzahlCommand ?? (_setSameKatapulteAnzahlCommand = new RelayCommand(SetSameKatapulteAnzahl));

        public string SamePaladinAnzahl { get; set; }

        public ICommand SetSamePaladinAnzahlCommand => _setSamePaladinAnzahlCommand ?? (_setSamePaladinAnzahlCommand = new RelayCommand(SetSamePaladinAnzahl));

        public string SameAdelsgeschlechtAnzahl { get; set; }

        public ICommand SetSameAdelsgeschlechtAnzahlCommand => _setSameAdelsgeschlechtAnzahlCommand ?? (_setSameAdelsgeschlechtAnzahlCommand = new RelayCommand(SetSameAdelsgeschlechtAnzahl));

        public string SameAnkunftszeit {
            get => _sameAnkunftszeit.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                var date = value.Split('\n')[0];
                try {
                    _sameAnkunftszeit = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                } catch {
                    // ignored
                }
                OnPropertyChanged();
            }
        }

        public bool SameZeitMitAbstandIsChecked { get; set; }

        public int MindestAbstand {
            get => _mindestAbstand;
            set {
                _mindestAbstand = value;
                OnPropertyChanged();
            }
        }

        public ICommand SetSameAnkunftszeitCommand => _setSameAnkunftszeitCommand ?? (_setSameAnkunftszeitCommand = new RelayCommand(SetSameAnkunftszeit));

        public ICommand SetSameAbsendezeitCommand => _setSameAbsendezeitCommand ?? (_setSameAbsendezeitCommand = new RelayCommand(SetSameAbsendezeit));

        public ICommand SetAllToActiveCommand => _setAllToActiveCommand ?? (_setAllToActiveCommand = new RelayCommand(SetAllToActive));

        public ICommand SetAllExactTimingCommand => _setAllExactTimingCommand ?? (_setAllExactTimingCommand = new RelayCommand(SetAllExactTiming));

        public ICommand SetAllToFalseCommand => _setAllToFalseCommand ?? (_setAllToFalseCommand = new RelayCommand(SetAllToFalse));

        public ICommand AddNewBewegungCommand => _addNewBewegungCommand ?? (_addNewBewegungCommand = new RelayCommand(AddNewBewegung, () => CanAddNewBewegung));

        public ICommand CopyActualBefehlCommand => _copyActualBefehlCommand ?? (_copyActualBefehlCommand = new RelayCommand(CopyActualCommand));

        public ICommand DeleteAllCommand => _deleteAllCommand ?? (_deleteAllCommand = new RelayCommand(DeleteAll));

        public ICommand SearchPlayerCommand => _searchPlayerCommand ?? (_searchPlayerCommand = new RelayCommand(SearchPlayer));

        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand(Remove));

        public int CloneCommandTimes { get; set; }
        private object VillagelistSelectedAccountLock { get; } = new object();

        private object BewegungenLock { get; } = new object();
        public object SelectedBewegungenLock { get; set; } = new object();

        public bool ProgressRingActive {
            get => _progressRingActive;
            set {
                _progressRingActive = value;
                OnPropertyChanged();
            }
        }

        public bool IsEnabled => App.Permissions.SendingIsEnabled;

        public event PropertyChangedEventHandler PropertyChanged;

        // ReSharper disable once UnusedMethodReturnValue.Local
        private async Task GetEmpfaengerVillages() {
            await Task.Run(() => {
                               ProgressRing2Active = true;
                               _villagelistSelectedAccount.Clear();
                               if (_selectedEmpfaengerAccount != null)
                                   foreach (var dorf in App.VillagelistWholeWorld.Where(x => x.Value.PlayerId == _selectedEmpfaengerAccount.PlayerId).OrderBy(x => x.Value.Name))
                                       _villagelistSelectedAccount.Add(dorf.Value);
                               else
                                   _villagelistSelectedAccount.Add(SelectedEmpfaengerVillage);
                               ProgressRing2Active = false;
                           });
        }

        //public ICommand ImportFromDsWorkbenchCommand
        //    =>
        //        _importFromDsWorkbenchCommand ??
        //        (_importFromDsWorkbenchCommand = new RelayCommand(ImportFromDsWorkbench));

        //public ICommand ImportFromMassPlanerCommand
        //    =>
        //        _importFromMassPlanerCommand ??
        //        (_importFromMassPlanerCommand = new RelayCommand(ImportFromMassPlaner));

        private void UpdateGesamteTruppenverwendet() {
            Template = new TruppenTemplateViewModel {TemplateName = "GESAMT"};
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _bewegungen.Count; i++) {
                var bewegung = _bewegungen[i];
                Template.TruppenTemplate.SpeertraegerAnzahl += bewegung.Model.SpeertraegerAnzahl;
                Template.TruppenTemplate.SchwertkaempferAnzahl += bewegung.Model.SchwertkaempferAnzahl;
                Template.TruppenTemplate.AxtkaempferAnzahl += bewegung.Model.AxtkaempferAnzahl;
                Template.TruppenTemplate.BogenschuetzenAnzahl += bewegung.Model.BogenschuetzenAnzahl;
                Template.TruppenTemplate.SpaeherAnzahl += bewegung.Model.SpaeherAnzahl;
                Template.TruppenTemplate.LeichteKavallerieAnzahl += bewegung.Model.LeichteKavallerieAnzahl;
                Template.TruppenTemplate.BeritteneBogenschuetzenAnzahl += bewegung.Model.BeritteneBogenschuetzenAnzahl;
                Template.TruppenTemplate.SchwereKavallerieAnzahl += bewegung.Model.SchwereKavallerieAnzahl;
                Template.TruppenTemplate.RammboeckeAnzahl += bewegung.Model.RammboeckeAnzahl;
                Template.TruppenTemplate.KatapulteAnzahl += bewegung.Model.KatapulteAnzahl;
                Template.TruppenTemplate.PaladinAnzahl += bewegung.Model.PaladinAnzahl;
                Template.TruppenTemplate.AdelsgeschlechterAnzahl += bewegung.Model.AdelsgeschlechterAnzahl;
            }
            OnPropertyChanged("");
        }

        public void AddBewegung(TruppenbewegungViewModel bewegung) {
            _bewegungen.Add(bewegung);
            AttacksToSendCount = _bewegungen.Count(x => x.Model.Active);
            AttacksSentCount = _bewegungen.Count(x => x.Model.Sent);
            AttacksQueuedCount = _bewegungen.Count(x => x.Model.Queued);
            UpdateGesamteTruppenverwendet();
            OnPropertyChanged("");
        }

        private void SetSameBewegungsart() {
            foreach (var bewegung in SelectedBewegungen)
                bewegung.BewegungsartSelected = SameBewegungsart;
        }

        private async void SetSameSpeertraegerAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Speertraeger = SameSpeertraegerAnzahl;
                           });
        }

        private async void SetSameSchwertkaempferAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Schwertkaempfer = SameSchwertkaempferAnzahl;
                           });
        }

        private async void SetSameAxtkaempferAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Axtkaempfer = SameAxtkaempferAnzahl;
                           });
        }

        private async void SetSameBogenschuetzenAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Bogenschuetzen = SameBogenschuetzenAnzahl;
                           });
        }

        private async void SetSameSpaeherAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Spaeher = SameSpaeherAnzahl;
                           });
        }

        private async void SetSameLeichteKavallerieAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].LeichteKavallerie = SameLeichteKavallerieAnzahl;
                           });
        }

        private async void SetSameBeritteneBogenschuetzenAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].BeritteneBogenschuetzen = SameBeritteneBogenschuetzenAnzahl;
                           });
        }

        private async void SetSameSchwereKavallerieAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].SchwereKavallerie = SameSchwereKavallerieAnzahl;
                           });
        }

        private async void SetSameRammboeckeAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Rammboecke = SameRammboeckeAnzahl;
                           });
        }

        private async void SetSameKatapulteAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Katapulte = SameKatapulteAnzahl;
                           });
        }

        private async void SetSamePaladinAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Paladin = SamePaladinAnzahl;
                           });
        }

        private async void SetSameAdelsgeschlechtAnzahl() {
            await Task.Run(() => {
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   SelectedBewegungen[i].Adelsgeschlechter = SameAdelsgeschlechtAnzahl;
                           });
        }

        private async void SetSameAnkunftszeit() {
            await Task.Run(() => {
                               var count = 0;
                               for (var i = 0; i < SelectedBewegungen.Count; i++) {
                                   var tempDateTime = _sameAnkunftszeit.AddMilliseconds(count);
                                   SelectedBewegungen[i].Ankunftszeitpunkt = tempDateTime.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                                   count += MindestAbstand > 0 && SameZeitMitAbstandIsChecked ? MindestAbstand : 40;
                               }
                           });
        }

        private async void SetSameAbsendezeit() {
            await Task.Run(() => {
                               var count = 0;
                               for (var i = 0; i < SelectedBewegungen.Count; i++) {
                                   var tempDateTime = _sameAnkunftszeit.AddMilliseconds(count);
                                   SelectedBewegungen[i].Abschickzeitpunkt = tempDateTime.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                                   count += MindestAbstand > 0 && SameZeitMitAbstandIsChecked ? MindestAbstand : 40;
                               }
                           });
        }

        private async void SetAllToActive() {
            await Task.Run(() => {
                               var yes = false;
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   yes = SelectedBewegungen[i].SetToTrue(yes);
                           });
            AttacksToSendCount = _bewegungen.Count(x => x.Model.Active);
            AttacksCount = _bewegungen.Count;
            AttacksSentCount = _bewegungen.Count(x => x.Model.Sent);
            AttacksQueuedCount = _bewegungen.Count(x => x.Model.Queued);
            OnPropertyChanged("");
        }

        private async void SetAllExactTiming() {
            await Task.Run(() => {
                               foreach (var bewegung in SelectedBewegungen)
                                   bewegung.GenauTimenIsActive = true;
                           });
            OnPropertyChanged("");
        }

        private async void SetAllToFalse() {
            await Task.Run(() => {
                               var yes = false;
                               for (var i = 0; i < SelectedBewegungen.Count; i++)
                                   yes = SelectedBewegungen[i].SetToFalse(yes);
                           });
            AttacksToSendCount = _bewegungen.Count(x => x.Model.Active);
            AttacksCount = _bewegungen.Count;
            AttacksSentCount = _bewegungen.Count(x => x.Model.Sent);
            AttacksQueuedCount = _bewegungen.Count(x => x.Model.Queued);
            OnPropertyChanged("");
        }

        private async void DeleteAll() {
            if (await MessageDialog.ShowAsync(Resources.RemoveTheWholeList, Resources.AreYouSure, MessageBoxButton.YesNo, MessageDialogType.Accent, _window) == MessageBoxResult.No)
                return;
            _bewegungen.Clear();
            _accountsettings.GeplanteBewegungen.Clear();
            AttacksToSendCount = _bewegungen.Count(x => x.Model.Active);
            AttacksCount = _bewegungen.Count;
            AttacksSentCount = _bewegungen.Count(x => x.Model.Sent);
            AttacksQueuedCount = _bewegungen.Count(x => x.Model.Queued);
            OnPropertyChanged("");
        }

        private void SearchPlayer() {
            var view = new SearchPlayerView(_window);
            var viewmodel = new SearchPlayerViewModel(view);
            view.DataContext = viewmodel;
            view.ShowDialog();
            if (viewmodel.UsePlayer && SelectedEmpfaengerAccount != null)
                SelectedEmpfaengerAccount = viewmodel.SelectedPlayer;
        }

        private void CopyActualCommand() {
            if (Bewegungen.CurrentItem != null) {
                for (var i = 1; i < CloneCommandTimes + 1; i++) {
                    var actualbewegung = (TruppenbewegungViewModel) Bewegungen.CurrentItem;
                    var copy = actualbewegung.Clone(" " + Resources.Copy + " " + i);
                    _bewegungen.Add(copy);
                    _accountsettings.GeplanteBewegungen.Add(copy.Model);
                }
                AttacksToSendCount = _bewegungen.Count(x => x.Model.Active);
                AttacksSentCount = _bewegungen.Count(x => x.Model.Sent);
                AttacksQueuedCount = _bewegungen.Count(x => x.Model.Queued);
                UpdateGesamteTruppenverwendet();
                OnPropertyChanged("");
            }
        }

        private void AddNewBewegung() {
            if (SelectedSenderVillage != null && SelectedEmpfaengerVillage != null && SelectedSenderVillage != SelectedEmpfaengerVillage) {
                var truppenbewegung = new TruppenbewegungViewModel(SelectedSenderVillage, SelectedEmpfaengerVillage);
                if (!string.IsNullOrEmpty(SameBewegungsart))
                    truppenbewegung.BewegungsartSelected = SameBewegungsart;
                if (!string.IsNullOrEmpty(SameSpeertraegerAnzahl))
                    truppenbewegung.Speertraeger = SameSpeertraegerAnzahl;
                if (!string.IsNullOrEmpty(SameSchwertkaempferAnzahl))
                    truppenbewegung.Schwertkaempfer = SameSchwertkaempferAnzahl;
                if (!string.IsNullOrEmpty(SameAxtkaempferAnzahl))
                    truppenbewegung.Axtkaempfer = SameAxtkaempferAnzahl;
                if (!string.IsNullOrEmpty(SameBogenschuetzenAnzahl))
                    truppenbewegung.Bogenschuetzen = SameBogenschuetzenAnzahl;
                if (!string.IsNullOrEmpty(SameSpaeherAnzahl))
                    truppenbewegung.Spaeher = SameSpaeherAnzahl;
                if (!string.IsNullOrEmpty(SameLeichteKavallerieAnzahl))
                    truppenbewegung.LeichteKavallerie = SameLeichteKavallerieAnzahl;
                if (!string.IsNullOrEmpty(SameBeritteneBogenschuetzenAnzahl))
                    truppenbewegung.BeritteneBogenschuetzen = SameBeritteneBogenschuetzenAnzahl;
                if (!string.IsNullOrEmpty(SameSchwereKavallerieAnzahl))
                    truppenbewegung.SchwereKavallerie = SameSchwereKavallerieAnzahl;
                if (!string.IsNullOrEmpty(SameRammboeckeAnzahl))
                    truppenbewegung.Rammboecke = SameRammboeckeAnzahl;
                if (!string.IsNullOrEmpty(SameKatapulteAnzahl))
                    truppenbewegung.Katapulte = SameKatapulteAnzahl;
                if (!string.IsNullOrEmpty(SamePaladinAnzahl))
                    truppenbewegung.Paladin = SamePaladinAnzahl;
                if (!string.IsNullOrEmpty(SameAdelsgeschlechtAnzahl))
                    truppenbewegung.Adelsgeschlechter = SameAdelsgeschlechtAnzahl;
                if (!string.IsNullOrEmpty(SameAnkunftszeit))
                    truppenbewegung.Ankunftszeitpunkt = SameAnkunftszeit;
                _bewegungen.Add(truppenbewegung);
                _accountsettings.GeplanteBewegungen.Add(truppenbewegung.Model);
                AttacksToSendCount = _bewegungen.Count(x => x.Model.Active);
                AttacksSentCount = _bewegungen.Count(x => x.Model.Sent);
                AttacksQueuedCount = _bewegungen.Count(x => x.Model.Queued);
                UpdateGesamteTruppenverwendet();
                OnPropertyChanged("");
            }
        }

        private async void Remove() {
            if (await MessageDialog.ShowAsync(Resources.RemoveTheselectedAttacks, Resources.Achtung, MessageBoxButton.YesNo, MessageDialogType.Accent, _window) == MessageBoxResult.No)
                return;
            var gesamt = SelectedBewegungen.Count;
            var xx = SelectedBewegungen.ToList();
            for (var i = 0; i < gesamt; i++) {
                _accountsettings.GeplanteBewegungen.Remove(xx[i].Model);
                _bewegungen.Remove(xx[i]);
            }
            UpdateGesamteTruppenverwendet();
            OnPropertyChanged("");
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private async Task LoadPlayerVillages() {
            await Task.Run(() => {
                               ProgressRingActive = true;
                               try {
                                   _villagelistActualAccount.Clear();
                                   foreach (var liste in App.Settings.Accountsettingslist.Select(x => x.DoerferSettings.Where(y => y.Dorf.PlayerId.Equals(x.Playerid) && (SelectedSenderGruppe?.DoerferIds.Any(k => k.Equals(y.Dorf.VillageId)) ?? true)))
                                       ) {
                                       foreach (var dorf in liste.OrderBy(x => x.Dorf.Name))
                                           _villagelistActualAccount.Add(dorf.Dorf);
                                   }
                               } catch {
                                       App.LogString(Resources.FailureLoadingPlayervillages);
                               }
                               if (_villagelistActualAccount.Any())
                                   SelectedSenderVillage = _villagelistActualAccount.FirstOrDefault();
                               OnPropertyChanged("");
                               ProgressRingActive = false;
                           });
        }

        public void Refresh() {
            foreach (var bewegung in _bewegungen)
                bewegung.Refresh();
            UpdateGesamteTruppenverwendet();
            OnPropertyChanged("");
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
