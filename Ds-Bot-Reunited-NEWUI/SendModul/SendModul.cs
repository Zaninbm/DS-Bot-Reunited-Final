﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.SendModul {
    public static class SendModul {
        public static async Task<string> SendAttackOverPlace(Browser browser, List<TroopTemplate> troops,
            DorfViewModel senderDorf, DateTime time, bool genautimen, string kattaziel = "") {
            try {
                string error = await SendOverPlace(browser.WebDriver, troops, senderDorf.VillagePoints, senderDorf.VillageId);
                if (!string.IsNullOrEmpty(error) && !error.Contains("Fill up Troops")) {
                    return "troopcount too low of " + error;
                }
                if (!string.IsNullOrEmpty(error)) {
                    return error;
                }
                await Versammlungsplatzmodul.PressAngreifen(browser, kattaziel);
                List<IWebElement> errorlist = browser.WebDriver.FindElements(By.ClassName("error_box")).ToList();
                if (errorlist.Any()) {
                    return errorlist.FirstOrDefault()?.Text;
                }
                return "";
            } catch (Exception) {
                return Resources.BotcaptchaError;
            }
        }

        public static async Task<string> SendSupportOverPlace(Browser browser, List<TroopTemplate> troops, DorfViewModel senderDorf, DateTime time, bool genautimen) {
            try {
                string error = await SendOverPlace(browser.WebDriver, troops, 0, senderDorf.VillageId);
                if (!string.IsNullOrEmpty(error) && !error.Contains("Fill up Troops")) {
                    return "troopcount too low of " + error;
                }
                await Versammlungsplatzmodul.PressUnterstuetzen(browser);
                List<IWebElement> errorlist = browser.WebDriver.FindElements(By.ClassName("error_box")).ToList();
                if (errorlist.Any()) {
                    return errorlist.FirstOrDefault()?.Text;
                }
                return "";
            } catch (Exception){
                return Resources.BotcaptchaError;
            }
        }

        public static async Task<string> SendOverPlace(IWebDriver webDriver, List<TroopTemplate> troops, int punkte, int villageid) {
            int mintroops = punkte > 0 ? punkte / 100 : 0;
            int actualtroops = 0;

            foreach (TroopTemplate troop in troops) {
                if (troop.Amount > 0) {
                    int maxvalue = GetMaxValue(webDriver, troop);
                    if (maxvalue == 0) {
                        return troop.Unit;
                    }
                    if (maxvalue < troop.Amount) {
                        troop.Amount = maxvalue;
                    }
                    actualtroops += troop.Amount * GetBhPlaetzeForUnit(troop.Unit);
                    //SetAvailable(troop.Unit, villageid, troop.Amount);
                }
            }
            await Versammlungsplatzmodul.FillKoordsAndTroops(webDriver, 0, 0, GetTemplateOutOfTroopTemplate(troops));
            if (actualtroops < mintroops) {
                return await MitTruppenAuffuellen(webDriver, mintroops - actualtroops, troops, villageid);
            }
            return "";
        }

        private static async Task<string> MitTruppenAuffuellen(IWebDriver webDriver, int bhPlaetzeToFill, List<TroopTemplate> troops,
            int villageid) {
            string error = "";
            int bhplaetze = bhPlaetzeToFill;
            List<TroopTemplate> template = new List<TroopTemplate>(troops);
            template.Remove(template.FirstOrDefault(x => x.Unit.Equals("snob")));
            template.Remove(template.FirstOrDefault(x => x.Unit.Equals("knight")));
            foreach (var troop in template) {
                if (troop.Amount == 0) {
                    int maxvalue = GetMaxValue(webDriver, troop);
                    int maxbhplaetze = maxvalue * GetBhPlaetzeForUnit(troop.Unit);
                    int amounts = maxvalue;
                    if (maxbhplaetze > bhplaetze) {
                        int amount = (bhplaetze / GetBhPlaetzeForUnit(troop.Unit)) + 1;
                        amounts = amount;
                    }
                    troop.Amount = amounts;
                    bhplaetze -= (maxvalue * GetBhPlaetzeForUnit(troop.Unit));
                    //SetAvailable(troop.Unit, villageid, int.Parse(amounts));
                }
                if (bhplaetze <= 0) {
                    return error;
                }
            }
            await Versammlungsplatzmodul.FillKoordsAndTroops(webDriver, 0, 0, GetTemplateOutOfTroopTemplate(template));
            if (bhplaetze > 0) {
                error += "Fill up Troops Error";
            }
            return error;
        }

        private static Farmen.TruppenTemplate GetTemplateOutOfTroopTemplate(List<TroopTemplate> template) {
            Farmen.TruppenTemplate templatee = new TruppenTemplate();
            if (template.Any(x => x.Unit.Equals("spear")))
                templatee.SpeertraegerAnzahl = template.FirstOrDefault(x => x.Unit.Equals("spear")).Amount;

            if (template.Any(x => x.Unit.Equals("sword")))
                templatee.SchwertkaempferAnzahl = template.FirstOrDefault(x => x.Unit.Equals("sword")).Amount;
            if (template.Any(x => x.Unit.Equals("axe")))
                templatee.AxtkaempferAnzahl = template.FirstOrDefault(x => x.Unit.Equals("axe")).Amount;
            if (template.Any(x => x.Unit.Equals("archer")))
                templatee.BogenschuetzenAnzahl = template.FirstOrDefault(x => x.Unit.Equals("archer")).Amount;
            if (template.Any(x => x.Unit.Equals("spy")))
                templatee.SpaeherAnzahl = template.FirstOrDefault(x => x.Unit.Equals("spy")).Amount;
            if (template.Any(x => x.Unit.Equals("light")))
                templatee.LeichteKavallerieAnzahl = template.FirstOrDefault(x => x.Unit.Equals("light")).Amount;
            if (template.Any(x => x.Unit.Equals("marcher")))
                templatee.BeritteneBogenschuetzenAnzahl = template.FirstOrDefault(x => x.Unit.Equals("marcher")).Amount;
            if (template.Any(x => x.Unit.Equals("heavy")))
                templatee.SchwereKavallerieAnzahl = template.FirstOrDefault(x => x.Unit.Equals("heavy")).Amount;
            if (template.Any(x => x.Unit.Equals("ram")))
                templatee.RammboeckeAnzahl = template.FirstOrDefault(x => x.Unit.Equals("ram")).Amount;
            if (template.Any(x => x.Unit.Equals("cat")))
                templatee.KatapulteAnzahl = template.FirstOrDefault(x => x.Unit.Equals("cat")).Amount;
            if (template.Any(x => x.Unit.Equals("knight")))
                templatee.PaladinAnzahl = template.FirstOrDefault(x => x.Unit.Equals("knight")).Amount;
            if (template.Any(x => x.Unit.Equals("snob")))
                templatee.AdelsgeschlechterAnzahl = template.FirstOrDefault(x => x.Unit.Equals("snob")).Amount;
            return templatee;
        }

        //private static void SetAvailable(string unit, int villageid, int amount) {
        //    AvailableTroopsPerDorf availableTroops =
        //        App.AvailableTroops.RealAvailableTroopsForActualAccount.FirstOrDefault(x => x.VillageId == villageid);
        //    if (availableTroops != null) {
        //        switch (unit) {
        //            case "spear":
        //                availableTroops.SpeertraegerAnzahl = availableTroops.SpeertraegerAnzahl - amount;
        //                break;
        //            case "sword":
        //                availableTroops.SchwertkaempferAnzahl = availableTroops.SchwertkaempferAnzahl - amount;
        //                break;
        //            case "axe":
        //                availableTroops.AxtkaempferAnzahl = availableTroops.AxtkaempferAnzahl - amount;
        //                break;
        //            case "archer":
        //                availableTroops.BogenschuetzenAnzahl = availableTroops.BogenschuetzenAnzahl - amount;
        //                break;
        //            case "spy":
        //                availableTroops.SpaeherAnzahl = availableTroops.SpaeherAnzahl - amount;
        //                break;
        //            case "light":
        //                availableTroops.LeichteKavallerieAnzahl = availableTroops.LeichteKavallerieAnzahl - amount;
        //                break;
        //            case "marcher":
        //                availableTroops.BeritteneBogenschuetzenAnzahl = availableTroops.BeritteneBogenschuetzenAnzahl -
        //                                                                amount;
        //                break;
        //            case "heavy":
        //                availableTroops.SchwereKavallerieAnzahl = availableTroops.SchwereKavallerieAnzahl - amount;
        //                break;
        //            case "ram":
        //                availableTroops.RammboeckeAnzahl = availableTroops.RammboeckeAnzahl - amount;
        //                break;
        //            case "cat":
        //                availableTroops.KatapulteAnzahl = availableTroops.KatapulteAnzahl - amount;
        //                break;
        //            case "knight":
        //                availableTroops.PaladinAnzahl = availableTroops.PaladinAnzahl - amount;
        //                break;
        //            case "snob":
        //                availableTroops.AdelsgeschlechterAnzahl = availableTroops.AdelsgeschlechterAnzahl - amount;
        //                break;
        //        }
        //    }
        //}

        private static int GetBhPlaetzeForUnit(string unit) {
            switch (unit) {
                case "spear":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SPEERTRAEGER;
                case "sword":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SCHWERTKAEMPFER;
                case "axe":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_AXTKAEMPFER;
                case "archer":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_BOGENSCHUETZEN;
                case "spy":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SPAEHER;
                case "light":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_LEICHTEKAVALLERIE;
                case "marcher":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_BERITTENEBOGENSCHUETZEN;
                case "heavy":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SCHWEREKAVALLERIE;
                case "ram":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_RAMMBOECKE;
                case "cat":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_KATAPULTE;
                case "knight":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_PALADIN;
                case "snob":
                    return TRUPPENCONSTANTS.BAUERNHOFPLAETZE_ADELSGESCHLECHT;
            }
            return 0;
        }

        private static int GetMaxValue(IWebDriver webDriver, TroopTemplate troop) {
            int maxvalue = 0;
            try {
                switch (troop.Unit) {
                    case "spear":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.SPEERTRAEGER_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "sword":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.SCHWERTKAEMPFER_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "axe":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.AXTKAEMPFER_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "archer":
                        if (App.Settings.Weltensettings.BogenschuetzenActive)
                            maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.BOGENSCHUETZEN_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "spy":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.SPAEHER_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "light":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.LEICHTEKAVALLERIE_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "marcher":
                        if (App.Settings.Weltensettings.BogenschuetzenActive)
                            maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.BERITTENEBOGENSCHUETZEN_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "heavy":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.SCHWEREKAVALLERIE_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "ram":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.RAMMBOECKE_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "cat":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.KATAPULTE_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "knight":
                        if (App.Settings.Weltensettings.PaladinActive)
                            maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.PALADIN_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                    case "snob":
                        maxvalue = int.Parse(webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.ADELSGESCHLECHT_ALLE_SELECTOR_XPATH)).Text.Replace("(", "").Replace(")", ""));
                        break;
                }
                return maxvalue;
            } catch {
                return 0;
            }
        }
    }
}