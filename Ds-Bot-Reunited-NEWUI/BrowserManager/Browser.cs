﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Ds_Bot_Reunited_NEWUI.BrowserManager {
    [Serializable]
    public class Handles {
        public string Handle { get; set; }
        public bool BeingUsed { get; set; }
        public IntPtr Pointer { get; set; }
        public bool Haupthandle { get; set; }
    }

    [Serializable]
    public class Browser {
        public string Accountname { get; set; }
        public IWebDriver WebDriver { get; set; }
        public bool GetsUsed { get; set; }
        public int FensterOffen { get; set; }
        public bool Active { get; set; }
        public List<Handles> Handles { get; set; }
        public ChromeDriverService Service { get; set; }
        public IJavaScriptExecutor JavascriptExecutor { get; set; }

        public Browser() {
            Handles = new List<Handles>();
        }

        public void Destroy(bool destroy) {
            if (destroy) {
                WebDriver?.Quit();
            }
            Service?.Dispose();
            GetsUsed = false;
            FensterOffen = 0;
            Accountname = "";
        }
    }
}
