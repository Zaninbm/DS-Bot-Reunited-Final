﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.RegistrierungsModul {
    public static class RegistrierungsModul {
        private static List<string> _proxies;
        private static string _lastProxyUsed;
        private static List<string> _proxiesToUse;

        private class Account {
            public string Accountname { get; set; }
            public string Passwort { get; set; }
            public string Email { get; set; }
            public DateTime Registierungsdatum { get; set; }
            public string Proxy { get; set; }
        }

        private class Email {
            public string Name { get; set; }
            public string Passwort { get; set; }
        }


        public static string GetProxy() {
            if (File.Exists("proxies.txt")) {
                if(_proxies == null)
                    _proxies = File.ReadAllLines("proxies.txt").ToList();
                if (_proxiesToUse == null) {
                    _proxiesToUse = new List<string>();
                }
                if (!_proxiesToUse.Any()) {
                    foreach (var proxy in _proxies.OrderBy(x => x)) {
                        int lastindex = proxy.LastIndexOf('.');
                        string ipstamm = proxy.Substring(0, proxy.Length - (proxy.Length - lastindex));
                        if (!_proxiesToUse.Any(x => x.Equals(ipstamm))) {
                            _proxiesToUse.Add(ipstamm);
                        }
                    }
                }
                string proxyString = "";

                if (string.IsNullOrEmpty(_lastProxyUsed) && App.Settings.Accountsettingslist.Any(x => x.MultiaccountingDaten.Accountmode == Accountmode.Botaccount)) {
                    _lastProxyUsed = App.Settings.Accountsettingslist.Last(x => x.MultiaccountingDaten.Accountmode == Accountmode.Botaccount).ProxyEins;
                }
                if (string.IsNullOrEmpty(_lastProxyUsed)) {
                    proxyString = _proxies.FirstOrDefault(x => x.StartsWith(_proxiesToUse.FirstOrDefault()));
                } else {
                    int lastindex = _lastProxyUsed.LastIndexOf('.');
                    string ipstamm = _lastProxyUsed.Substring(0, _lastProxyUsed.Length - (_lastProxyUsed.Length - lastindex));
                    int lastIndex = _proxiesToUse.IndexOf(ipstamm) + 1;
                    proxyString = _proxies.FirstOrDefault(x => x.StartsWith(_proxiesToUse[lastIndex]));
                    _proxies.Remove(proxyString);
                    _lastProxyUsed = proxyString;
                }
                File.WriteAllText("proxies.txt", string.Join("\n", _proxies));
                return proxyString;
            }
            return "";
        }

        public static void Registrierungslauf() {
            try {
                if (File.Exists("accountnames.txt") && File.Exists("proxies.txt") && File.Exists("emails.txt")) {
                    List<string> accountNamen = File.ReadAllLines("accountnames.txt").ToList();
                    List<Email> emails = new List<Email>();
                    foreach (var line in File.ReadAllLines("emails.txt")) {
                        string[] splitted = line.Split(',');
                        emails.Add(new Email {Name = splitted[0], Passwort = splitted[1]});
                    }

                    if (accountNamen.Any() && _proxies.Any() && emails.Any())
                        Task.Run(async () => {
                            for (;;) {
                                if (accountNamen.Any() && _proxies.Any() && emails.Any()) {
                                    Account account = new Account {
                                        Accountname = accountNamen[0],
                                        Passwort = emails[0].Passwort,
                                        Registierungsdatum = DateTime.Now,
                                        Email = emails[0].Name,
                                        Proxy = GetProxy()
                                    };

                                    var browser = BrowserManager.BrowserManager.AddNewBrowser(null, account.Proxy,true,false,"socks5://",account.Accountname);

                                    browser.WebDriver.Navigate().GoToUrl("https://www.die-staemme.de/page/new");
                                    await Rndm.Sleep(0, 100);
                                    browser.WebDriver.FindElement(By.XPath("//input[@id='register_username']")).SendKeys(account.Accountname);
                                    await Rndm.Sleep(0, 100);
                                    browser.WebDriver.FindElement(By.XPath("//input[@id='register_password']")).SendKeys(account.Passwort);
                                    await Rndm.Sleep(0, 100);
                                    browser.WebDriver.FindElement(By.XPath("//input[@id='register_email']")).SendKeys(account.Email);
                                    await Rndm.Sleep(0, 100);
                                    browser.WebDriver.FindElement(By.XPath("//input[@id='terms']")).Click();
                                    await Rndm.Sleep(0, 100);
                                    if (browser.WebDriver.FindElement(By.XPath("//div[contains(@class,'error-message error']")).Displayed) {
                                        if (browser.WebDriver.FindElements(By.XPath("//a[@class='suggested-name']")).Any()) {
                                            browser.WebDriver.FindElements(By.XPath("//a[@class='suggested-name']"))[0].Click();
                                            await Rndm.Sleep(0, 100);
                                        }
                                    }

                                    browser.WebDriver.FindElement(By.XPath("//a[@class='btn-register']")).Click();
                                    await Rndm.Sleep(0, 100);


                                    //TODO EMAIL HOLEN!
                                    bool registriert = false;

                                    do {
                                        browser.WebDriver.Navigate().GoToUrl("https://web.de/");
                                        await Rndm.Sleep(500, 1000);
                                        browser.WebDriver.FindElement(By.XPath("//a[@class='hasIcon icon-freemail']")).Click();
                                        await Rndm.Sleep(0, 100);
                                        browser.WebDriver.FindElement(By.XPath("//input[@id='inpFreemailLoginUsername']")).SendKeys(account.Email);
                                        await Rndm.Sleep(0, 100);
                                        browser.WebDriver.FindElement(By.XPath("//input[@id='inpFreemailLoginPassword']")).SendKeys(account.Passwort);
                                        await Rndm.Sleep(0, 100);
                                        browser.WebDriver.FindElement(By.XPath("//span[@class='btn-wrapper btn-m btn-service']")).Click();
                                        await Rndm.Sleep(500, 1000);
                                        if (browser.WebDriver.ElementExists(By.ClassName("layerCloser"))) {
                                            browser.WebDriver.FindElement(By.XPath("//a[@class='layerCloser']")).Click();
                                            await Rndm.Sleep(100, 500);
                                        }
                                        browser.WebDriver.Navigate().GoToUrl(browser.WebDriver.Url.Replace("home", "mail"));
                                        await Rndm.Sleep(0, 100);
                                        browser.WebDriver.FindElement(By.XPath("//span[@class='icon folder-unknown']")).Click();
                                        await Rndm.Sleep(0, 100);

                                        var elements = browser.WebDriver.FindElements(By.XPath("//span[@class='subject']")).ToList();
                                        if (elements.Count(x => x.Text.Contains("Stämme")) > 0) {
                                            elements.FirstOrDefault(x => x.Text.Contains("Stämme"))?.Click();
                                            //TODO Link in der Mail anklicken!
                                            if(browser.WebDriver.ElementExists(By.XPath("//a[contains(@href,'validate']"))) {
                                                browser.ClickByXpath("//a[contains(@href,'validate']");
                                            }


                                            registriert = true;
                                        }
                                    } while (!registriert);

                                    accountNamen.Remove(account.Accountname);
                                    _proxies.Remove(account.Proxy);
                                    emails.RemoveAt(0);

                                    File.WriteAllText("accountnames.txt", string.Join("\n", accountNamen));
                                    File.WriteAllText("emails.txt", string.Join("\n", emails.Select(x => x.Name + "," + x.Passwort)));
                                }
                                await Task.Delay(60000 * App.Random.Next(25, 60));
                            }
                        });
                }
            } catch (Exception e) {
                App.LogString("Failure Account Registration " + e);
            }
        }
    }
}