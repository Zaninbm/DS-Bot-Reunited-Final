﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public static class WindowHider {
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hwnd, ShowWindowCommands nCmdShow);


        private enum ShowWindowCommands : uint {
            /// <summary>Hides the window and activates another window.</summary>
            /// <remarks>See SW_HIDE</remarks>
            Hide = 0,

            /// <summary>Activates and displays a window. If the window is minimized
            /// or maximized, the system restores it to its original size and
            /// position. An application should specify this flag when displaying
            /// the window for the first time.</summary>
            /// <remarks>See SW_SHOWNORMAL</remarks>
            ShowNormal = 1,

            /// <summary>Activates the window and displays it as a minimized window.</summary>
            /// <remarks>See SW_SHOWMINIMIZED</remarks>
            ShowMinimized = 2,

            /// <summary>Activates the window and displays it as a maximized window.</summary>
            /// <remarks>See SW_SHOWMAXIMIZED</remarks>
            ShowMaximized = 3,

            /// <summary>Maximizes the specified window.</summary>
            /// <remarks>See SW_MAXIMIZE</remarks>
            Maximize = 3,

            /// <summary>Displays a window in its most recent size and position.
            /// This value is similar to "ShowNormal", except the window is not
            /// actived.</summary>
            /// <remarks>See SW_SHOWNOACTIVATE</remarks>
            ShowNormalNoActivate = 4,

            /// <summary>Activates the window and displays it in its current size
            /// and position.</summary>
            /// <remarks>See SW_SHOW</remarks>
            Show = 5,

            /// <summary>Minimizes the specified window and activates the next
            /// top-level window in the Z order.</summary>
            /// <remarks>See SW_MINIMIZE</remarks>
            Minimize = 6,

            /// <summary>Displays the window as a minimized window. This value is
            /// similar to "ShowMinimized", except the window is not activated.</summary>
            /// <remarks>See SW_SHOWMINNOACTIVE</remarks>
            ShowMinNoActivate = 7,

            /// <summary>Displays the window in its current size and position. This
            /// value is similar to "Show", except the window is not activated.</summary>
            /// <remarks>See SW_SHOWNA</remarks>
            ShowNoActivate = 8,

            /// <summary>Activates and displays the window. If the window is
            /// minimized or maximized, the system restores it to its original size
            /// and position. An application should specify this flag when restoring
            /// a minimized window.</summary>
            /// <remarks>See SW_RESTORE</remarks>
            Restore = 9,

            /// <summary>Sets the show state based on the SW_ value specified in the
            /// STARTUPINFO structure passed to the CreateProcess function by the
            /// program that started the application.</summary>
            /// <remarks>See SW_SHOWDEFAULT</remarks>
            ShowDefault = 10,

            /// <summary>Windows 2000/XP: Minimizes a window, even if the thread
            /// that owns the window is hung. This flag should only be used when
            /// minimizing windows from a different thread.</summary>
            /// <remarks>See SW_FORCEMINIMIZE</remarks>
            ForceMinimized = 11
        }

        public static void HideBrowserWindows(List<IntPtr> pointer) {
            //if (AllreadyHiddenWindows == null) {
            //    AllreadyHiddenWindows = new List<IntPtr>();
            //}
            foreach (var hWnd in pointer) {
                ShowWindow(hWnd, ShowWindowCommands.Hide);
                //if (!AllreadyHiddenWindows.Any(x => x.Equals(hWnd))) {
                //    AllreadyHiddenWindows.Add(hWnd);
                //}
            }
        }

        private static void MinimizeBrowserWindows(List<IntPtr> pointer) {
            foreach (var hWnd in pointer) {
                ShowWindow(hWnd, ShowWindowCommands.Minimize);
            }
        }

        public static void ShowBrowserWindows(List<IntPtr> pointer) {
            if(pointer != null && pointer.Any())
                foreach (var hWnd in pointer) {
                    if(hWnd != null)
                        ShowWindow(hWnd, ShowWindowCommands.ShowNormal);
                }
        }

        public static List<IntPtr> GetWindowHandles(string processName) {
            Process[] processRunning = Process.GetProcesses();
            return (from pr in processRunning
                where
                pr.MainWindowTitle.Contains(processName)
                select pr.MainWindowHandle).ToList();
        }

        public static void HideAllNewChromeWindows()
        {
            List<IntPtr> list = GetWindowHandles("Untitled");
            //data:,
            if (list.Any()) {
                HideBrowserWindows(list);
            }
            list = GetWindowHandles("data:,");
            //data:,
            if (list.Any()) {
                HideBrowserWindows(list);
            }
        }

        private static IntPtr _mainWindow;

        public static void HideMainWindow() {
            HideBrowserWindows(new List<IntPtr> {_mainWindow});
        }

        public static void ShowMainWindow() {
            ShowBrowserWindows(new List<IntPtr> {_mainWindow});
        }

        public static void SetActualWindow(string welt) {
            _mainWindow = GetWindowHandles(welt).FirstOrDefault();
        }


        //public static void HideAllChromeWindows() {
        //    if (AllreadyHiddenWindows.Any()) {
        //        HideBrowserWindows(AllreadyHiddenWindows);
        //    }
        //}

        //private static List<IntPtr> AllreadyHiddenWindows;

        public static void ShowChromeWindow() {
            //if (AllreadyHiddenWindows.Any()) {
            //    ShowBrowserWindows(AllreadyHiddenWindows);
            //}
        }
    }
}