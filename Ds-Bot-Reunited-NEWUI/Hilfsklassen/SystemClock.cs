﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public static class NativeMethods {
        [StructLayout(LayoutKind.Sequential)]
        internal struct SystemTime {
            public Int16 Year;

            public Int16 Month;

            public Int16 DayOfWeek;

            public Int16 Day;

            public Int16 Hour;

            public Int16 Minute;

            public Int16 Second;

            public Int16 Millisecond;
        }


        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "SetSystemTime")]
        internal static extern int SetSystemTime(ref SystemTime s);


        [DllImport("netapi32.dll", SetLastError = true, EntryPoint = "NetRemoteTOD", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetRemoteTime(string UncServerName, ref IntPtr BufferPtr);


        [DllImport("netapi32.dll", SetLastError = true, EntryPoint = "NetApiBufferFree")]
        internal static extern int NetApiBufferFree(IntPtr bufptr);
    }


    /// <summary>
    /// Statische Klasse zur Behandlung der lokalen Systemuhr
    /// </summary>
    public static class SystemClock {
        [StructLayout(LayoutKind.Sequential)]
        private struct TimeOfDayInfo {
            public int Elapsed;

            public int Milliseconds;

            public int Hours;

            public int Minutes;

            public int Seconds;

            public int Hundredth;

            public int Timezone;

            public int Interval;

            public int Day;

            public int Month;

            public int Year;

            public int Weekday;
        }
        
        public static DateTime Clock {
            get {
                return DateTime.Now;
            }

            [SecurityPermission(SecurityAction.LinkDemand)]
            set {
                if (value.Kind != DateTimeKind.Utc)

                    value = value.ToUniversalTime();


                NativeMethods.SystemTime s = new NativeMethods.SystemTime();

                s.Year = Convert.ToInt16(value.Year);

                s.Month = Convert.ToInt16(value.Month);

                s.Day = Convert.ToInt16(value.Day);

                s.Hour = Convert.ToInt16(value.Hour);

                s.Minute = Convert.ToInt16(value.Minute);

                s.Second = Convert.ToInt16(value.Second);

                s.Millisecond = Convert.ToInt16(value.Millisecond);

                s.DayOfWeek = Convert.ToInt16(value.DayOfWeek, CultureInfo.InvariantCulture);


                int result = NativeMethods.SetSystemTime(ref s);

                int error = Marshal.GetLastWin32Error();

                if (result != 1) {
                    throw new Win32Exception(error);
                }
            }
        }
    }
}
