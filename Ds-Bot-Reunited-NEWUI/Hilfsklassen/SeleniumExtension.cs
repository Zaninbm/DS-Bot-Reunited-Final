﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
// ReSharper disable UnusedMember.Global

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public static class SeleniumExtensions {
        private static void SetJavascriptExecutor(this Browser browser) {
            if(browser.JavascriptExecutor == null)
                browser.JavascriptExecutor = browser.WebDriver as IJavaScriptExecutor;
        }

        //https://github.com/tehmarken/SeleniumEFWExtensions/blob/master/Selenium.EFWExtensions/SeleniumExtensions.cs
        public static void ScrollToElement(this Browser browser, IWebElement element) {
            SetJavascriptExecutor(browser);
            browser.JavascriptExecutor.ExecuteScript("$('#chat-wrapper').hide();");
            browser.JavascriptExecutor.ExecuteScript($"window.scrollTo(0,{element.Location.Y - browser.WebDriver.Manage().Window.Size.Height / 2 + element.Size.Height / 2});");
        }

        public static IWebElement ByName(this IWebDriver driver, string name) {
            return driver.FindElement(By.Name(name));
        }

        public static bool ClickElement(this Browser browser, IWebElement element) {
            try {
                SetJavascriptExecutor(browser);
                if (element != null) {
                    browser.ScrollToElement(element);
                    element.Click();
                    return true;
                }
            } catch (Exception e) {
                App.LogString("Failure Click by Element " + e);
            }
            return false;
        }

        public static bool ClickByXpath(this Browser browser, string xpath, int id = 0) {
            try {
                SetJavascriptExecutor(browser);
                var element = browser.WebDriver.FindElements(By.XPath(xpath))[id];
                browser.ScrollToElement(element);
                element.Click();
                return true;
            } catch (Exception e) {
                App.LogString("Failure Click by XPath " + e);
            }
            return false;
        }

	    // ReSharper disable once UnusedMember.Global
	    public static bool ClickById(this Browser browser, string id) {
            try {
                SetJavascriptExecutor(browser);
                var element = browser.WebDriver.FindElement(By.Id(id));
                browser.ScrollToElement(element);
                element.Click();
                return true;
            } catch (Exception e) {
                App.LogString("Failure Click by Id " + e);
            }
            return false;
        }

        // ReSharper disable once UnusedMember.Global
        public static bool ClickByCss(this Browser browser, string id)
        {
            try
            {
                SetJavascriptExecutor(browser);
                var element = browser.WebDriver.FindElement(By.CssSelector(id));
                browser.ScrollToElement(element);
                element.Click();
                return true;
            }
            catch (Exception e)
            {
                App.LogString("Failure Click by Id " + e);
            }
            return false;
        }

        // ReSharper disable once UnusedMember.Global
        public static int DistanceFromTopOfViewport(this IWebDriver driver, IWebElement element) {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var dist = (Int64) js.ExecuteScript("return arguments[0] - $(window)['scrollTop']()", element.Location.Y);
            return Convert.ToInt32(dist);
        }

        public static void WaitForAjax(this IWebDriver driver, int sec = 15) {
            new WebDriverWait(driver, TimeSpan.FromSeconds(sec)).Until(d => d.ExecuteJavaScript<bool>("return jQuery.active == 0"));
        }

        public static void WaitForPageload(this IWebDriver driver, int sec = 15) {
            new WebDriverWait(driver, TimeSpan.FromSeconds(sec)).Until(d => d.ExecuteJavaScript<string>("return document.readyState").Equals("complete"));
        }

        public static async Task WaitASec(this IWebDriver driver, int sec = 1) {
            await Task.Delay(sec * 1000);
        }

        public static void WaitForTextInElement(this IWebDriver driver, IWebElement element, string text, int sec = 15) {
            new WebDriverWait(driver, TimeSpan.FromSeconds(sec)).Until(d => element.Text.Contains(text));
        }

        public static bool ElementExists(this IWebDriver driver, By byLocator) {
            var elements = driver.FindElements(byLocator);
            var count = elements.Count(x=>x.Displayed);
            return count > 0;
        }

        public static void SelectBySubText(this SelectElement selectElement, string subText) {
            foreach (var option in selectElement.Options.Where(option => option.Text.Contains(subText))) {
                option.Click();
                return;
            }
            selectElement.SelectByText(subText);
        }

        public static IWebElement FindElementByText(this IWebDriver driver, string text) {
            var elem = driver.FindElement(By.XPath($"//*[text()='{text}']"));
            return elem;
        }

        public static IWebElement FindElementByText(this IWebElement webElement, string text) {
            var elem = webElement.FindElement(By.XPath($"//*[text()='{text}']"));
            return elem;
        }

        public static void SelectOptionByStartsWithText(this IWebElement selectWebElement, string startsWithText) {
            selectWebElement.FindElement(By.XPath($"./option[starts-with(text(), '{startsWithText}')]")).Click();
        }
    }
}
