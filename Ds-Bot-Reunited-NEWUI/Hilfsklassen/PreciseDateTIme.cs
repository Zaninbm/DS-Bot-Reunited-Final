﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public class PreciseDatetime {
        private static DateTime _dateTime;

        // using DateTime.Now resulted in many many log events with the same timestamp.

        // use static variables in case there are many instances of this class in use in the same program

        // (that way they will all be in sync)

        private Stopwatch _stopwatch;

        private Stopwatch _stopwatchNow;

        private Stopwatch _stopwatch2;
        
        //private static TimeSpan _offset;

        //[DllImport("kernel32.dll")]
        //public static extern bool SetSystemTime(ref SYSTEMTIME st);
        const string NtpServer = "pool.ntp.org";

        const int DaysTo1900 = 1900 * 365 + 95; // 95 = offset for leap-years etc.
        const long TicksPerSecond = 10000000L;
        const long TicksPerDay = 24 * 60 * 60 * TicksPerSecond;
        const long TicksTo1900 = DaysTo1900 * TicksPerDay;
        private static long _elapsedPing;

        public void GetNetworkTime() {
            if (_stopwatch2 == null)
                _stopwatch2 = new Stopwatch();
            var ntpData = new byte[48];
            ntpData[0] = 0x1B; // LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)
            var addresses = Dns.GetHostEntry(NtpServer).AddressList;
            var ipEndPoint = new IPEndPoint(addresses[0], 123);
            long pingDuration = Stopwatch.GetTimestamp(); // temp access (JIT-Compiler need some time at first call)
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
                socket.Connect(ipEndPoint);
                socket.ReceiveTimeout = 5000;
                socket.Send(ntpData);
                pingDuration = Stopwatch.GetTimestamp(); // after Send-Method to reduce WinSocket API-Call time

                socket.Receive(ntpData);
                pingDuration = Stopwatch.GetTimestamp() - pingDuration;
            }

            long pingTicks = pingDuration * TicksPerSecond / Stopwatch.Frequency;

            // optional: display response-time
            //Debug.WriteLine("{0:N2} ms", new TimeSpan(pingTicks).TotalMilliseconds);

            long intPart = (long) ntpData[40] << 24 | (long) ntpData[41] << 16 | (long) ntpData[42] << 8 | ntpData[43];
            long fractPart = (long) ntpData[44] << 24 | (long) ntpData[45] << 16 | (long) ntpData[46] << 8 | ntpData[47];
            long netTicks = intPart * TicksPerSecond + (fractPart * TicksPerSecond >> 32);

            _dateTime = new DateTime(TicksTo1900 + netTicks + pingTicks);
        }

        public PreciseDatetime() {
            if (_stopwatch == null) {
                _stopwatch = new Stopwatch();
            }
            _dateTime = NtpClient.GetTime(NtpServer).TransmitTimestamp;
            _stopwatch.Start();
            try {
                SystemClock.Clock = _dateTime;
            } catch (Exception) {
                // ignored
            }

            Update();
        }

        void Update() {
            var task = new Task(async () => {
                                    while (true) {
                                        if (_stopwatch.ElapsedMilliseconds > 5 * 60000) {
                                            _dateTime = NtpClient.GetTime(NtpServer).TransmitTimestamp;
                                            _stopwatch.Restart();
                                            try {
                                                SystemClock.Clock = _dateTime;
                                            } catch (Exception) {
                                                // ignored
                                            }
                                        }
                                        await Task.Delay(100);
                                    }
                                }, TaskCreationOptions.LongRunning);
            task.Start();
        }

        public DateTime Now => GetDateTime();
        public DateTime NowExact => HighResolutionDateTime.IsAvailable ? HighResolutionDateTime.UtcNow : _dateTime.AddMilliseconds(_stopwatch.ElapsedMilliseconds);


        private DateTime _actualTime = DateTime.Now;

        private DateTime GetDateTime() {
            if (_stopwatchNow == null)
                _stopwatchNow = Stopwatch.StartNew();
            if (_stopwatchNow.ElapsedMilliseconds > 5000) {
                _actualTime = DateTime.Now;
                _stopwatchNow.Restart();
            }
            return _actualTime;
        }
    }
}
