﻿namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public struct VillageTuple<T1, T2> {
        public readonly T1 XCoordinate;
        public readonly T2 YCoordinate;

        public VillageTuple(T1 xCoordinate, T2 yCoordinate) {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }
    }

    public struct PlayerTuple<T1, T2> {
        public readonly T1 Playername;
        public readonly T2 PlayerId;

        public PlayerTuple(T1 playername, T2 playerId) {
            Playername = playername;
            PlayerId = playerId;
        }
    }
}