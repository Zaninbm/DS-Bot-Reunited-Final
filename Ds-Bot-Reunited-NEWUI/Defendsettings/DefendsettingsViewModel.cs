﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.DefendModul;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;

namespace Ds_Bot_Reunited_NEWUI.Defendsettings {
    public class DefendsettingsViewModel : INotifyPropertyChanged {
        private ICommand _defendTemplateEntfernenCommand;
        private ICommand _defendTemplateHinzufuegenCommand;
        private ICommand _retimeTemplateEntfernenCommand;
        private ICommand _retimeTemplateHinzufuegenCommand;
        private ICommand _tabTemplateEntfernenCommand;
        private ICommand _tabTemplateHinzufuegenCommand;

        public bool BogenschuetzenEnabled => App.Settings.Weltensettings.BogenschuetzenActive;
        public bool PaladinEnabled => App.Settings.Weltensettings.PaladinActive;

        public bool AggressiveEnabled => DefendArt.Equals(DefendArt.Aggressive) && IsEnabled;
        public bool DefensiveEnabled => DefendArt.Equals(DefendArt.Defensive) && IsEnabled;
        public bool NeutralEnabled => DefendArt.Equals(DefendArt.Neutral) && IsEnabled;
        public bool IsEnabled => /*App.Permissions.DefendingIsEnabled*/false;

        public Window Window { get; set; }
        public List<DefendArt> Defendmodes { get; set; }

        public DefendArt DefendArt {
            get => App.Settings.DefendArt;
            set {
                App.Settings.DefendArt = value;
                OnPropertyChanged("");
            }
        }

        public bool DetectActive {
            get => App.Settings.DetectActive;
            set {
                App.Settings.DetectActive = value;
                OnPropertyChanged();
            }
        }

        public bool RenameActive {
            get => App.Settings.RenameActive;
            set {
                App.Settings.RenameActive = value;
                OnPropertyChanged();
            }
        }

        public int AttackDetectionInterval {
            get => App.Settings.AttackdetectionInterval;
            set {
                App.Settings.AttackdetectionInterval = value;
                OnPropertyChanged();
            }
        }

        public bool PreventActive {
            get => App.Settings.PreventActive;
            set {
                App.Settings.PreventActive = value;
                OnPropertyChanged();
            }
        }

        public bool SendouttroopsActive {
            get => App.Settings.SendouttroopsActive;
            set {
                App.Settings.SendouttroopsActive = value;
                OnPropertyChanged();
            }
        }

        public int NeighbourMaxRadius {
            get => App.Settings.NeighbourMaxRadius;
            set {
                App.Settings.NeighbourMaxRadius = value;
                OnPropertyChanged();
            }
        }

        public bool RetimeActive {
            get => App.Settings.RetimeActive;
            set {
                App.Settings.RetimeActive = value;
                OnPropertyChanged();
            }
        }

        public int RetimeMaxLaufzeit {
            get => App.Settings.RetimeMaxLaufzeit;
            set {
                App.Settings.RetimeMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int UpdateUnitsinterval {
            get => App.Settings.UpdateUnitsinterval;
            set {
                App.Settings.UpdateUnitsinterval = value;
                OnPropertyChanged();
            }
        }

        public bool EmptyRessourcesActive {
            get => App.Settings.EmptyRessourcesActive;
            set {
                App.Settings.EmptyRessourcesActive = value;
                OnPropertyChanged();
            }
        }

        public bool RecruitToEmptyRessourcesActive {
            get => App.Settings.RecruitToEmptyRessourcesActive;
            set {
                App.Settings.RecruitToEmptyRessourcesActive = value;
                OnPropertyChanged();
            }
        }

        public bool BuildToEmptyRessourcesActive {
            get => App.Settings.BuildToEmptyRessourcesActive;
            set {
                App.Settings.BuildToEmptyRessourcesActive = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<TruppenTemplate> TabbingTemplates {
            get => App.Settings.TabbingTemplates;
            set {
                App.Settings.TabbingTemplates = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate SelectedTabbingTemplate { get; set; }

        public AsyncObservableCollection<TruppenTemplate> DefendingTemplates {
            get => App.Settings.DefendingTemplates;
            set {
                App.Settings.DefendingTemplates = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate SelectedDefendingTemplate { get; set; }

        public AsyncObservableCollection<TruppenTemplate> RetimeTemplates {
            get => App.Settings.RetimeTemplates;
            set {
                App.Settings.RetimeTemplates = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate SelectedRetimeTemplate { get; set; }

        public AsyncObservableCollection<TruppenTemplate> HoldBackTemplates {
            get => App.Settings.HoldBackTemplates;
            set {
                App.Settings.HoldBackTemplates = value;
                OnPropertyChanged();
            }
        }

        public ICommand TabTemplateHinzufuegenCommand => _tabTemplateHinzufuegenCommand ?? (_tabTemplateHinzufuegenCommand = new RelayCommand(TabTemplateHinzufuegen));

        public ICommand TabTemplateEntfernenCommand => _tabTemplateEntfernenCommand ?? (_tabTemplateEntfernenCommand = new RelayCommand(TabTemplateEntfernen));

        public ICommand DefendTemplateHinzufuegenCommand => _defendTemplateHinzufuegenCommand ?? (_defendTemplateHinzufuegenCommand = new RelayCommand(DefendTemplateHinzufuegen));

        public ICommand DefendTemplateEntfernenCommand => _defendTemplateEntfernenCommand ?? (_defendTemplateEntfernenCommand = new RelayCommand(DefendTemplateEntfernen));

        public ICommand RetimeTemplateHinzufuegenCommand => _retimeTemplateHinzufuegenCommand ?? (_retimeTemplateHinzufuegenCommand = new RelayCommand(RetimeTemplateHinzufuegen));

        public ICommand RetimeTemplateEntfernenCommand => _retimeTemplateEntfernenCommand ?? (_retimeTemplateEntfernenCommand = new RelayCommand(RetimeTemplateEntfernen));


        public event PropertyChangedEventHandler PropertyChanged;


        public DefendsettingsViewModel(Window window) {
            Window = window;
            Defendmodes = new List<DefendArt> {DefendArt.Aggressive, DefendArt.Neutral, DefendArt.Defensive};
        }

        private void TabTemplateHinzufuegen() {
            var counter = 0;
            if (TabbingTemplates.Any()) {
                counter = TabbingTemplates.Count;
            }

            TabbingTemplates.Add(new TruppenTemplate {TemplateName = Resources.Tabbing + (counter > 0 ? counter.ToString() : "")});
        }

        private void TabTemplateEntfernen() {
            if (SelectedTabbingTemplate != null && !SelectedTabbingTemplate.TemplateName.Equals(Resources.Tabbing)) {
                TabbingTemplates.Remove(SelectedTabbingTemplate);
            }
        }

        private void DefendTemplateHinzufuegen() {
            var counter = 0;
            if (DefendingTemplates.Any()) {
                counter = DefendingTemplates.Count;
            }

            DefendingTemplates.Add(new TruppenTemplate {TemplateName = Resources.Defending + (counter > 0 ? counter.ToString() : "")});
            DefendingTemplates.Add(new TruppenTemplate {TemplateName = Resources.DefendMinTemplate + (counter > 0 ? counter.ToString() : "")});
            DefendingTemplates.Add(new TruppenTemplate {TemplateName = Resources.DefendMaxTemplate + (counter > 0 ? counter.ToString() : "")});
        }

        private void DefendTemplateEntfernen() {
            if (SelectedDefendingTemplate != null && !SelectedDefendingTemplate.TemplateName.Equals(Resources.DefendMinTemplate) && !SelectedDefendingTemplate.TemplateName.Equals(Resources.DefendMaxTemplate) &&
                !SelectedDefendingTemplate.TemplateName.Equals(Resources.Defending)) {
                DefendingTemplates.Remove(SelectedDefendingTemplate);
            }
        }

        private void RetimeTemplateHinzufuegen() {
            var counter = 0;
            if (RetimeTemplates.Any()) {
                counter = RetimeTemplates.Count;
            }

            RetimeTemplates.Add(new TruppenTemplate {TemplateName = Resources.Retiming + (counter > 0 ? counter.ToString() : "")});
        }

        private void RetimeTemplateEntfernen() {
            if (SelectedRetimeTemplate != null) {
                RetimeTemplates.Remove(SelectedRetimeTemplate);
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}