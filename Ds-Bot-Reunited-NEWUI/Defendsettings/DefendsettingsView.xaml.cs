﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Defendsettings {
    public partial class DefendsettingsView {
        public DefendsettingsView(Window owner, DefendsettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
