﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Weltensettings {
    public partial class WeltensettingsView {
        public WeltensettingsView(Window owner, WeltensettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
