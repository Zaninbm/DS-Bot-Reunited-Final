﻿using System;
using System.Linq;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Settings;

namespace Ds_Bot_Reunited_NEWUI.EinzelAccounting {
    public class EinzelaccountingSettingsAdjustModul {
        public static void AdjustSettingsForEinzelaccount(Accountsetting accountsettings) {
            if (accountsettings.AutoPilot)
                if (accountsettings.LastAdjusted < DateTime.Now.AddHours(-20))
                    if (accountsettings.DoerferSettings.Any() && accountsettings.DoerferSettings.Count == 1)
                        foreach (var dorf in accountsettings.DoerferSettings) {
                            SetBauschleifeForDorf(dorf, accountsettings);
                            accountsettings.LastAdjusted = DateTime.Now;
                        }
        }

        public static void SetBauschleifeForDorf(AccountDorfSettings dorfSettings, Accountsetting accountsettings) {
            var prioritaet = 0;
            if (dorfSettings.Bauschleife.Count < 10) {
                //Quest 1010
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Holzfaeller)});
                //Quest 1020
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Lehmgrube)});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Eisenmine)});
                //Quest 1030
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 2, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Holzfaeller)});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 2, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Lehmgrube)});
                //Quest 1040
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 2, Priority = prioritaet++});
                //Quest 1050
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 3, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 1, Priority = prioritaet++});
                //Quest 1060
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 3, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 3, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 2, Priority = prioritaet++});
                //Quest 1070
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 2, Priority = prioritaet++});
                //Quest 1090
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 2, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 3, Priority = prioritaet++});
                //Quest 1150
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 3, Priority = prioritaet++});
                //Quest 1810
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Statue, TargetLevel = 1, Priority = prioritaet++});
                //Quest 1160
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 3, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 2, Priority = prioritaet++});
                //Quest 1110
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 5, Priority = prioritaet++});
                //VERSETZT NACH UNTEN!
                //Quest 1240
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = 1, Priority = prioritaet++});
                //Quest 1130
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = 1, Priority = prioritaet++});
                //Quest 1091
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 4, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 4, Priority = prioritaet++});
                //Quest 1880
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Versteck, TargetLevel = 3, Priority = prioritaet++});
                //Quest 1250
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 5, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 5, Priority = prioritaet++});

                //Questteil
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = 1, Priority = prioritaet++});


                //Normale minen ausbauen
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 6, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 6, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 5, Priority = prioritaet++});

                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 7, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 7, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 6, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 8, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 8, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 7, Priority = prioritaet++});

                //Quest ohne nummer

                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 6, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 4, Priority = prioritaet++});


                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 9, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 9, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 8, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 10, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 10, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 9, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 11, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 11, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 12, Priority = prioritaet++});

                //Quest 2030
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 7, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 5, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 5, Priority = prioritaet++});

                //Quest 4030

                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 10, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = 5, Priority = prioritaet++});

                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Stall, TargetLevel = 1, Priority = prioritaet++});

                //Normal Building
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 12, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 12, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 14, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = 5, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 13, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 13, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 15, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 14, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 14, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 15, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 15, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 17, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 20, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 20, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 20, Priority = prioritaet});
            }
        }
    }
}
