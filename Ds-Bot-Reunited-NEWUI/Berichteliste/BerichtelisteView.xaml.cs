﻿using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.BerichtModul;

namespace Ds_Bot_Reunited_NEWUI.Berichteliste {
    public partial class BerichtelisteView {
        public BerichtelisteView(Window owner, BerichtelisteViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((BerichtelisteViewModel) DataContext)?.SelectedBerichte != null) {
                ((BerichtelisteViewModel) DataContext).SelectedBerichte.Clear();
                foreach (var item in DataGrid.SelectedItems)
                    ((BerichtelisteViewModel) DataContext).SelectedBerichte.Add((Bericht) item);
            }
        }
    }
}
