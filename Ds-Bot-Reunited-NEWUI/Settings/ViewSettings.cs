﻿namespace Ds_Bot_Reunited_NEWUI.Settings
{
    [global::System.Reflection.Obfuscation(Exclude = true, Feature = "renaming")]
    public class ViewSettings
    {
        public bool AccountsettingsViewHidden { get; set; }
        public ViewSettings() { }

        public ViewSettings Clone()
        {
            ViewSettings newViewSettings = new ViewSettings
            {
                AccountsettingsViewHidden = AccountsettingsViewHidden
            };
            return newViewSettings;
        }
    }
}