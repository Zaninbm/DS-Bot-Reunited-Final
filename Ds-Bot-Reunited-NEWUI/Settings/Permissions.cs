﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;

namespace Ds_Bot_Reunited_NEWUI.Settings {
    public class Permissions : INotifyPropertyChanged {
        private bool _sendingIsEnabled;
        private bool _defendingIsEnabled;
        private bool _buildingIsEnabled;
        private bool _recruitingIsEnabled;
        private bool _attackplanerIsEnabled;
        private bool _marketingIsEnabled;
        private bool _premiumDepotIsEnabled;
        private bool _multiaccountingIsEnabled;
        private bool _farmingIsEnabled;
        private bool _isBeta;

        public bool IsBeta {
            get => _isBeta;
            set {
                _isBeta = value;
                OnPropertyChanged();
            }
        }
        public bool SendingIsEnabled  {
            get => _sendingIsEnabled;
            set {
                _sendingIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool DefendingIsEnabled {
            get => _defendingIsEnabled;
            set {
                _defendingIsEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool BuildingIsEnabled {
            get => _buildingIsEnabled;
            set {
                _buildingIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool RecruitingIsEnabled {
            get => _recruitingIsEnabled;
            set {
                _recruitingIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool AttackplanerIsEnabled {
            get => _attackplanerIsEnabled;
            set {
                _attackplanerIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool MarketingIsEnabled {
            get => _marketingIsEnabled;
            set {
                _marketingIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool PremiumDepotIsEnabled {
            get => _premiumDepotIsEnabled;
            set {
                _premiumDepotIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool MultiaccountingIsEnabled {
            get => _multiaccountingIsEnabled;
            set {
                _multiaccountingIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool FarmingIsEnabled {
            get => _farmingIsEnabled;
            set {
                _farmingIsEnabled = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}