﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmliste;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.BerichtModul {
    public static class BerichteModul {
        public static async Task StartReadingBerichte(Browser browser, Accountsetting accountsettings) {
            if (accountsettings.LastReportReading.AddMinutes(20) < DateTime.Now) {
                bool fehlerfrei;
                var counter = 0;
                do {
                    fehlerfrei = false;
                    try {
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ReportReadingStarted);
                        await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);


                        var berichtordner = new List<string> {NAMECONSTANTS.GetNameForServer(Names.ReportNeueBerichte), NAMECONSTANTS.GetNameForServer(Names.ReportFarmassistent)};
                        var counterrr = 0;
                        foreach (var ordner in berichtordner) {
                            var link = App.Settings.OperateLink + accountsettings.UvZusatzFuerLink + "&screen=report&group_id=0";
                            browser.WebDriver.Navigate().GoToUrl(link);
                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                            counterrr++;
                            if (counterrr > 1)
                                for (var i = 0; i < berichtordner.Count; i++)
                                    try {
                                        browser.WebDriver.FindElement(By.XPath("//a[contains(text(),'" + berichtordner[i] + "')]")).Click();
                                        await Rndm.Sleep(400, 700);
                                        i = berichtordner.Count;
                                    } catch {
                                        // ignored
                                    }
                            var elements = browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Berichte.ANGRIFFE_BUTTON_SELECTOR_XPATH)).ToList();
                            elements.FirstOrDefault(x => x.Displayed).Click();
                            await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                            List<IWebElement> seiten = null;
                            try {
                                seiten = browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).ToList();
                            } catch {
                                // ignored
                            }
                            var start = 1;
                            try {
                                start = int.Parse(seiten.Last().Text.Replace("[", "").Replace("]", ""));
                            } catch {
                                // ignored
                            }
                            switch (App.Random.Next(0, 1)) {
                                case 0:
                                    for (var k = 1; k <= start; k++) {
                                        try {
                                            browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).FirstOrDefault(x => x.Text.Equals("[" + k + "]")).Click();
                                        } catch (Exception) {
                                            // ignored
                                        }
                                        await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                                        var table = browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Berichte.REPORTLIST_SELECTOR_XPATH));
                                        var tableRows = table.FindElements(By.TagName("tr")).ToList();
                                        for (var j = 0; j < tableRows.Count; j++) {
                                            var button = tableRows[j].FindElements(By.XPath(SpielOberflaeche.Berichte.BERICHT_SELECTOR_XPATH)).ToList();
                                            for (var i = 0; i < button.Count; i++) {
                                                var attribute = button[i].GetAttribute("href");
                                                var indexofView = attribute.IndexOf("view", StringComparison.CurrentCulture);
                                                var actualreportid = attribute.Substring(indexofView + 5);
                                                if (!App.Berichte.Any(x => x.Berichtid.Equals(actualreportid))) {
                                                    browser.ClickElement(button[i]);
                                                    await ReadBerichte(browser, accountsettings, 0);
                                                    i = button.Count;
                                                    j = tableRows.Count;
                                                    k = start + 1;
                                                }
                                                if (i == button.Count - 1) {
                                                    j = tableRows.Count;
                                                    k = start + 1;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 1:
                                    for (var k = start; k > 0; k--) {
                                        browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).FirstOrDefault(x => x.Text.Contains(k.ToString())).Click();
                                        await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                                        var table2 = browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Berichte.REPORTLIST_SELECTOR_XPATH));
                                        var tableRows2 = table2.FindElements(By.TagName("tr")).ToList();
                                        for (var j = 0; j < tableRows2.Count; j++) {
                                            var button = tableRows2[j].FindElements(By.XPath(SpielOberflaeche.Berichte.BERICHT_SELECTOR_XPATH)).ToList();
                                            for (var i = 0; i < button.Count; i++) {
                                                var attribute = button[i].GetAttribute("href");
                                                var indexofView = attribute.IndexOf("view", StringComparison.CurrentCulture);
                                                var actualreportid = attribute.Substring(indexofView + 5);
                                                if (!App.Berichte.Any(x => x.Berichtid.Equals(actualreportid))) {
                                                    browser.ClickElement(button[i]);
                                                    await ReadBerichte(browser, accountsettings, 1);
                                                    i = button.Count;
                                                    j = tableRows2.Count;
                                                    k = -1;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    for (var k = 1; k <= start; k++) {
                                        try {
                                            browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).FirstOrDefault(x => x.Text.Equals("[" + k + "]")).Click();
                                        } catch (Exception) {
                                            // ignored
                                        }
                                        await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                                        var table = browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Berichte.REPORTLIST_SELECTOR_XPATH));
                                        var tableRows = table.FindElements(By.TagName("tr")).ToList();
                                        for (var j = 0; j < tableRows.Count; j++) {
                                            var button = tableRows[j].FindElements(By.XPath(SpielOberflaeche.Berichte.BERICHT_SELECTOR_XPATH)).ToList();
                                            for (var i = 0; i < button.Count; i++) {
                                                var attribute = button[i].GetAttribute("href");
                                                var indexofView = attribute.IndexOf("view", StringComparison.CurrentCulture);
                                                var actualreportid = attribute.Substring(indexofView + 5);
                                                if (!App.Berichte.Any(x => x.Berichtid.Equals(actualreportid))) {
                                                    browser.ClickElement(button[i]);
                                                    await ReadBerichte(browser, accountsettings, 0);
                                                    i = button.Count;
                                                    j = tableRows.Count;
                                                    k = start + 1;
                                                }
                                                if (i == button.Count - 1) {
                                                    j = tableRows.Count;
                                                    k = start + 1;
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    } catch (Exception) {
                        fehlerfrei = true;

                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ErrorReportReading);
                    }
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ReportReadingEnded);

                    counter++;
                } while (fehlerfrei && counter < 2);
                accountsettings.LastReportReading = DateTime.Now;
            }
        }

        private static async Task ReadBerichte(Browser browser, Accountsetting accountsettings, int mode) {
            var counter = 0;
            var schoneingelesencounter = 0;
            browser.WebDriver.WaitForPageload();
            do {
                if (counter > 0) {
                    switch (mode) {
                        case 0:
                            browser.ClickByXpath(SpielOberflaeche.Berichte.BerichteUebersicht.BERICHT_VORHER_BUTTON_SELECTOR_XPATH);
                            break;
                        case 1:
                            browser.ClickByXpath(SpielOberflaeche.Berichte.BerichteUebersicht.BERICHT_NAECHSTER_BUTTON_SELECTOR_XPATH);
                            break;
                    }
                    browser.WebDriver.WaitForPageload();
                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                }
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                await App.WaitForAttacks();
                var bericht = ReadBericht(browser.WebDriver, accountsettings);
                if (bericht != null)
                    if (!App.Berichte.Any(x => x.Berichtid.Equals(bericht.Berichtid))) {
                        App.Berichte.Add(bericht);
                        if (bericht.Kampfzeit < DateTime.Now.AddHours(-12))
                            schoneingelesencounter++;
                    }
                    else
                        schoneingelesencounter++;
                counter++;
            } while ((mode == 0
                         ? browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Berichte.BerichteUebersicht.BERICHT_VORHER_BUTTON_SELECTOR_XPATH))
                         : browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Berichte.BerichteUebersicht.BERICHT_NAECHSTER_BUTTON_SELECTOR_XPATH))) && schoneingelesencounter < 3);
        }


        public static DateTime GetZeit(string[] temp2) {
            if (App.Settings.Server.Equals(SERVERCONSTANTS.Diestaemme)) {
                var dt = new DateTime(DateTime.Now.Year, int.Parse(temp2[0].Substring(3, 2)), int.Parse(temp2[0].Substring(0, 2)), int.Parse(temp2[1].Substring(0, 2)), int.Parse(temp2[1].Substring(3, 2)), int.Parse(temp2[1].Substring(6, 2)));
                if (App.Settings.Weltensettings.MillisecondsActive)
                    dt = dt.AddMilliseconds(int.Parse(temp2[1].Substring(9, 3)));
                return dt;
            }
            if (App.Settings.Server.Equals(SERVERCONSTANTS.Tribalwarsnet) || App.Settings.Server.Equals(SERVERCONSTANTS.Tribalwarscouk)) {
                var dt = new DateTime(DateTime.Now.Year, 1, 1, int.Parse(temp2[3].Substring(0, 2)), int.Parse(temp2[3].Substring(3, 2)), int.Parse(temp2[3].Substring(6, 2)));
                var month = temp2[0];
                switch (month) {
                    case "Jan":
                        dt = dt.AddMonths(0);
                        break;
                    case "Feb":
                        dt = dt.AddMonths(1);
                        break;
                    case "Mar":
                        dt = dt.AddMonths(2);
                        break;
                    case "Apr":
                        dt = dt.AddMonths(3);
                        break;
                    case "May":
                        dt = dt.AddMonths(4);
                        break;
                    case "Jun":
                        dt = dt.AddMonths(5);
                        break;
                    case "Jul":
                        dt = dt.AddMonths(6);
                        break;
                    case "Aug":
                        dt = dt.AddMonths(7);
                        break;
                    case "Sep":
                        dt = dt.AddMonths(8);
                        break;
                    case "Oct":
                        dt = dt.AddMonths(9);
                        break;
                    case "Nov":
                        dt = dt.AddMonths(10);
                        break;
                    case "Dec":
                        dt = dt.AddMonths(11);
                        break;
                }
                dt = dt.AddDays(int.Parse(temp2[1].TrimEnd(',')) - 1);
                if (App.Settings.Weltensettings.MillisecondsActive)
                    dt = dt.AddMilliseconds(int.Parse(temp2[3].Substring(9, 3)));
                return dt;
            }
            return new DateTime();
        }


        private static Bericht ReadBericht(IWebDriver webDriver, Accountsetting accountsettings) {
            try {
                var element = webDriver.FindElement(By.XPath(SpielOberflaeche.Berichte.BerichteUebersicht.BERICHTUEBERSICHT_SELECTOR_XPATH));

                bool spyressourcesVorhanden = webDriver.FindElements(By.XPath("//table[@id='attack_spy_resources']")).Any();
                var gebaeude = webDriver.FindElements(By.XPath(SpielOberflaeche.Berichte.ERSPAEHTEGEBAEUDE_SELECTOR_XPATH)).ToList();
                var counter = gebaeude.Select(x => x.FindElements(By.TagName("tr")).Count).Sum();
	            string splitteddd = "";
				if(webDriver.FindElements(By.XPath("//table[@id='attack_results']")).Any())
					splitteddd = webDriver.FindElements(By.XPath("//table[@id='attack_results']"))[0].Text;
	            string beute = "";
				if(webDriver.FindElements(By.XPath(SpielOberflaeche.Berichte.ANGRIFFERGEBNIS_SELECTOR_XPATH)).Any())
					beute = webDriver.FindElements(By.XPath(SpielOberflaeche.Berichte.ANGRIFFERGEBNIS_SELECTOR_XPATH))[0].Text;


				
                var text = element.FindElements(By.TagName("table"))[4].Text;
                var splitted = text.Split('\n');


                var bericht = new Bericht();
                var url = webDriver.Url;
                var indexofView = url.IndexOf("view", StringComparison.CurrentCulture);
                bericht.Berichtid = url.Substring(indexofView + 5);
                bericht.Betreff = splitted[0].Replace("\r", "").Replace(NAMECONSTANTS.GetNameForServer(Names.ReportBetreff) + " ", "");
                var temp2 = splitted[1].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportKampfzeit) + " ", "").Replace("\r", "").Split(' ');
                var kampfzeit = GetZeit(temp2);
#pragma warning disable 4014
                Task.Run(async () => {
#pragma warning restore 4014
                    try {
                        bericht.Kampfzeit = kampfzeit;
                        bericht.Ergebnis = splitted[2];
                        var index = 4;
                        bericht.Angreiferglueck = double.Parse(splitted[index].Replace("%", "").Replace("\r", "").Replace("-", ""));
                        if (splitted[index++].Contains("-"))
                            bericht.Angreiferglueck *= -1;
                        if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportMoral)))
                            bericht.Moral = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportMoral) + ": ", "").Replace("%", "").Replace("\r", ""));
                        if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportNachtbonus)))
                            index++;
                        bericht.AngreiferPlayerId = Worlddata.GetPlayerIdForBericht(splitted[index++].Replace("\r", ""));
                        bericht.AngreiferVillageId = Worlddata.GetVillageIdForBericht(splitted[index++].Replace("\r", ""));
                        bericht.AngreiferTruppenAnzahl = GetTruppen(splitted[index++].Replace("\r", ""));
                        bericht.AngreiferTruppenVerluste = GetTruppen(splitted[index++].Replace("\r", ""));
                        if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportFlagge)))
                            index++;
                        if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportEffekte)))
                            bericht.Effekt = splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportEffekte), "").Replace("\r", "");
                        if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportGlauben)))
                            index++;
                        if (App.Settings.Weltensettings.PaladinActive && splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportPaladin)))
                            index++;
                        var playerindex = splitted.ToList().IndexOf(splitted.ToList().FirstOrDefault(x => x.Contains(NAMECONSTANTS.GetNameForServer(Names.ReportVerteidiger)) && !x.Contains(NAMECONSTANTS.GetNameForServer(Names.ReportNachtbonus))));
                        index = playerindex;
                        bericht.VerteidigerPlayerId = Worlddata.GetPlayerIdForBericht(splitted[index++].Replace("\r", ""));
                        var koordindex = index;
                        bericht.VerteidigerVillageId = Worlddata.GetVillageIdForBericht(splitted[index++].Replace("\r", ""));

                        Gebaeude erspaehteGebaeude = null;
                        TruppenTemplate erspaehteTruppenAußerhalb = null;
                        TruppenTemplate erspaehteTruppen = null;
                        if (!webDriver.PageSource.Contains(NAMECONSTANTS.GetNameForServer(Names.ReportKaempfer))) {
                            bericht.VerteidigerTruppenAnzahl = GetTruppen(splitted[index++].Replace("\r", ""));
                            bericht.VerteidigerTruppenVerluste = GetTruppen(splitted[index++].Replace("\r", ""));
                            if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportFlagge)))
                                index++;
                            if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportEffekte)))
                                bericht.Effekt = splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportEffekte), "").Replace("\r", "");
                            if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportGlauben)))
                                index++;
                            if (App.Settings.Weltensettings.PaladinActive && splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportPaladin)))
                                index++;
                            index++;
                            erspaehteTruppen = bericht.VerteidigerTruppenAnzahl.Clone();
                            erspaehteTruppen = FarmModul.SubstractTemplateFromTemplate(erspaehteTruppen, bericht.VerteidigerTruppenVerluste);
                            index = splitted.ToList().IndexOf(splitted.ToList().FirstOrDefault(x => x.Contains(NAMECONSTANTS.GetNameForServer(Names.ReportErspaehteRohstoffe)))) - 1;
                            if (index < 0)
                                index = splitted.ToList().IndexOf(splitted.ToList().FirstOrDefault(x => x.Contains(NAMECONSTANTS.GetNameForServer(Names.ReportMoeglicheRohstoffe)))) - 1;
                            if (spyressourcesVorhanden) {
                                index++;
                                if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportErspaehte)) && !string.IsNullOrEmpty(splitted[index])) {
                                    bericht.NochDrin = GetBeute(splitted[index++].Replace(".", ""));
                                    if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportMoegliche)))
                                        index++;
                                }
                                if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportMoegliche)))
                                    index++;
                            }
                            if (counter > 0) {
                                erspaehteGebaeude = new Gebaeude();
                                index++;
                                for (var i = 0; i < counter; i++)
                                    if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportMain))) {
                                        erspaehteGebaeude.Hauptgebaeude = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportMain) + " ", ""));
                                        if (App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId)) != null && App
                                                .Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId))
                                                .Attacks.Any(x => x.LandTime > DateTime.Now && x.Truppen.KatapulteAnzahl > 0 && x.Truppen.TemplateName.Equals("main")))
                                            erspaehteGebaeude.Hauptgebaeude = 1;
                                    }
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportBarracks))) {
                                        erspaehteGebaeude.Kaserne = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportBarracks) + " ", ""));
                                        if (App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId)) != null && App
                                                .Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId))
                                                .Attacks.Any(x => x.LandTime > DateTime.Now && x.Truppen.KatapulteAnzahl > 0 && x.Truppen.TemplateName.Equals("barracks")))
                                            erspaehteGebaeude.Kaserne = 0;
                                    }
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportStable))) {
                                        erspaehteGebaeude.Stall = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportStable) + " ", ""));
                                        if (App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId)) != null && App
                                                .Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId))
                                                .Attacks.Any(x => x.LandTime > DateTime.Now && x.Truppen.KatapulteAnzahl > 0 && x.Truppen.TemplateName.Equals("stable")))
                                            erspaehteGebaeude.Stall = 0;
                                    }
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportWorkshop))) {
                                        erspaehteGebaeude.Werkstatt = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportWorkshop) + " ", ""));
                                        if (App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId)) != null && App
                                                .Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId))
                                                .Attacks.Any(x => x.LandTime > DateTime.Now && x.Truppen.KatapulteAnzahl > 0 && x.Truppen.TemplateName.Equals("workshop")))
                                            erspaehteGebaeude.Werkstatt = 1;
                                    }
                                    else if (splitted[index].Equals(NAMECONSTANTS.GetNameForServer(Names.ReportChurch)))
                                        erspaehteGebaeude.Kirche = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportChurch) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportFirstChurch)))
                                        erspaehteGebaeude.ErsteKirche = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportFirstChurch) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportSmith))) {
                                        erspaehteGebaeude.Schmiede = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportSmith) + " ", ""));
                                        if (App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId)) != null && App
                                                .Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId))
                                                .Attacks.Any(x => x.LandTime > DateTime.Now && x.Truppen.KatapulteAnzahl > 0 && x.Truppen.TemplateName.Equals("smith")))
                                            erspaehteGebaeude.Schmiede = 1;
                                    }
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportSnob)))
                                        erspaehteGebaeude.Adelshof = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportSnob) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportWachturm)))
                                        erspaehteGebaeude.Watchtower = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportWachturm) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportVersammlungsplatz)))
                                        erspaehteGebaeude.Versammlungsplatz = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportVersammlungsplatz) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportStatue)))
                                        erspaehteGebaeude.Statue = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportStatue) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportMarktplatz))) {
                                        erspaehteGebaeude.Marktplatz = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportMarktplatz) + " ", ""));
                                        if (App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId)) != null && App
                                                .Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId))
                                                .Attacks.Any(x => x.LandTime > DateTime.Now && x.Truppen.KatapulteAnzahl > 0 && x.Truppen.TemplateName.Equals("market")))
                                            erspaehteGebaeude.Marktplatz = 1;
                                    }
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportHolz)))
                                        erspaehteGebaeude.Holzfaeller = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportHolz) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportLehm)))
                                        erspaehteGebaeude.Lehmgrube = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportLehm) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportEisen)))
                                        erspaehteGebaeude.Eisenmine = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportEisen) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportFarm)))
                                        erspaehteGebaeude.Bauernhof = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportFarm) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportSpeicher)))
                                        erspaehteGebaeude.Speicher = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportSpeicher) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportVersteck)))
                                        erspaehteGebaeude.Versteck = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportVersteck) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportWall)))
                                        erspaehteGebaeude.Wall = int.Parse(splitted[index++].Replace(NAMECONSTANTS.GetNameForServer(Names.ReportWall) + " ", ""));
                                    else if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportGebaeudeStufe)))
                                        index++;

                                //List<IWebElement> gebaeude = webDriver.FindElements(By.XPath("//table[contains(@id,'attack_spy_buildings')]")).ToList();
                                //foreach (var gebauedeaufzaehlung in gebaeude) {
                                //    List<IWebElement> tablerows = gebauedeaufzaehlung.FindElements(By.TagName("tr")).ToList();
                                //    foreach (var tablerow in tablerows) {
                                //        if (tablerow.FindElements(By.TagName("img")).Count > 0) {
                                //            bool weitermachen = true;
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'main.png')]")).Count > 0 && erspaehteGebaeude.Hauptgebaeude == 0 && weitermachen) {
                                //                var element1 = tablerow.FindElement(By.XPath("//img[contains(@src,'main.png')]"));
                                //                if (element1 != null && element1.Location.Y + 3 >= tablerow.Location.Y && element1.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Hauptgebaeude = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'barracks.png')]")).Count > 0 && erspaehteGebaeude.Kaserne == 0 && weitermachen) {
                                //                var element2 = tablerow.FindElement(By.XPath("//img[contains(@src,'barracks.png')]"));
                                //                if (element2 != null && weitermachen && element2.Location.Y + 3 >= tablerow.Location.Y && element2.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Kaserne = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'stable.png')]")).Count > 0 && erspaehteGebaeude.Stall == 0 && weitermachen) {
                                //                var element3 = tablerow.FindElement(By.XPath("//img[contains(@src,'stable.png')]"));
                                //                if (element3 != null && weitermachen && element3.Location.Y + 3 >= tablerow.Location.Y && element3.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Stall = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'garage.png')]")).Count > 0 && erspaehteGebaeude.Werkstatt == 0 && weitermachen) {
                                //                var element4 = tablerow.FindElement(By.XPath("//img[contains(@src,'garage.png')]"));
                                //                if (element4 != null && weitermachen && element4.Location.Y + 3 >= tablerow.Location.Y && element4.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Werkstatt = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'smith.png')]")).Count > 0 && erspaehteGebaeude.Schmiede == 0 && weitermachen) {
                                //                var element5 = tablerow.FindElement(By.XPath("//img[contains(@src,'smith.png')]"));
                                //                if (element5 != null && weitermachen && element5.Location.Y + 3 >= tablerow.Location.Y && element5.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Schmiede = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'snob.png')]")).Count > 0 && erspaehteGebaeude.Adelshof == 0 && weitermachen) {
                                //                var element6 = tablerow.FindElement(By.XPath("//img[contains(@src,'snob.png')]"));
                                //                if (element6 != null && weitermachen && element6.Location.Y + 3 >= tablerow.Location.Y && element6.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Adelshof = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'statue.png')]")).Count > 0 && erspaehteGebaeude.Statue == 0 && weitermachen) {
                                //                var element7 = tablerow.FindElement(By.XPath("//img[contains(@src,'statue.png')]"));
                                //                if (element7 != null && weitermachen && element7.Location.Y + 3 >= tablerow.Location.Y && element7.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Statue = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'place.png')]")).Count > 0 && erspaehteGebaeude.Versammlungsplatz == 0 && weitermachen) {
                                //                var element8 = tablerow.FindElement(By.XPath("//img[contains(@src,'place.png')]"));
                                //                if (element8 != null && weitermachen && element8.Location.Y + 3 >= tablerow.Location.Y && element8.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Versammlungsplatz = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'market.png')]")).Count > 0 && erspaehteGebaeude.Marktplatz == 0 && weitermachen) {
                                //                var element9 = tablerow.FindElement(By.XPath("//img[contains(@src,'market.png')]"));
                                //                if (element9 != null && weitermachen && element9.Location.Y + 3 >= tablerow.Location.Y && element9.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Marktplatz = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'wood.png')]")).Count > 0 && erspaehteGebaeude.Holzfaeller == 0 && weitermachen) {
                                //                var element10 = tablerow.FindElement(By.XPath("//img[contains(@src,'wood.png')]"));
                                //                if (element10 != null && weitermachen && element10.Location.Y + 3 >= tablerow.Location.Y && element10.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Holzfaeller = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'stone.png')]")).Count > 0 && erspaehteGebaeude.Lehmgrube == 0 && weitermachen) {
                                //                var element11 = tablerow.FindElement(By.XPath("//img[contains(@src,'stone.png')]"));
                                //                if (element11 != null && weitermachen && element11.Location.Y + 3 >= tablerow.Location.Y && element11.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Lehmgrube = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'iron.png')]")).Count > 0 && erspaehteGebaeude.Eisenmine == 0 && weitermachen) {
                                //                var element12 = tablerow.FindElement(By.XPath("//img[contains(@src,'iron.png')]"));
                                //                if (element12 != null && weitermachen && element12.Location.Y + 3 >= tablerow.Location.Y && element12.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Eisenmine = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'farm.png')]")).Count > 0 && erspaehteGebaeude.Bauernhof == 0 && weitermachen) {
                                //                var element13 = tablerow.FindElement(By.XPath("//img[contains(@src,'farm.png')]"));
                                //                if (element13 != null && weitermachen && element13.Location.Y + 3 >= tablerow.Location.Y && element13.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Bauernhof = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'storage.png')]")).Count > 0 && erspaehteGebaeude.Speicher == 0 && weitermachen) {
                                //                var element14 = tablerow.FindElement(By.XPath("//img[contains(@src,'storage.png')]"));
                                //                if (element14 != null && weitermachen && element14.Location.Y + 3 >= tablerow.Location.Y && element14.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Speicher = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                    weitermachen = false;
                                //                }
                                //            }
                                //            if (tablerow.FindElements(By.XPath("//img[contains(@src,'hide.png')]")).Count > 0 && erspaehteGebaeude.Versteck == 0 && weitermachen) {
                                //                var element15 = tablerow.FindElement(By.XPath("//img[contains(@src,'hide.png')]"));
                                //                if (element15 != null && weitermachen && element15.Location.Y + 3 >= tablerow.Location.Y && element15.Location.Y - 3 <= tablerow.Location.Y) {
                                //                    erspaehteGebaeude.Versteck = int.Parse(tablerow.FindElements(By.TagName("td"))[1].Text.Replace(".", ""));
                                //                }
                                //            }
                                //        }
                                //    }
                                //}
                            }

                            if (webDriver.FindElements(By.XPath(SpielOberflaeche.Berichte.ERSPAEHTETRUPPENAUSERHALB_SELECTOR_XPATH)).Count > 0) {
                                if (splitted[index].Contains(NAMECONSTANTS.GetNameForServer(Names.ReportEinheitenAußerhalb)))
                                    index++;
                                erspaehteTruppenAußerhalb = GetTruppen(splitted[index++].Replace(".", ""));
                            }
                        }
                        if (!string.IsNullOrEmpty(splitteddd)) {
                            var splitted2 = splitteddd.Split('\n')[0];
                            //Schaden durch Rammböcke: Wall beschädigt von Level 5 auf Level 0
                            var indexOfSchraegstrich = splitted2.Replace("\r", "").IndexOf("/", StringComparison.CurrentCulture) + 1;
                            var erwartet = splitted2.Replace("\r", "").Substring(indexOfSchraegstrich);
                            bericht.Erwartet = int.Parse(erwartet.Replace(".", ""));
                            bericht.Beute = GetBeute(beute);
                            bericht.Erfolgsquotient = (int) (Convert.ToDouble(bericht.Beute.Gesamt) / Convert.ToDouble(bericht.Erwartet) * 100);
                        }

                        var frischHinzugefuegt = false;
                        if (!App.Farmliste.Any(x => x.VillageId.Equals(bericht.VerteidigerVillageId))) {
                            var koords = splitted[koordindex].Replace("" + NAMECONSTANTS.GetNameForServer(Names.ReportHerkunft) + ": ", "");
                            var xindex = koords.IndexOf("(", StringComparison.CurrentCulture);
                            var yindex = koords.IndexOf(")", StringComparison.CurrentCulture);
                            var coords = koords.Substring(xindex + 1, yindex - xindex - 1);
                            var xVerteidiger = int.Parse(coords.Split('|')[0]);
                            var yVerteidiger = int.Parse(coords.Split('|')[1]);
                            FarmlistenVerwaltung.AddToFarmliste(xVerteidiger, yVerteidiger, erspaehteGebaeude, erspaehteTruppenAußerhalb, erspaehteTruppen);
                            frischHinzugefuegt = true;
                        }
                        await Rndm.Sleep(100, 200);
                        var farmtarget = App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(bericht.VerteidigerVillageId));
                        if (farmtarget != null) {
                            if (erspaehteGebaeude != null)
                                farmtarget.Gebaeude = erspaehteGebaeude;
                            if (erspaehteTruppenAußerhalb != null)
                                farmtarget.AusserhalbTruppen = erspaehteTruppenAußerhalb;
                            if (erspaehteTruppen != null)
                                farmtarget.VorhandeneTruppen = erspaehteTruppen;
                            if (erspaehteTruppenAußerhalb != null && FarmModul.HasAnyNormalTroopInIt(erspaehteTruppenAußerhalb) || erspaehteTruppen != null && FarmModul.HasAnyNormalTroopInIt(erspaehteTruppen) && frischHinzugefuegt)
                                if (!farmtarget.FixStatus)
                                    farmtarget.Active = false;
                            if (farmtarget.Dorf.PlayerId.Equals(accountsettings.Playerid))
                                farmtarget.Active = false;

                            if (bericht.AngreiferTruppenVerluste.SpeertraegerAnzahl > 0 || bericht.AngreiferTruppenVerluste.SchwertkaempferAnzahl > 0 || bericht.AngreiferTruppenVerluste.AxtkaempferAnzahl > 0 ||
                                bericht.AngreiferTruppenVerluste.BogenschuetzenAnzahl > 0 || bericht.AngreiferTruppenVerluste.SpaeherAnzahl > 0 || bericht.AngreiferTruppenVerluste.LeichteKavallerieAnzahl > 0 ||
                                bericht.AngreiferTruppenVerluste.BeritteneBogenschuetzenAnzahl > 0 || bericht.AngreiferTruppenVerluste.SchwereKavallerieAnzahl > 0 || bericht.AngreiferTruppenVerluste.RammboeckeAnzahl > 0 ||
                                bericht.AngreiferTruppenVerluste.KatapulteAnzahl > 0 || bericht.AngreiferTruppenVerluste.PaladinAnzahl > 0) {
                                if (bericht.AngreiferTruppenVerluste.SpeertraegerAnzahl == bericht.AngreiferTruppenAnzahl.SpeertraegerAnzahl && bericht.AngreiferTruppenVerluste.SchwertkaempferAnzahl == bericht.AngreiferTruppenAnzahl.SchwertkaempferAnzahl &&
                                    bericht.AngreiferTruppenVerluste.AxtkaempferAnzahl == bericht.AngreiferTruppenAnzahl.AxtkaempferAnzahl && bericht.AngreiferTruppenVerluste.BogenschuetzenAnzahl == bericht.AngreiferTruppenAnzahl.BogenschuetzenAnzahl &&
                                    bericht.AngreiferTruppenVerluste.LeichteKavallerieAnzahl == bericht.AngreiferTruppenAnzahl.LeichteKavallerieAnzahl &&
                                    bericht.AngreiferTruppenVerluste.BeritteneBogenschuetzenAnzahl == bericht.AngreiferTruppenAnzahl.BeritteneBogenschuetzenAnzahl &&
                                    bericht.AngreiferTruppenVerluste.SchwereKavallerieAnzahl == bericht.AngreiferTruppenAnzahl.SchwereKavallerieAnzahl && bericht.AngreiferTruppenVerluste.RammboeckeAnzahl == bericht.AngreiferTruppenAnzahl.RammboeckeAnzahl &&
                                    bericht.AngreiferTruppenVerluste.KatapulteAnzahl == bericht.AngreiferTruppenAnzahl.KatapulteAnzahl && bericht.AngreiferTruppenVerluste.PaladinAnzahl == bericht.AngreiferTruppenAnzahl.PaladinAnzahl)
                                    farmtarget.Active = false;
                                if (farmtarget.Gebaeude.Wall == 0)
                                    farmtarget.Gebaeude.Wall = 1;
                            }
                            if (bericht.Beute != null && bericht.Beute.Gesamt != 0) {
                                if (accountsettings.UseRegressionForErfolgsquotient) {
                                    var berichte = App.Berichte.OrderBy(x => x.Kampfzeit).Where(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId) && x.Kampfzeit > DateTime.Now.AddHours(-accountsettings.MaxHoursToCountBerichteForErfolgsquotient)).ToList();
                                    var gesamteq = Convert.ToDouble(bericht.Beute.Gesamt) / Convert.ToDouble(bericht.Erwartet);
                                    if (berichte.Any() || gesamteq > 0 && !text.Contains(NAMECONSTANTS.GetNameForServer(Names.Farmlimit))) {
                                        var berichtee = berichte.Select(x => (double) x.Erfolgsquotient).ToList();
                                        berichtee.Add(gesamteq * 100);
                                        double erfolg = GetErfolgsquotientVorraussage(berichtee, farmtarget.Erfolgsquotient);
                                        farmtarget.Erfolgsquotient = erfolg > 100 ? 99 : erfolg;
                                    }
                                }
                                else {
                                    var berichte = App.Berichte.Where(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId) && x.Kampfzeit > DateTime.Now.AddHours(-accountsettings.MaxHoursToCountBerichteForErfolgsquotient)).ToList();
                                    var gesamteq = Convert.ToDouble(bericht.Beute.Gesamt) / Convert.ToDouble(bericht.Erwartet);
                                    if (berichte.Any() || gesamteq > 0 && !text.Contains(NAMECONSTANTS.GetNameForServer(Names.Farmlimit)))
                                        farmtarget.Erfolgsquotient = (int) ((gesamteq * 100 + Convert.ToDouble(berichte.Sum(x => x.Erfolgsquotient))) / (berichte.Count + 1));
                                    if (farmtarget.Erfolgsquotient <= 5)
                                        farmtarget.Erfolgsquotient = 5;
                                }


                                farmtarget.ErbeutetGesamt.Holz += bericht.Beute.Holz;
                                farmtarget.ErbeutetGesamt.Lehm += bericht.Beute.Lehm;
                                farmtarget.ErbeutetGesamt.Eisen += bericht.Beute.Eisen;
                            }
                        }
                    } catch (Exception e) {
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ErrorReportReading + " " + e);
                    }
                });
                return bericht;
            } catch (Exception e) {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ErrorReportReading + " " + e);
            }
            return null;
        }

        private static double GetErfolgsquotientVorraussage(List<double> yVals, double erfolgsquotient) {
            List<double> x = new List<double>();
            int counter = 1;
            foreach (var temp in yVals) {
                x.Add(counter++);
            }
            LinearRegression(x, yVals, 0, yVals.Count, out double rsquared, out double yintercept, out double slope);
            double xx = yintercept + slope * 1;
            if (xx.Equals(Double.NaN))
                return erfolgsquotient;
            return xx;
        }

        private static void LinearRegression(List<double> xVals, List<double> yVals,
            int inclusiveStart, int exclusiveEnd,
            out double rsquared, out double yintercept,
            out double slope) {
            double sumOfX = 0;
            double sumOfY = 0;
            double sumOfXSq = 0;
            double sumOfYSq = 0;
            double ssX = 0;
            double ssY = 0;
            double sumCodeviates = 0;
            double sCo = 0;
            double count = exclusiveEnd - inclusiveStart;

            for (int ctr = inclusiveStart; ctr < exclusiveEnd; ctr++) {
                double x = xVals[ctr];
                double y = yVals[ctr];
                sumCodeviates += x * y;
                sumOfX += x;
                sumOfY += y;
                sumOfXSq += x * x;
                sumOfYSq += y * y;
            }
            ssX = sumOfXSq - ((sumOfX * sumOfX) / count);
            ssY = sumOfYSq - ((sumOfY * sumOfY) / count);
            double RNumerator = (count * sumCodeviates) - (sumOfX * sumOfY);
            double RDenom = (count * sumOfXSq - (sumOfX * sumOfX))
                            * (count * sumOfYSq - (sumOfY * sumOfY));
            sCo = sumCodeviates - ((sumOfX * sumOfY) / count);

            double meanX = sumOfX / count;
            double meanY = sumOfY / count;
            double dblR = RNumerator / Math.Sqrt(RDenom);
            rsquared = dblR * dblR;
            yintercept = meanY - ((sCo / ssX) * meanX);
            slope = sCo / ssX;
        }

        private static Beute GetBeute(string beutestring) {
            var beute = new Beute();
            try {
                var bs = beutestring.Replace(NAMECONSTANTS.GetNameForServer(Names.ReportBeute) + ": ", "").Replace(NAMECONSTANTS.GetNameForServer(Names.ReportErspaehteRohstoffe) + ": ", "").Split(' ');
                if (bs.Any() && bs.Length < 1)
                    return beute;
                beute.Holz = int.Parse(bs[0].Replace(".", ""));
                if (bs.Any() && bs.Length < 2)
                    return beute;
                beute.Lehm = int.Parse(bs[1].Replace(".", ""));
                if (bs.Any() && bs.Length < 3)
                    return beute;
                beute.Eisen = int.Parse(bs[2].Replace(".", ""));
            } catch { }
            return beute;
        }

        private static TruppenTemplate GetTruppen(string truppenstring) {
            var truppen = truppenstring.Replace(NAMECONSTANTS.GetNameForServer(Names.ReportAnzahl) + ": ", "")
                .Replace(NAMECONSTANTS.GetNameForServer(Names.ReportVerluste) + ": ", "")
                .Replace(NAMECONSTANTS.GetNameForServer(Names.ReportEinheitenAußerhalb) + ":\r\n", "")
                .Replace(".", "")
                .Split(' ');
            var angreiferTruppen = new TruppenTemplate();
            var index = 0;
            angreiferTruppen.SpeertraegerAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.SchwertkaempferAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.AxtkaempferAnzahl = int.Parse(truppen[index++]);
            if (App.Settings.Weltensettings.BogenschuetzenActive)
                angreiferTruppen.BogenschuetzenAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.SpaeherAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.LeichteKavallerieAnzahl = int.Parse(truppen[index++]);
            if (App.Settings.Weltensettings.BogenschuetzenActive)
                angreiferTruppen.BeritteneBogenschuetzenAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.SchwereKavallerieAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.RammboeckeAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.KatapulteAnzahl = int.Parse(truppen[index++]);
            if (App.Settings.Weltensettings.PaladinActive)
                angreiferTruppen.PaladinAnzahl = int.Parse(truppen[index++]);
            angreiferTruppen.AdelsgeschlechterAnzahl = int.Parse(truppen[index++]);
            return angreiferTruppen;
        }
    }
}