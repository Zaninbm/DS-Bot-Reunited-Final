﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AccountsettingsView;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Attackplaner;
using Ds_Bot_Reunited_NEWUI.Bauensettings;
using Ds_Bot_Reunited_NEWUI.Berichteliste;
using Ds_Bot_Reunited_NEWUI.Botmessages;
using Ds_Bot_Reunited_NEWUI.Botsettings;
using Ds_Bot_Reunited_NEWUI.Defendsettings;
using Ds_Bot_Reunited_NEWUI.DorfUebersichtView;
using Ds_Bot_Reunited_NEWUI.EinzelAccounting;
using Ds_Bot_Reunited_NEWUI.Farmliste;
using Ds_Bot_Reunited_NEWUI.Farmsettings;
using Ds_Bot_Reunited_NEWUI.Gruppensettings;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Log;
using Ds_Bot_Reunited_NEWUI.Marktsettings;
using Ds_Bot_Reunited_NEWUI.Multiaccounting;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Rekrutierensettings;
using Ds_Bot_Reunited_NEWUI.Weltensettings;
using Framework.UI.Controls;

#pragma warning disable 4014

// ReSharper disable FunctionNeverReturns
// ReSharper disable PossibleNullReferenceException

namespace Ds_Bot_Reunited_NEWUI {
	public class TileViewModel : INotifyPropertyChanged {
		private ICommand _browserWindowCommand;
		private bool _mainWindowsHidden = true;
		private ICommand _openAccountsettingsViewCommand;
		private ICommand _openAttackDetectionViewCommand;
		private ICommand _openAttackplanerViewCommand;
		private ICommand _openBotsettingsViewCommand;
		private ICommand _openBuildmodulViewCommand;
		private ICommand _openDefendmodulViewCommand;
		private ICommand _openFarmlistViewCommand;
		private ICommand _openFarmmodulViewCommand;
		private RelayCommand _openGroupsettingsViewCommand;
		private ICommand _openLogViewCommand;
		private ICommand _openMarketmodulViewCommand;
		private ICommand _openQueueViewCommand;
		private ICommand _openVillagemodulViewCommand;
		private ICommand _openReportlistViewCommand;
		private ICommand _openSendmodulViewCommand;
		private ICommand _openVillageinformationViewCommand;
		private ICommand _openWeltensettingsViewCommand;
		private ICommand _startPauseCommand;
		private string _status;

		public MainWindow Window { get; set; }

		public string Status {
			get => _status;
			set {
				_status = value;
				OnPropertyChanged("");
			}
		}

		public bool Active {
			get => App.Active;
			set {
				App.Active = value;
				OnPropertyChanged();
			}
		}

		public Brush BotmessageTileBrush {
			get {
				if (App.Botmessages.Any(x => x.Unread)) return Brushes.Red;
				return Brushes.LightBlue;
			}
		}

		public bool Started { get; set; }

		public ICommand OpenWeltensettingsViewCommand =>
			_openWeltensettingsViewCommand = _openWeltensettingsViewCommand ?? new RelayCommand(OpenWeltensettingsView);

		public ICommand StartPauseCommand => _startPauseCommand = _startPauseCommand ?? new RelayCommand(StartPause);

		public ICommand BrowserWindowCommand =>
			_browserWindowCommand = _browserWindowCommand ?? new RelayCommand(BrowserWindow);

		public ICommand OpenAccountsettingsViewCommand =>
			_openAccountsettingsViewCommand =
				_openAccountsettingsViewCommand ?? new RelayCommand(OpenAccountsettingsView);

		public ICommand OpenGroupsettingsViewCommand =>
			_openGroupsettingsViewCommand = _openGroupsettingsViewCommand ?? new RelayCommand(OpenGroupsettingsView);

		public ICommand OpenBuildmodulViewCommand =>
			_openBuildmodulViewCommand = _openBuildmodulViewCommand ?? new RelayCommand(OpenBuildmodulView);

		public ICommand OpenFarmmodulViewCommand =>
			_openFarmmodulViewCommand = _openFarmmodulViewCommand ?? new RelayCommand(OpenFarmmodulView);

		public ICommand OpenSendmodulViewCommand =>
			_openSendmodulViewCommand = _openSendmodulViewCommand ?? new RelayCommand(OpenSendmodulView);

		public ICommand OpenAttackplanerViewCommand =>
			_openAttackplanerViewCommand = _openAttackplanerViewCommand ?? new RelayCommand(OpenAttackplanerView);

		public ICommand OpenDefendmodulViewCommand =>
			_openDefendmodulViewCommand = _openDefendmodulViewCommand ?? new RelayCommand(OpenDefendmodulView);

		public ICommand OpenMarketmodulViewCommand =>
			_openMarketmodulViewCommand = _openMarketmodulViewCommand ?? new RelayCommand(OpenMarketmodulView);

		public ICommand OpenVillagemodulViewCommand =>
			_openVillagemodulViewCommand = _openVillagemodulViewCommand ?? new RelayCommand(OpenVillagemodulView);

		public ICommand OpenFarmlistViewCommand =>
			_openFarmlistViewCommand = _openFarmlistViewCommand ?? new RelayCommand(OpenFarmlistView);

		public ICommand OpenReportlistViewCommand =>
			_openReportlistViewCommand = _openReportlistViewCommand ?? new RelayCommand(OpenReportlistView);

		public ICommand OpenVillageinformationViewCommand =>
			_openVillageinformationViewCommand =
				_openVillageinformationViewCommand ?? new RelayCommand(OpenVillageinformationView);

		public ICommand OpenQueueViewCommand =>
			_openQueueViewCommand = _openQueueViewCommand ?? new RelayCommand(OpenQueueView);

		public ICommand OpenLogViewCommand =>
			_openLogViewCommand = _openLogViewCommand ?? new RelayCommand(OpenLogView);

		public ICommand OpenBotsettingsViewCommand =>
			_openBotsettingsViewCommand = _openBotsettingsViewCommand ?? new RelayCommand(OpenBotsettingsView);

		public ICommand OpenBotmessagesViewCommand =>
			_openAttackDetectionViewCommand = _openAttackDetectionViewCommand ?? new RelayCommand(OpenBotmessagesView);

		public bool MainWindowsHidden {
			get => _mainWindowsHidden;
			set {
				_mainWindowsHidden = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void OpenBotmessagesView() {
			var window = new BotmessagesView(Window);
			window.ShowDialog();
		}

		private void OpenWeltensettingsView() {
			var window = new WeltensettingsView(Window, new WeltensettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void StartPause() {
			if (string.IsNullOrEmpty(App.Settings.TwoCaptchaApiKey)) {
				await MessageDialog.ShowAsync("2captcha apikey missing", "2captcha apikey is missing, please go to 2captcha.com and register a new acc and copy the apikey into the botsettingsfield", MessageBoxButton.OK, MessageDialogType.Accent
				                              , Window);
				return;
            }

			if (MainWindowsHidden) StartCycle();
			OnPropertyChanged("");
		}

		private void BrowserWindow() {
			if (Active) {
				Active = !Active;
				if (!MainWindowsHidden) {
					if (BrowserManager.BrowserManager.HideMainWindows()) MainWindowsHidden = true;
				} else {
					if (BrowserManager.BrowserManager.ShowMainWindows()) MainWindowsHidden = false;
				}
			} else {
				if (!MainWindowsHidden) {
					if (BrowserManager.BrowserManager.HideMainWindows()) MainWindowsHidden = true;
				} else {
					if (BrowserManager.BrowserManager.ShowMainWindows()) MainWindowsHidden = false;
				}

				if (Started) Active = !Active;
			}
		}

		public async void StartCycle() {
			if (!Started) {
				if (App.Settings.Accountsettingslist.Any(x => !string.IsNullOrEmpty(x.Accountname) &&
				                                              string.IsNullOrEmpty(x.Passwort)) ||
				    App.Settings.Accountsettingslist.Count > 1 &&
				    App.Settings.Accountsettingslist.Any(x => string.IsNullOrEmpty(x.ProxyEins) &&
				                                              !x.MultiaccountingDaten.Accountmode
				                                                .Equals(Accountmode.Mainaccount) &&
				                                              !string.IsNullOrEmpty(x.Accountname))) {
					await MessageDialog.ShowAsync("", Resources.NoAccount, MessageBoxButton.OK, MessageDialogType.Accent
					                              , Window);
					return;
				}

				App.LogString(Resources.Started);
				Active = true;
				Started = true;
				Window.Text = App.Settings.Accountsettingslist
				                 .FirstOrDefault(x => x.MultiaccountingDaten.Accountmode == Accountmode.Mainaccount)
				                 ?.Accountname;

				var einzelaccount = new EinzelaccountingModul(this);
				einzelaccount.EinzelAccountCycles();
				MultiaccountingModul.MultiaccountingCycle();
				if (Started && Active)
					App.NotificationIcon.ContextMenu.MenuItems[0].Text = Resources.Pause;
				else
					App.NotificationIcon.ContextMenu.MenuItems[0].Text = Resources.Start;
				Status = Resources.Started;
			} else {
				Active = !Active;
				if (Active) {
					App.LogString(Resources.BotResumed);
					Status = Resources.BotResumed;
				} else {
					App.LogString(Resources.BotPaused);
					Status = Resources.BotPaused;
				}

				if (Started && Active)
					App.NotificationIcon.ContextMenu.MenuItems[0].Text = Resources.Pause;
				else
					App.NotificationIcon.ContextMenu.MenuItems[0].Text = Resources.Start;
			}
		}

		private async void OpenAccountsettingsView() {
			if (string.IsNullOrEmpty(App.Settings.Welt)) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAWorldToPlayOn, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new AccountsettingsView.AccountsettingsView(Window, new AccountsettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenGroupsettingsView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new GruppensettingsView(Window, new GruppensettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenBuildmodulView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new BauensettingsView(Window, new BauensettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenFarmmodulView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new FarmsettingsView(Window, new FarmsettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenSendmodulView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var account =
				App.Settings.Accountsettingslist.FirstOrDefault(x =>
					                                                x
						                                                .MultiaccountingDaten.Accountmode
						                                                .Equals(Accountmode.Mainaccount));
			if (account == null) {
				await MessageDialog.ShowAsync(Resources.NoMainAccountAvailable, Resources.Achtung, MessageBoxButton.OK
				                              , MessageDialogType.Accent, App.Window);
				return;
			}

			var settings = new AngreifenSettingsViewModel(account, Window);
			var view = new AngreifensettingsView(Window) {DataContext = settings};
			view.ShowDialog();
		}

		private async void OpenAttackplanerView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new AttackplanerView(Window, new AttackplanerViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenDefendmodulView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new DefendsettingsView(Window, new DefendsettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenMarketmodulView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new MarktsettingsView(Window, new MarktsettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenVillagemodulView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new RekrutierensettingsView(Window, new RekrutierensettingsViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenFarmlistView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new FarmlisteView(Window, new FarmlisteViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenReportlistView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new BerichtelisteView(Window, new BerichtelisteViewModel(Window));
			window.ShowDialog();
		}

		private async void OpenVillageinformationView() {
			if (App.Settings.Accountsettingslist.All(x => string.IsNullOrEmpty(x.Accountname))) {
				await MessageDialog.ShowAsync(Resources.PleaseChoseAnAccountToPlayWith, Resources.Achtung
				                              , MessageBoxButton.OK, MessageDialogType.Accent, App.Window);
				return;
			}

			var window = new DorfUebersichtView.DorfUebersichtView(Window, new DorfUebersichtViewModel());
			window.ShowDialog();
		}

		private void OpenQueueView() {
			var window = new QueueView.QueueView(Window);
			window.ShowDialog();
		}

		private void OpenLogView() {
			var window = new LogView(Window);
			window.ShowDialog();
		}

		private void OpenBotsettingsView() {
			var window = new BotsettingsView(Window, new BotsettingsViewModel(Window));
			window.ShowDialog();
		}

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
