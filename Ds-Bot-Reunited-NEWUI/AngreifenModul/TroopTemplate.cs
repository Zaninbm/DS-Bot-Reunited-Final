﻿namespace Ds_Bot_Reunited_NEWUI.AngreifenModul {
    public class TroopTemplate {
        public TroopTemplate(int intValue, string strValue) {
            Amount = intValue;
            Unit = strValue;
        }

        public int Amount { get; set; }
        public string Unit { get; set; }
        public TroopTemplate() { }
    }
}