﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.AngreifenModul {
    public class Versammlungsplatzmodul {
        public static async Task SendBewegungOverVersammlungsplatz(Browser browser, int xcoordinate, int ycoordinate, TruppenTemplate template, AccountDorfSettings senderDorf, string bewegungsart, Accountsetting accountsettings) {
            string link = App.Settings.OperateLink + "village=" + senderDorf.Dorf.VillageId + accountsettings.UvZusatzFuerLink + "&screen=place";
            browser.WebDriver.Navigate().GoToUrl(link);
            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
            await FillKoordsAndTroops(browser.WebDriver, xcoordinate, ycoordinate, template);
            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
            switch (bewegungsart) {
                case "Attack":
                    await PressAngreifen(browser, template.TemplateName);
                    break;
                case "Support":
                    await PressUnterstuetzen(browser);
                    break;
            }
            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
            try {
                await PressOk(browser);
            } catch {
                // ignored
            }
        }

        public static async Task FillKoordsAndTroops(IWebDriver webDriver, int xcoordinate, int ycoordinate, TruppenTemplate template) {
            int counter = 0;
            bool fehlerfrei;
            string url = webDriver.Url;
            do {
                fehlerfrei = false;
                if (counter > 0) {
                    webDriver.Navigate().GoToUrl(url);
                    await Rndm.Sleep(100, 300);
                }
                List<IWebElement> truppenTextboxen = webDriver.FindElements(By.XPath(SpielOberflaeche.Versammlungsplatz.TRUPPENTEXTBOXEN_SELECTOR_XPATH)).ToList();
                for (int i = 0; i < truppenTextboxen.Count; i++) {
                    try {
                        int index = 0;
                        if (i == index++) {
                            if (template.SpeertraegerAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.SpeertraegerAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.SchwertkaempferAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.SchwertkaempferAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.AxtkaempferAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.AxtkaempferAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (App.Settings.Weltensettings.BogenschuetzenActive && i == index++) {
                            if (template.BogenschuetzenAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.BogenschuetzenAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.SpaeherAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.SpaeherAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.LeichteKavallerieAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.LeichteKavallerieAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (App.Settings.Weltensettings.BogenschuetzenActive && i == index++) {
                            if (template.BeritteneBogenschuetzenAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.BeritteneBogenschuetzenAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.SchwereKavallerieAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.SchwereKavallerieAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.RammboeckeAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.RammboeckeAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index++) {
                            if (template.KatapulteAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.KatapulteAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (App.Settings.Weltensettings.PaladinActive && i == index++) {
                            if (template.PaladinAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.PaladinAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        if (i == index) {
                            if (template.AdelsgeschlechterAnzahl != 0) {
                                truppenTextboxen[i].Clear();
                                truppenTextboxen[i].SendKeys(template.AdelsgeschlechterAnzahl.ToString());
                                await Rndm.Sleep(10, 20);
                            }
                        }
                        fehlerfrei = true;
                    } catch (Exception) {
                        fehlerfrei = false;
                        truppenTextboxen = webDriver.FindElements(By.XPath(SpielOberflaeche.Versammlungsplatz.TRUPPENTEXTBOXEN_SELECTOR_XPATH)).ToList();
                        i = 0;
                    }
                }
                try {
                    if (xcoordinate != 0 && ycoordinate != 0) {
                        webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.KOORDINATENFELD_SELECTOR_XPATH)).SendKeys(xcoordinate.ToString());
                        await Rndm.Sleep(100, 300);
                        webDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.KOORDINATENFELD_SELECTOR_XPATH)).SendKeys("|" + ycoordinate);
                    }
                    fehlerfrei = true;
                } catch (Exception) {
                    fehlerfrei = false;
                }
                counter++;
            } while (!fehlerfrei && counter < 3);
            await Rndm.Sleep(100, 200);
        }

        public static async Task PressAngreifen(Browser browser, string kattaziel) {
            browser.ClickByXpath(SpielOberflaeche.Versammlungsplatz.Befehle.Befehlbestaetigung.ANGREIFEN_BUTTON_SELECTOR_XPATH);
            await Rndm.Sleep(50, 100);
            try {
                if (!string.IsNullOrEmpty(kattaziel)) {
                    IWebElement element = browser.WebDriver.ByName("building");
                    element.SelectOptionByStartsWithText(kattaziel.ToLower());
                    await Rndm.Sleep(50, 100);
                }
            } catch { }
        }

        public static async Task PressUnterstuetzen(Browser browser) {
            browser.ClickByXpath(SpielOberflaeche.Versammlungsplatz.Befehle.Befehlbestaetigung.UNTERSTUETZEN_BUTTON_SELECTOR_XPATH);
            await Rndm.Sleep(50, 200);
        }

        public static async Task PressOk(Browser browser) {
            browser.ClickByXpath(SpielOberflaeche.Versammlungsplatz.Befehle.Befehlbestaetigung.BESTAETIGUNGSBUTTON_SELECTOR_XPATH);
            await Rndm.Sleep(100, 500);
        }

        public static async Task PressOk(IWebElement element) {
            element.Click();
            await Rndm.Sleep(100, 500);
        }
    }
}
