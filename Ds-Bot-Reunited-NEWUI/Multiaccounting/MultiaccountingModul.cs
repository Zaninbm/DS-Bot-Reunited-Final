﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.TimeModul;

namespace Ds_Bot_Reunited_NEWUI.Multiaccounting {
	public class MultiaccountingModul {
		public static void MultiaccountingCycle() {
			if (App.Permissions.MultiaccountingIsEnabled) {
				if (App.Settings.Accountsettingslist.Any(x =>
					                                         x
						                                         .MultiaccountingDaten.Accountmode
						                                         .Equals(Accountmode.Botaccount) && x.LoginActive)) {
					var again = false;
					do {
						Task task = new Task(async () => {
							                     var newWorld = false;
							                     if (App.Settings.Accountsettingslist.All(x => !x.DoerferSettings.Any())
							                     ) {
								                     foreach (var settings in
									                     App.Settings.Accountsettingslist.Where(x => !x.DoerferSettings
									                                                                   .Any())) {
									                     // ReSharper disable once UnusedVariable
									                     var browser =
										                     BrowserManager.BrowserManager.GetFreeBrowser(settings
										                                                                  , settings
											                                                                  .ProxyEins
										                                                                  , false);

									                     BrowserManager.BrowserManager.RemoveBrowser(settings);
								                     }

								                     await Task.Delay(60 * 20 * 1000);
								                     newWorld = true;
							                     }

							                     App.LogString("Multiaccounting gestartet");
							                     for (;;) {
								                     if (App.Settings.MultiaccountingActive) {
									                     while (!App.Active) await Task.Delay(1000);
									                     var now = DateTime.Now;
									                     if (!App.Settings.Accountsettingslist.Any(x =>
										                                                               x
											                                                               .MultiaccountingDaten
											                                                               .Accountmode
											                                                               .Equals(Accountmode
												                                                                       .Mainaccount)) &&
									                         App.LastWorlddataCheck.AddMinutes(60) < now &&
									                         !App.SaveIsGoingOn || newWorld) {
										                     await Worlddata.LoadWorldData();
									                     }

									                     var accountsettings = App
									                                           .Settings.Accountsettingslist
									                                           .Where(x => {
										                                                  var accountDorfSettings =
											                                                  x.DoerferSettings
											                                                   .LastOrDefault();
										                                                  return
											                                                  accountDorfSettings !=
											                                                  null &&
											                                                  x.CanLoginAndDoSomething &&
											                                                  !x.MultiaccountingDaten
											                                                    .GetsUsed &&
											                                                  x.LoginActive &&
											                                                  x.MultiaccountingDaten
											                                                   .Accountmode
											                                                   .Equals(Accountmode
												                                                           .Botaccount) &&
											                                                  !x.LastLogins.Any(k =>
												                                                                    k >
												                                                                    DateTime
													                                                                    .Now
													                                                                    .AddMinutes(-10));
									                                                  }).OrderBy(x => x.LastLogins.Any()
										                                                                  ? x
										                                                                    .LastLogins
										                                                                    .OrderByDescending(y =>
											                                                                                       y)
										                                                                    .First()
										                                                                    .AddMinutes(App
										                                                                                .Random
										                                                                                .Next(-10
										                                                                                      , 10))
										                                                                  : DateTime
											                                                                  .MinValue)
									                                           .FirstOrDefault();
									                     while (!BrowserManager.BrowserManager.CanAddNewBrowser())
										                     await Task.Delay(1000);
									                     if (accountsettings != null) {
										                     accountsettings.MultiaccountingDaten.GetsUsed = true;
										                     var senderdorf = accountsettings
										                                      .DoerferSettings.LastOrDefault();
										                     var browser =
											                     await BrowserManager
											                           .BrowserManager
											                           .GetFreeBrowser(accountsettings
											                                           , accountsettings.ProxyEins
											                                           , App.Settings.Botmode ==
											                                             Botmode.Botmode.Performance);

										                     browser.GetsUsed = true;

#pragma warning disable CS4014 // Da dieser Aufruf nicht abgewartet wird, wird die Ausführung der aktuellen Methode fortgesetzt, bevor der Aufruf abgeschlossen ist
										                     Task.Run(async () => {
											                              if (
												                              accountsettings
													                              .MultiaccountingDaten
													                              .RausstellenZeitpunkt >
												                              DateTime.MinValue &&
												                              accountsettings
													                              .MultiaccountingDaten
													                              .RausstellenZeitpunkt.AddMinutes(20) >
												                              now) {
												                              //TODO Bessere DörferAuswahl nötig!
												                              var firstOrDefault =
													                              App.Settings.Accountsettingslist
													                                 .Where(x => !x.LoginActive &&
													                                             x
														                                             .MultiaccountingDaten
														                                             .Accountmode
														                                             .Equals(Accountmode
															                                                     .Botaccount))
													                                 .OrderByDescending(x =>
														                                                    Laufzeit
															                                                    .GetTravelTime(x.DoerferSettings.LastOrDefault()?.Dorf.Dorf
															                                                                   , accountsettings
															                                                                     .DoerferSettings
															                                                                     .LastOrDefault()
															                                                                     ?.Dorf
															                                                                     .Dorf
															                                                                   , new
															                                                                     TruppenTemplate {
																                                                                                     SpeertraegerAnzahl
																	                                                                                     = 1
															                                                                                     }))
													                                 .FirstOrDefault();
												                              var accountDorfSettings =
													                              firstOrDefault
														                              ?.DoerferSettings.LastOrDefault();
												                              if (accountDorfSettings != null) {
													                              var dorf = accountDorfSettings
													                                         .Dorf.Dorf;
													                              var lastOrDefault =
														                              accountsettings
															                              .DoerferSettings
															                              .LastOrDefault();
													                              if (lastOrDefault != null)
														                              browser.WebDriver.Navigate()
														                                     .GoToUrl(App.Settings
														                                                 .OperateLink +
														                                              "village=" +
														                                              lastOrDefault
															                                              .Dorf
															                                              .VillageId +
														                                              "&screen=place");
													                              await Versammlungsplatzmodul
														                              .SendBewegungOverVersammlungsplatz(browser
														                                                                 , dorf
															                                                                 .XCoordinate
														                                                                 , dorf
															                                                                 .YCoordinate
														                                                                 , DorfinformationenModul
														                                                                   .DorfinformationenModul
														                                                                   .GetAvailableTroops(browser
														                                                                                       , senderdorf)
														                                                                 , senderdorf
														                                                                 , Resources
															                                                                 .Support
														                                                                 , accountsettings);
												                              }

												                              accountsettings
														                              .MultiaccountingDaten
														                              .RausstellenZeitpunkt =
													                              DateTime.Now.AddMinutes(-30);
											                              }

											                              if (accountsettings.LoginActive)
												                              try {
													                              MultiaccountingSettingsAdjustModul
														                              .AdjustSettingsForMultiaccount(accountsettings);
													                              if (!accountsettings.SellMode) {
														                              await InventoryModul
														                                    .InventoryModul
														                                    .UseInventorySkills(browser
														                                                        , accountsettings);
														                              if (accountsettings
															                              .SetBoosterActive)
															                              await PremiumFeaturesModul
															                                    .PremiumFeaturesModul
															                                    .SetBooster(browser
															                                                , accountsettings);
														                              if (accountsettings
															                              .SetInventoryItemsActive)
															                              await InventoryModul
															                                    .InventoryModul
															                                    .UseInventoryRohstoffe(browser
															                                                           , accountsettings);
														                              if (accountsettings.HandelnActive)
															                              await Marktmodul
															                                    .Marktmodul
															                                    .StartTrading(browser
															                                                  , accountsettings);
														                              if (!accountsettings
														                                   .MultiaccountingDaten
														                                   .HasAlly)
															                              await StammModul
															                                    .StammModul
															                                    .StammBeitretenFallsNichtVorhanden(browser
															                                                                       , accountsettings);
														                              if (accountsettings
															                              .SetFlaggeActive)
															                              await FlaggenModul
															                                    .FlaggenModul
															                                    .SetRohstoffFlagge(browser
															                                                       , accountsettings
															                                                       , null);
														                              if (accountsettings.QuestsActive)
															                              await QuestModul
															                                    .QuestModul
															                                    .MakeQuests(browser
															                                                , accountsettings
															                                                , accountsettings
															                                                  .DoerferSettings
															                                                  .LastOrDefault());
														                              if (accountsettings
															                              .RekrutierenActive)
															                              await RekrutierenModul
															                                    .RekrutierenModul
															                                    .StartRekrut(browser
															                                                 , accountsettings);
														                              if (accountsettings.BauenActive) {
															                              var bauen =
																                              new BauenModul.
																	                              BauenModul();
															                              await bauen.Bauen(browser
															                                                , accountsettings);
														                              }

														                              if (accountsettings.QuestsActive)
															                              await QuestModul
															                                    .QuestModul
															                                    .MakeQuests(browser
															                                                , accountsettings
															                                                , accountsettings
															                                                  .DoerferSettings
															                                                  .LastOrDefault());
													                              } else {
														                              await InventoryModul
														                                    .InventoryModul
														                                    .UseInventorySkills(browser
														                                                        , accountsettings);

														                              if (accountsettings
															                              .SetBoosterActive)
															                              await PremiumFeaturesModul
															                                    .PremiumFeaturesModul
															                                    .SetBooster(browser
															                                                , accountsettings);
														                              if (accountsettings
															                              .SetInventoryItemsActive)
															                              await InventoryModul
															                                    .InventoryModul
															                                    .UseInventoryRohstoffe(browser
															                                                           , accountsettings);
														                              if (accountsettings.QuestsActive)
															                              await QuestModul
															                                    .QuestModul
															                                    .MakeQuests(browser
															                                                , accountsettings
															                                                , accountsettings
															                                                  .DoerferSettings
															                                                  .LastOrDefault());
														                              if (!accountsettings
														                                   .MultiaccountingDaten
														                                   .HasAlly)
															                              await StammModul
															                                    .StammModul
															                                    .StammBeitretenFallsNichtVorhanden(browser
															                                                                       , accountsettings);
														                              if (accountsettings
															                              .SetFlaggeActive)
															                              await FlaggenModul
															                                    .FlaggenModul
															                                    .SetRohstoffFlagge(browser
															                                                       , accountsettings
															                                                       , null);
														                              if (accountsettings.BauenActive) {
															                              var bauen =
																                              new BauenModul.
																	                              BauenModul();
															                              await bauen.Bauen(browser
															                                                , accountsettings);
														                              }

														                              if (accountsettings.HandelnActive)
															                              await Marktmodul
															                                    .Marktmodul
															                                    .StartTrading(browser
															                                                  , accountsettings);
														                              if (accountsettings
															                              .RekrutierenActive)
															                              await RekrutierenModul
															                                    .RekrutierenModul
															                                    .StartRekrut(browser
															                                                 , accountsettings);
														                              if (accountsettings.QuestsActive)
															                              await QuestModul
															                                    .QuestModul
															                                    .MakeQuests(browser
															                                                , accountsettings
															                                                , accountsettings
															                                                  .DoerferSettings
															                                                  .LastOrDefault());
													                              }

													                              if (accountsettings
														                              .NachrichtenLesenActive)
														                              await NachrichtenModul
														                                    .NachrichtenModul
														                                    .MakeNachrichten(browser
														                                                     , accountsettings);
#pragma warning disable 168
												                              } catch (Exception e) {
#pragma warning restore 168
													                              App.LogString((!string
														                                             .IsNullOrEmpty(accountsettings
															                                                            .Uvname)
														                                             ? accountsettings
															                                             .Uvname
														                                             : accountsettings
															                                             .Accountname) +
													                                            ": " + " Failure " + e);
												                              }

											                              BrowserManager
												                              .BrowserManager
												                              .RemoveBrowser(accountsettings);
											                              accountsettings
												                              .MultiaccountingDaten.GetsUsed = false;
										                              });
#pragma warning restore CS4014 // Da dieser Aufruf nicht abgewartet wird, wird die Ausführung der aktuellen Methode fortgesetzt, bevor der Aufruf abgeschlossen ist
									                     }

									                     await Task.Delay(2000);
								                     }
							                     }
						                     }, TaskCreationOptions.LongRunning);

						try {
							task.Start();
						} catch (Exception e) {
							App.LogString("Multiaccounting Error: " + e);
							again = true;
						}
					} while (again);
				}
			}
		}
	}
}