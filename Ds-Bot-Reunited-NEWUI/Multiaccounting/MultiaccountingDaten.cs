﻿using System;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Nachrichten;

namespace Ds_Bot_Reunited_NEWUI.Multiaccounting {
    [Serializable]
    public class MultiaccountingDaten {
        public DateTime RausstellenZeitpunkt { get; set; }
        public Accountmode Accountmode { get; set; }
        public AsyncObservableCollection<GemeinsamesZielFuerBewegung> DeffListe { get; set; }
        public bool RestartedOverItem { get; set; }
        public bool RestartedOverSettings { get; set; }
        public bool Deleted { get; set; }
        public double EntfernungZuHauptAccount { get; set; }
        public bool HasAlly { get; set; }
        public DateTime LastAllyCheck { get; set; }
        public AsyncObservableCollection<Nachricht> Nachrichten { get; set; }
        public string Gruppe { get; set; }
        public bool GetsUsed { get; set; }
        public int UnterstutzungenUnterwegs { get; set; }
        public int PremiumPunkte { get; set; }
        public bool WoodBoostSet { get; set; }
        public DateTime WoodBoostEndtime { get; set; }
        public bool StoneBoostSet { get; set; }
        public DateTime StoneBoostEndtime { get; set; }
        public bool IronBoostSet { get; set; }
        public DateTime IronBoostEndtime { get; set; }
        public bool FlageSet { get; set; }
        public DateTime InventoryLastUsed { get; set; }
        public DateTime PaladinLastSkillUsed { get; set; }

        public bool Quest1822Done { get; set; }

        public bool Quest1821Done { get; set; }
        public bool Quest1820Done { get; set; }
        public bool Quest1920Done { get; set; }
        public bool Quest1850Done { get; set; }
        public bool Quest1220Done { get; set; }
        public bool Quest1900Done { get; set; }
        public int Quest1910Done { get; set; }
        public int Quest2010Done { get; set; }
        public int Quest1120Done { get; set; }
        public bool ForTruppenAward { get; set; }
        public object NachrichtenLock { get; set; } = new object();
        public object DeffListeLock { get; set; } = new object();

        public MultiaccountingDaten() {
            DeffListe = new AsyncObservableCollection<GemeinsamesZielFuerBewegung>();
            Nachrichten = new AsyncObservableCollection<Nachricht>();
            BindingOperations.EnableCollectionSynchronization(Nachrichten,
                NachrichtenLock);
            BindingOperations.EnableCollectionSynchronization(DeffListe,
                DeffListeLock);
            Accountmode = Accountmode.Mainaccount;
        }

        public MultiaccountingDaten Clone() {
            MultiaccountingDaten newMultiaccountingDaten = new MultiaccountingDaten {
                RausstellenZeitpunkt = RausstellenZeitpunkt,
                DeffListe = new AsyncObservableCollection<GemeinsamesZielFuerBewegung>(),
                RestartedOverItem = RestartedOverItem,
                RestartedOverSettings = RestartedOverSettings,
                EntfernungZuHauptAccount = EntfernungZuHauptAccount,
                HasAlly = HasAlly,
                Nachrichten = new AsyncObservableCollection<Nachricht>(),
                Accountmode = Accountmode,
                Gruppe = Gruppe,
                UnterstutzungenUnterwegs = UnterstutzungenUnterwegs,
                Quest1820Done = Quest1822Done,
                Quest1822Done = Quest1822Done,
                Quest1821Done = Quest1821Done,
                Quest1920Done = Quest1920Done,
                Quest1850Done = Quest1850Done,
                Quest1220Done = Quest1220Done,
                Quest1900Done = Quest1900Done,
                Quest1910Done = Quest1910Done,
                Quest2010Done = Quest2010Done,
                Quest1120Done = Quest1120Done,
                ForTruppenAward = ForTruppenAward,
                Deleted = Deleted,
                FlageSet = FlageSet,
                IronBoostSet = IronBoostSet,
                PremiumPunkte = PremiumPunkte,
                StoneBoostSet = StoneBoostSet,
                WoodBoostSet = WoodBoostSet,
                InventoryLastUsed = InventoryLastUsed,
                LastAllyCheck = LastAllyCheck,
                PaladinLastSkillUsed = PaladinLastSkillUsed,
                WoodBoostEndtime = WoodBoostEndtime,
                StoneBoostEndtime = StoneBoostEndtime,
                IronBoostEndtime = IronBoostEndtime
            };
            foreach (var deffziel in DeffListe) {
                newMultiaccountingDaten.DeffListe.Add(deffziel.Clone());
            }

            foreach (var nachricht in Nachrichten) {
                newMultiaccountingDaten.Nachrichten.Add(nachricht.Clone());
            }
            return newMultiaccountingDaten;
        }
    }
}