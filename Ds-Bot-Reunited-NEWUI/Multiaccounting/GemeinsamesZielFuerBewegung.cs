﻿using System;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Multiaccounting {
    [Serializable]
    public class GemeinsamesZielFuerBewegung {
        public Dorf EmpfaengerDorf { get; set; }
        public DateTime SpaetesterAnkunftszeitpunkt { get; set; }
        public DateTime GenauerAnkunftszeitpunkt { get; set; }
        public TruppenTemplate EmpfangendeTruppen { get; set; }
        public string Bewegungsart { get; set; }

        public GemeinsamesZielFuerBewegung() { }

        public GemeinsamesZielFuerBewegung Clone() {
            GemeinsamesZielFuerBewegung newGemeinsamesZielFuerBewegung = new GemeinsamesZielFuerBewegung {
                Bewegungsart = Bewegungsart,
                SpaetesterAnkunftszeitpunkt = SpaetesterAnkunftszeitpunkt,
                GenauerAnkunftszeitpunkt = GenauerAnkunftszeitpunkt,
                EmpfaengerDorf = EmpfaengerDorf,
                EmpfangendeTruppen = EmpfangendeTruppen.Clone()
            };
            return newGemeinsamesZielFuerBewegung;
        }
    }
}