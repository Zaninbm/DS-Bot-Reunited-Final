﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.LoginModul {
    public static class Loginmodul {
        public static async Task<int> LoginStart(Browser browser, Accountsetting accountsettings, bool fromsessionlost = false) {
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.Login + " " + Resources.Started);
            if (accountsettings.CanLoginAndDoSomething) {
                if (fromsessionlost) {
                    if (!browser.Handles.Any(x => x.Haupthandle)) {
                        browser = await BrowserManager.BrowserManager.GetFreeBrowser(accountsettings, "");
                    }
                }
                bool failure;
                int counttter = 0;
                do {
                    counttter++;
                    failure = false;
                    try {
                        browser.WebDriver.Navigate().GoToUrl("https://www." + App.Settings.Server.GetDescription());
                        if (browser.WebDriver.FindElements(By.XPath("//div[@class='worlds-container']")).Count == 0) {
                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.ACCOUNTNAME_TEXTBOX_SELECTOR_XPATH)).SendKeys(accountsettings.Accountname);
                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.PASSWORT_TEXTBOX_SELECTOR_XPATH)).SendKeys(accountsettings.Passwort);
                            await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                            try {
                                browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.ANMELDEN_BUTTON_SELECTOR_XPATH)).Click();
                            } catch (Exception) {
                                browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.PASSWORT_TEXTBOX_SELECTOR_XPATH)).SendKeys("\n");
                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": Second Method used");
                            }
                        }
                        await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                        if (browser.WebDriver.PageSource.Contains(Resources.PasswortUngueltig)) {
                            browser.WebDriver.Dispose();
                            return 1;
                        }
                        if (browser.WebDriver.PageSource.Contains(Resources.AccountNichtVorhanden)) {
                            browser.WebDriver.Dispose();
                            return 1;
                        }

                        var element = browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Startseite.WELT_BUTTON_SELECTOR_XPATH.Replace("*", App.Settings.Welt.Replace("p", "").Replace("de", "").Replace("en", "")))).Count > 0 ? browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.WELT_BUTTON_SELECTOR_XPATH.Replace("*", App.Settings.Welt.Replace("p", "").Replace("de", "").Replace("en", "")))) : browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.WELT_INAKTIV_BUTTON_SELECTOR_XPATH.Replace("*", App.Settings.Welt.Replace("p", "").Replace("de", "").Replace("en", ""))));
                        int counter = 0;
                        while (element == null && counter < 10) {
                            counter++;
                            await Rndm.Sleep(1000, 1500);
                            element = browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Startseite.WELT_BUTTON_SELECTOR_XPATH.Replace("*", App.Settings.Welt.Replace("p", "").Replace("de", "").Replace("en", "")))).Count > 0 ? browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.WELT_BUTTON_SELECTOR_XPATH.Replace("*", App.Settings.Welt.Replace("p", "").Replace("de", "").Replace("en", "")))) : null;
                        }
                        if (element == null) {
                            browser.WebDriver.FindElement(By.XPath("//a[@class='world-select-show-all small']")).Click();
                            element = browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.WELT_INAKTIV_BUTTON_SELECTOR_XPATH.Replace("*", App.Settings.Welt)));
                        }
                        element.Click();
                        browser.WebDriver.WaitForPageload();
                        if (browser.WebDriver.PageSource.Contains(Resources.Update)) {
                            browser.WebDriver.Dispose();
                            return 2;
                        }
                        if (browser.WebDriver.FindElements(By.XPath("//div[@class='bordered-box']")).Count > 0) {
                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Startseite.TEILNEHMEN_BUTTON_SELECTOR_XPATH)).Click();
                            await Rndm.Sleep(App.Settings.MinActionDelay, App.Settings.MaxActionDelay);
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.FindElements(By.XPath("//div[@id='popup_box_daily_bonus']")).Count > 0) {
                            browser.WebDriver.FindElement(By.XPath("//div[@id='popup_box_daily_bonus']/descendant::a[@class='btn btn-default']")).Click();
                        }
                        if (browser.WebDriver.FindElements(By.XPath("//div[@class='popup_box slim show']")).Count > 0) {
                            browser.WebDriver.FindElement(By.XPath("//a[@class='popup_box_close tooltip-delayed']")).Click();
                            await Rndm.Sleep(200, 500);
                            browser.WebDriver.FindElement(By.XPath("//button[@class='btn evt-confirm-btn btn-confirm-yes']")).Click();
                        }
                        await Rndm.Sleep(2000, 5000);
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (!string.IsNullOrEmpty(accountsettings.Uvname)) {
                            await LoginUvStart(browser, accountsettings.Uvname);
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                    } catch (Exception) {
                        failure = true;
                    }
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.Login + " " + Resources.Ended);
                } while (counttter < 3 && failure);
                accountsettings.LastLogins.Add(DateTime.Now);
            }
            return 0;
        }

        private static async Task LoginUvStart(Browser browser, string accountname) {
            browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "screen=settings&mode=vacation");
            await Task.Delay(500);
            int playerid = 0;
            var spielerViewModel = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName == accountname);
            if (spielerViewModel != null) {
                playerid = spielerViewModel.PlayerId;
            }
            if (playerid != 0) {
                browser.WebDriver.FindElements(By.XPath("//a[contains(@href, '" + playerid + "')]"))[1].Click();
                await Task.Delay(500);
            }
            browser.WebDriver.SwitchTo().Window(browser.WebDriver.WindowHandles.LastOrDefault());
            List<IntPtr> list = WindowHider.GetWindowHandles(accountname);
            WindowHider.HideAllNewChromeWindows();
            browser.Handles.Add(new Handles {BeingUsed = false, Handle = browser.WebDriver.CurrentWindowHandle, Haupthandle = true});
            if (list.Any()) {
                browser.Handles.Last().Pointer = list.FirstOrDefault();
            }
            WindowHider.HideMainWindow();
        }

        public static void Logout(IWebDriver webDriver, Accountsetting accountsettings) {
            bool fehler;
            do {
                fehler = false;
                try {
                    webDriver.FindElement(By.XPath(SpielOberflaeche.Allgemein.AUSLOGGEN_BUTTON_SELECTOR_XPATH)).Click();
                    accountsettings.LastLogins.Add(DateTime.Now);
                } catch {
                    fehler = true;
                }
            } while (fehler);
        }
    }
}
