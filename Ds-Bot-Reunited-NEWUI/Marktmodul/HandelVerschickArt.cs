﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Marktmodul {
    namespace Ds_Bot_Reunited_NEWUI.Constants {
        [Serializable]
        public enum HandelVerschickArt {
            Marktbalancer,
            Markt,
            Premiumdepot,
            Handelsroute,
            None
        }
    }
}