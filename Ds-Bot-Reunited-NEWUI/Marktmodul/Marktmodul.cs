﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Marktmodul.Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.TimeModul;
using OpenQA.Selenium;

// ReSharper disable PossibleNullReferenceException

namespace Ds_Bot_Reunited_NEWUI.Marktmodul {
    public class Marktmodul {
        private const string selector = "//tr/descendant::form[contains(@class,'market_accept_offer')]/parent::*/parent::*";

        public static async Task StartTrading(Browser browser, Accountsetting accountsettings) {
            var handelsrouteDoerferSenden = new List<AccountDorfSettings>();
            var handelsrouteDoerferEmpfangen = new List<AccountDorfSettings>();
            var marktbalancerDoerferSenden = new List<AccountDorfSettings>();
            var marktbalancerDoerferEmpfangen = new List<AccountDorfSettings>();
            var premiumdepotDoerferSenden = new List<AccountDorfSettings>();
            var premiumdepotDoerferEmpfangen = new List<AccountDorfSettings>();
            var marktDoerferSenden = new List<AccountDorfSettings>();
            var marktDoerferEmpfangen = new List<AccountDorfSettings>();

            var usedVillages = new List<AccountDorfSettings>();


            foreach (var dorf in accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) && x.Dorf.PlayerId.Equals(accountsettings.Playerid)))
                if (dorf.HandelnActive) {
                    var addit = false;
                    if (dorf.HandelEmpfangenArt.Equals(HandelEmpfangArt.Marktbalancer)) {
                        marktbalancerDoerferEmpfangen.Add(dorf);
                        addit = true;
                    }
                    if (dorf.HandelEmpfangenArt.Equals(HandelEmpfangArt.Markt)) {
                        marktDoerferEmpfangen.Add(dorf);
                        addit = true;
                    }
                    if (dorf.HandelEmpfangenArt.Equals(HandelEmpfangArt.Handelsroute)) {
                        handelsrouteDoerferEmpfangen.Add(dorf);
                        addit = true;
                    }
                    if (dorf.HandelEmpfangenArt.Equals(HandelEmpfangArt.Premiumdepot)) {
                        premiumdepotDoerferEmpfangen.Add(dorf);
                        addit = true;
                    }


                    if (dorf.HandelVerschickenArt.Equals(HandelVerschickArt.Marktbalancer)) {
                        marktbalancerDoerferSenden.Add(dorf);
                        addit = true;
                    }
                    if (dorf.HandelVerschickenArt.Equals(HandelVerschickArt.Markt)) {
                        marktDoerferSenden.Add(dorf);
                        addit = true;
                    }
                    if (dorf.HandelVerschickenArt.Equals(HandelVerschickArt.Premiumdepot)) {
                        premiumdepotDoerferSenden.Add(dorf);
                        addit = true;
                    }
                    if (dorf.HandelVerschickenArt.Equals(HandelVerschickArt.Handelsroute)) {
                        handelsrouteDoerferSenden.Add(dorf);
                        addit = true;
                    }
                    if (addit)
                        usedVillages.Add(dorf);
                }

            //Update of Villageinformations
            await DorfinformationenModul.DorfinformationenModul.UpdateVillagesOverProductionView(browser, accountsettings, usedVillages);

            await DoMarktEmpfangen(browser, marktDoerferEmpfangen, accountsettings);

            await DoHandelsrouten(browser, handelsrouteDoerferSenden, accountsettings);
            await DoMarketBalancer(browser, marktbalancerDoerferSenden, marktbalancerDoerferEmpfangen, accountsettings);
            await DoBuyPremiumDepotOffers(browser, premiumdepotDoerferEmpfangen, accountsettings);
            await DoSellPremiumDepotOffers(browser, premiumdepotDoerferSenden, accountsettings);
        }

        private static void DoMarktSenden(Browser browser, List<AccountDorfSettings> villages, Accountsetting accountsettings) {
            foreach (var dorf in villages) {
                var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=own_offer";
                browser.WebDriver.Navigate().GoToUrl(link);
                //TODO
            }
        }


        private static async Task DoMarktEmpfangen(Browser browser, List<AccountDorfSettings> villages, Accountsetting accountsettings) {
            foreach (var dorf in villages) {
                await App.WaitForAttacks();
                var weitermachen = true;
                var minhaendler = (int) (dorf.HandelMinRessourcen / 1000);
                var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=other_offer";
                while (weitermachen && dorf.Dorfinformationen.AvailableTraders > dorf.HandelHaendlerZurueckHalten && dorf.Dorfinformationen.AvailableTraders > minhaendler && dorf.Dorfinformationen.SpeicherTotalHolz + dorf.HandelMinRessourcen < dorf.Dorfinformationen.StorageMax && dorf.Dorfinformationen.SpeicherTotalLehm + dorf.HandelMinRessourcen < dorf.Dorfinformationen.StorageMax && dorf.Dorfinformationen.SpeicherTotalEisen + dorf.HandelMinRessourcen < dorf.Dorfinformationen.StorageMax) {
                    if (!browser.WebDriver.Url.Equals(link)) {
                        browser.WebDriver.Navigate().GoToUrl(link);
                        browser.WebDriver.WaitForPageload();
                    }
                    var elements = browser.WebDriver.FindElements(By.XPath("//tr/descendant::form[contains(@class,'market_accept_offer')]/parent::*/parent::*")).ToList();
                    for (var i = 0; i < elements.Count; i++) {
                        var rohstoffeins = elements[i].FindElement(By.XPath("td[1]/span/span[1]")).GetAttribute("class");
                        var rohstoffzwei = elements[i].FindElement(By.XPath("td[2]/span/span[1]")).GetAttribute("class");
                        var text = elements[i].Text;

                        var splitted = text.Split(' ');

                        var indexKlammerAuf = text.IndexOf("(", StringComparison.Ordinal);

                        var amounttext = text.Substring(indexKlammerAuf + 1, text.Length - indexKlammerAuf - 2);
                        var amount = int.Parse(amounttext);


                        var rohstoffeinsAmount = int.Parse(splitted[0].Replace(".", ""));
                        var rohstoffzweiAmount = int.Parse(splitted[1].Replace(".", ""));

                        if (rohstoffeinsAmount > dorf.HandelMinRessourcen) {
                            var annehmenAnzahl = 0;

                            for (var k = 0; k < amount; k++)
                                if (dorf.Dorfinformationen.AvailableTraders > dorf.HandelHaendlerZurueckHalten && dorf.Dorfinformationen.AvailableTraders > minhaendler && dorf.Dorfinformationen.SpeicherTotalHolz + rohstoffeinsAmount < dorf.Dorfinformationen.StorageMax && dorf.Dorfinformationen.SpeicherTotalLehm + rohstoffeinsAmount < dorf.Dorfinformationen.StorageMax && dorf.Dorfinformationen.SpeicherTotalEisen + rohstoffeinsAmount < dorf.Dorfinformationen.StorageMax && (rohstoffzwei.Contains("wood") && dorf.Dorfinformationen.Speicher.Holz > rohstoffzweiAmount || rohstoffzwei.Contains("stone") && dorf.Dorfinformationen.Speicher.Lehm > rohstoffzweiAmount || rohstoffzwei.Contains("iron") && dorf.Dorfinformationen.Speicher.Eisen > rohstoffzweiAmount)) {
                                    if (rohstoffeins.Contains("wood") && dorf.HandelEmpfangenHolzUnter < dorf.Dorfinformationen.SpeicherTotalHolz)
                                        annehmenAnzahl++;
                                    if (rohstoffeins.Contains("stone") && dorf.HandelEmpfangenLehmUnter < dorf.Dorfinformationen.SpeicherTotalLehm)
                                        annehmenAnzahl++;
                                    if (rohstoffeins.Contains("iron") && dorf.HandelEmpfangenEisenUnter < dorf.Dorfinformationen.SpeicherTotalEisen)
                                        annehmenAnzahl++;
                                } else {
                                    i = elements.Count;
                                }
                            if (annehmenAnzahl > 0)
                                try {
                                    elements[i].FindElement(By.XPath("/td[7]/form/span/input")).SendKeys(annehmenAnzahl.ToString());
                                    elements[i].FindElement(By.XPath("/td[7]/form/input[2]")).Click();
                                    elements = browser.WebDriver.FindElements(By.XPath(selector)).ToList();
                                    var holz = 0;
                                    var lehm = 0;
                                    var eisen = 0;
                                    if (rohstoffeins.Contains("wood"))
                                        holz += rohstoffeinsAmount * annehmenAnzahl;
                                    else if (rohstoffeins.Contains("stone"))
                                        lehm += rohstoffeinsAmount * annehmenAnzahl;
                                    else if (rohstoffeins.Contains("iron"))
                                        eisen += rohstoffeinsAmount * annehmenAnzahl;
                                    var ts = TimeSpan.Parse(elements[i].FindElement(By.XPath("/td[4]")).Text);
                                    var transfer = new Markettransfer(holz, lehm, eisen, DateTime.Now.Add(ts));
                                    dorf.Dorfinformationen.AnkommendeMarkttransfers.Add(transfer);
                                }
                                // ReSharper disable once EmptyGeneralCatchClause
                                catch { }
                        }
                    }
                }
            }
        }


        private static async Task DoMarketBalancer(Browser browser, List<AccountDorfSettings> villages, List<AccountDorfSettings> empfangvillages, Accountsetting accountsettings) {
            var transfers = new List<Markettransfer>();
            foreach (var sender in villages) {
                await App.WaitForAttacks();
                if (sender.Dorfinformationen.AvailableTraders > sender.HandelHaendlerZurueckHalten && (sender.HandelVerschickenHolzUeber < sender.Dorfinformationen.SpeicherTotalHolz || sender.HandelVerschickenLehmUeber < sender.Dorfinformationen.SpeicherTotalLehm || sender.HandelVerschickenEisenUeber < sender.Dorfinformationen.SpeicherTotalEisen))
                    foreach (var empfaenger in empfangvillages.OrderBy(x => x.Dorf.XCoordinate).ThenBy(x => x.Dorf.YCoordinate))
                        if (sender.HandelVerschickenHolzUeber < sender.Dorfinformationen.SpeicherTotalHolz && empfaenger.HandelEmpfangenHolzUnter > empfaenger.Dorfinformationen.SpeicherTotalHolz || sender.HandelVerschickenLehmUeber < sender.Dorfinformationen.SpeicherTotalLehm && empfaenger.HandelEmpfangenLehmUnter > empfaenger.Dorfinformationen.SpeicherTotalLehm || sender.HandelVerschickenEisenUeber < sender.Dorfinformationen.SpeicherTotalEisen && empfaenger.HandelEmpfangenEisenUnter > empfaenger.Dorfinformationen.SpeicherTotalEisen)
                            if (TimeModulMarkt.GetLaufzeit(sender.Dorf.XCoordinate, sender.Dorf.YCoordinate, empfaenger.Dorf.XCoordinate, empfaenger.Dorf.YCoordinate).TotalMinutes < sender.HandelMaxLaufzeit) {
                                var holz = 0;
                                var lehm = 0;
                                var eisen = 0;

                                foreach (var handel in empfaenger.Handel.OrderBy(x => x.Priority))
                                    switch (handel.Ressource) {
                                        case "Wood":
                                            while (sender.Dorfinformationen.AvailableTraders > sender.HandelHaendlerZurueckHalten && sender.HandelVerschickenHolzUeber < sender.Dorfinformationen.SpeicherTotalHolz && empfaenger.HandelEmpfangenHolzUnter > empfaenger.Dorfinformationen.SpeicherTotalHolz) {
                                                holz += 1000;
                                                sender.Dorfinformationen.AvailableTraders -= 1;
                                            }
                                            break;
                                        case "Stone":
                                            while (sender.Dorfinformationen.AvailableTraders > sender.HandelHaendlerZurueckHalten && sender.HandelVerschickenLehmUeber < sender.Dorfinformationen.SpeicherTotalLehm && empfaenger.HandelEmpfangenLehmUnter > empfaenger.Dorfinformationen.SpeicherTotalLehm) {
                                                lehm += 1000;
                                                sender.Dorfinformationen.AvailableTraders -= 1;
                                            }
                                            break;
                                        case "Iron":
                                            while (sender.Dorfinformationen.AvailableTraders > sender.HandelHaendlerZurueckHalten && sender.HandelVerschickenEisenUeber < sender.Dorfinformationen.SpeicherTotalEisen && empfaenger.HandelEmpfangenEisenUnter > empfaenger.Dorfinformationen.SpeicherTotalEisen) {
                                                eisen += 1000;
                                                sender.Dorfinformationen.AvailableTraders -= 1;
                                            }
                                            break;
                                    }
                                transfers.Add(new Markettransfer(sender.Dorf, empfaenger.Dorf, holz, lehm, eisen));
                            }
            }
            foreach (var transfer in transfers) {
                await Marktplatz.SendMarketTransfer(transfer, browser, accountsettings);
                accountsettings.DoerferSettings.FirstOrDefault(x => x.Dorf.VillageId.Equals(transfer.EmpfaengerVillage.VillageId))?.Dorfinformationen.AnkommendeMarkttransfers.Add(transfer);
            }
        }


        private static async Task DoHandelsrouten(Browser browser, List<AccountDorfSettings> doerfer, Accountsetting accountsettings) {
            foreach (var dorf in doerfer) {
                await App.WaitForAttacks();
                DorfinformationenModul.DorfinformationenModul.UpdateVillageOverMarketplaceSending(browser, dorf, accountsettings);
                var holz = 0;
                var lehm = 0;
                var eisen = 0;
                var coords = dorf.HandelRouteCoords.Split('|');
                var xcoord = int.Parse(coords[0]);
                var ycoord = int.Parse(coords[1]);
                var empfangsdorf = accountsettings.DoerferSettings.FirstOrDefault(x => x.Dorf.XCoordinate.Equals(xcoord) && x.Dorf.YCoordinate.Equals(ycoord));

                if (empfangsdorf != null) {
                    if (dorf.Handel.FirstOrDefault(x => x.Ressource.Equals("Wood")).Active && dorf.Dorfinformationen.SpeicherTotalHolz > dorf.HandelVerschickenHolzUeber && empfangsdorf.HandelEmpfangenHolzUnter < empfangsdorf.Dorfinformationen.SpeicherTotalHolz) {
                        holz = dorf.Dorfinformationen.SpeicherTotalHolz - dorf.HandelVerschickenHolzUeber;
                        var unterschiedSpeicherMaxZuSpeicherEmpfang = empfangsdorf.Dorfinformationen.StorageMax - empfangsdorf.Dorfinformationen.SpeicherTotalHolz;
                        if (holz > unterschiedSpeicherMaxZuSpeicherEmpfang)
                            holz = unterschiedSpeicherMaxZuSpeicherEmpfang;
                    }
                    if (dorf.Handel.FirstOrDefault(x => x.Ressource.Equals("Stone")).Active && dorf.Dorfinformationen.SpeicherTotalLehm > dorf.HandelVerschickenLehmUeber && empfangsdorf.HandelEmpfangenLehmUnter < empfangsdorf.Dorfinformationen.SpeicherTotalLehm) {
                        lehm = dorf.Dorfinformationen.SpeicherTotalLehm - dorf.HandelVerschickenLehmUeber;
                        var unterschiedSpeicherMaxZuSpeicherEmpfang = empfangsdorf.Dorfinformationen.StorageMax - empfangsdorf.Dorfinformationen.SpeicherTotalLehm;
                        if (lehm > unterschiedSpeicherMaxZuSpeicherEmpfang)
                            lehm = unterschiedSpeicherMaxZuSpeicherEmpfang;
                    }
                    if (dorf.Handel.FirstOrDefault(x => x.Ressource.Equals("Iron")).Active && dorf.Dorfinformationen.SpeicherTotalEisen > dorf.HandelVerschickenEisenUeber && empfangsdorf.HandelEmpfangenEisenUnter < empfangsdorf.Dorfinformationen.SpeicherTotalEisen) {
                        eisen = dorf.Dorfinformationen.SpeicherTotalEisen - dorf.HandelVerschickenEisenUeber;
                        var unterschiedSpeicherMaxZuSpeicherEmpfang = empfangsdorf.Dorfinformationen.StorageMax - empfangsdorf.Dorfinformationen.SpeicherTotalEisen;
                        if (eisen > unterschiedSpeicherMaxZuSpeicherEmpfang)
                            eisen = unterschiedSpeicherMaxZuSpeicherEmpfang;
                    }

                    var gesamtTraders = (dorf.Dorfinformationen.AvailableTraders - dorf.HandelHaendlerZurueckHalten) * 1000;

                    var gesamt = holz + lehm + eisen;


                    if (gesamtTraders < gesamt) {
                        var unterschied = gesamt - gesamtTraders;
                        var abzug = unterschied / dorf.Handel.Count(x => x.Active);
                        if (dorf.Handel.FirstOrDefault(x => x.Ressource.Equals("Wood")).Active) {
                            holz -= abzug;
                            if (holz < 0)
                                holz = 0;
                        }
                        if (dorf.Handel.FirstOrDefault(x => x.Ressource.Equals("Stone")).Active) {
                            lehm -= abzug;
                            if (lehm < 0)
                                lehm = 0;
                        }
                        if (dorf.Handel.FirstOrDefault(x => x.Ressource.Equals("Iron")).Active) {
                            eisen -= abzug;
                            if (eisen < 0)
                                eisen = 0;
                        }
                    }
                    dorf.Dorfinformationen.AvailableTraders -= gesamtTraders / 1000;
                    var transfer = new Markettransfer(dorf.Dorf, empfangsdorf.Dorf, holz, lehm, eisen, DateTime.Now.Add(TimeModulMarkt.GetLaufzeit(dorf.Dorf.XCoordinate, dorf.Dorf.YCoordinate, empfangsdorf.Dorf.XCoordinate, empfangsdorf.Dorf.YCoordinate)));
                    await Marktplatz.SendMarketTransfer(transfer, browser, accountsettings);
                    empfangsdorf.Dorfinformationen.AnkommendeMarkttransfers.Add(transfer);
                    await Rndm.Sleep(100, 400);
                }
            }
        }

        public static async Task DoBuyPremiumDepotOffers(Browser browser, List<AccountDorfSettings> doerfer, Accountsetting accountsettings) {
            foreach (var dorf in doerfer) {
                await App.WaitForAttacks();
                var active = true;
                var start = DateTime.Now.AddMinutes(dorf.PremiumDepotStayMinutes);
                var counter = -1;
                var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=exchange";
                browser.WebDriver.Navigate().GoToUrl(link);
                browser.WebDriver.WaitForPageload();
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                while (active) {
                    if (counter == 2)
                        counter = -1;
                    counter++;

                    var premiumpoints = 1000000;

                    try {
                        premiumpoints = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Allgemein.PREMIUMPUNKTE_SELECTOR_XPATH)).Text.Trim());
                    }
                    // ReSharper disable once EmptyGeneralCatchClause
                    catch { }

                    if (premiumpoints > dorf.NotUnderPremiumPunkte && start > DateTime.Now) {
                        if (!browser.WebDriver.Url.Contains("exchange")) {
                            browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=exchange");
                        } else {
                            browser.WebDriver.Navigate().Refresh();
                            await Rndm.Sleep(50, 100);
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);


                        var gettingwood = 0;
                        var gettingstone = 0;
                        var gettingiron = 0;
                        var maxwood = double.Parse(browser.WebDriver.FindElement(By.Id("premium_exchange_stock_wood")).Text);
                        var maxstone = double.Parse(browser.WebDriver.FindElement(By.Id("premium_exchange_stock_stone")).Text);
                        var maxiron = double.Parse(browser.WebDriver.FindElement(By.Id("premium_exchange_stock_iron")).Text);
                        if (maxwood > dorf.HandelMinRessourcen || maxstone > dorf.HandelMinRessourcen || maxiron > dorf.HandelMinRessourcen) {
                            var ratios = browser.WebDriver.FindElements(By.XPath("//div[@class='premium-exchange-sep']")).ToList();
                            var ratioHolz = double.Parse(ratios[0].Text.Trim());
                            var ratioLehm = double.Parse(ratios[3].Text.Trim());
                            var ratioEisen = double.Parse(ratios[6].Text.Trim());
                            var handel = dorf.Handel.OrderBy(x => x.Priority).ToList()[counter];
                            if (handel.Active) {
                                if (handel.Ressource.Equals("Wood") && maxwood >= dorf.PremiumDepotHandelMinVerhaeltnis && ratioHolz > dorf.PremiumDepotHandelMinVerhaeltnis && ratioHolz < dorf.PremiumDepotHandelMaxVerhaeltnis) {
                                    var get = (int) maxwood;
                                    if (dorf.Dorfinformationen.SpeicherTotalHolz + get > dorf.Dorfinformationen.StorageMax)
                                        get = dorf.Dorfinformationen.StorageMax - dorf.Dorfinformationen.SpeicherTotalHolz;
                                    get = (int) (Convert.ToDouble(get) / ratioHolz * ratioHolz);
                                    gettingwood = get;
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.BUY_WOOD_SELECTOR_XPATH)).SendKeys(get.ToString());
                                } else if (handel.Ressource.Equals("Stone") && maxstone >= dorf.HandelMinRessourcen && ratioLehm > dorf.PremiumDepotHandelMinVerhaeltnis && ratioLehm < dorf.PremiumDepotHandelMaxVerhaeltnis) {
                                    var get = (int) maxstone;
                                    if (dorf.Dorfinformationen.SpeicherTotalLehm + get > dorf.Dorfinformationen.StorageMax)
                                        get = dorf.Dorfinformationen.StorageMax - dorf.Dorfinformationen.SpeicherTotalLehm;
                                    get = (int) (Convert.ToDouble(get) / ratioLehm * ratioLehm);
                                    gettingstone = get;
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.BUY_STONE_SELECTOR_XPATH)).SendKeys(get.ToString());
                                } else if (handel.Ressource.Equals("Iron") && maxiron >= dorf.HandelMinRessourcen && ratioEisen > dorf.PremiumDepotHandelMinVerhaeltnis && ratioEisen < dorf.PremiumDepotHandelMaxVerhaeltnis) {
                                    var get = (int) maxiron;
                                    if (dorf.Dorfinformationen.SpeicherTotalEisen + get > dorf.Dorfinformationen.StorageMax)
                                        get = dorf.Dorfinformationen.StorageMax - dorf.Dorfinformationen.SpeicherTotalEisen;
                                    get = (int) (Convert.ToDouble(get) / ratioEisen * ratioEisen);
                                    gettingiron = get;
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.BUY_IRON_SELECTOR_XPATH)).SendKeys(get.ToString());
                                }
                                if (gettingwood > 0 || gettingstone > 0 || gettingiron > 0) {
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.BUY_PREMIUM_BUTTON_SELECTOR_XPATH)).Click();
                                    await Rndm.Sleep(100, 200);
                                    try {
                                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.Inventar.ITEM_BESTAETIGEN_BUTTON_SELECTOR_XPATH)).Click();
                                        await Rndm.Sleep(5000, 5001);
                                    } catch {
                                        await Rndm.Sleep(50, 100);
                                        if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Markt.CANCELBUTTON_SELECTOR_XPATH)).Count > 0)
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.CANCELBUTTON_SELECTOR_XPATH)).Click();
                                    }
                                }
                            }
                        }
                        var transfer = new Markettransfer(gettingwood, gettingstone, gettingiron, DateTime.Now.AddMinutes(10));
                        dorf.Dorfinformationen.AnkommendeMarkttransfers.Add(transfer);
                    } else {
                        active = false;
                    }
                }
            }
        }

        public static async Task DoSellPremiumDepotOffers(Browser browser, List<AccountDorfSettings> doerfer, Accountsetting accountsettings) {
            foreach (var dorf in doerfer) {
                await App.WaitForAttacks();
                try {
                    var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=exchange";
                    browser.WebDriver.Navigate().GoToUrl(link);
                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);


                    var gettingwood = 0;
                    var gettingstone = 0;
                    var gettingiron = 0;
                    var ratios = browser.WebDriver.FindElements(By.XPath("//div[@class='premium-exchange-sep']")).ToList();
                    var ratioHolz = double.Parse(ratios[0].Text.Trim());
                    var ratioLehm = double.Parse(ratios[3].Text.Trim());
                    var ratioEisen = double.Parse(ratios[6].Text.Trim());

                    if (accountsettings.MultiaccountingDaten.Accountmode == Accountmode.Botaccount) {
                        dorf.Handel.First(x => x.Ressource.Equals("Wood")).Priority = (int) ratioHolz;
                        dorf.Handel.First(x => x.Ressource.Equals("Stone")).Priority = (int) ratioLehm;
                        dorf.Handel.First(x => x.Ressource.Equals("Iron")).Priority = (int) ratioEisen;
                    }

                    foreach (var handel in dorf.Handel.OrderBy(x => x.Priority)) {
                        var verfuegbarehaendler = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Uebersicht.VERFUEGBAREHAENDLER_SELECTOR_XPATH)).Text);
                        if (handel.Active && verfuegbarehaendler > 0)
                            do {
                                if (browser.WebDriver.FindElements(By.XPath("//td[@class='warn']")).Any()) {
                                    browser.WebDriver.FindElement(By.XPath("//button[@class='btn evt-cancel-btn btn-confirm-no']")).Click();
                                    await Rndm.Sleep(1000, 2000);
                                    ratios = browser.WebDriver.FindElements(By.XPath("//div[@class='premium-exchange-sep']")).ToList();
                                    ratioHolz = double.Parse(ratios[0].Text.Trim());
                                    ratioLehm = double.Parse(ratios[3].Text.Trim());
                                    ratioEisen = double.Parse(ratios[6].Text.Trim());
                                }
                                if (handel.Ressource.Equals("Wood") && dorf.Dorfinformationen.Speicher.Holz > dorf.HandelVerschickenHolzUeber && dorf.Dorfinformationen.Speicher.Holz >= dorf.HandelMinRessourcen && ratioHolz > dorf.PremiumDepotHandelMinVerhaeltnis && ratioHolz < dorf.PremiumDepotHandelMaxVerhaeltnis) {
                                    var get = (dorf.Dorfinformationen.Speicher.Holz - dorf.HandelVerschickenHolzUeber) * (dorf.PremiumDepotSellPercentage / 100);
                                    if (verfuegbarehaendler * 1000 < get)
                                        get = verfuegbarehaendler * 1000;
                                    if (get > 0) {
                                        get = get - (int) (get % ratioHolz) - (int) ratioHolz;
                                        if (get > dorf.Dorfinformationen.Speicher.Holz)
                                            get = dorf.Dorfinformationen.Speicher.Holz - dorf.HandelVerschickenHolzUeber - (int) (dorf.Dorfinformationen.Speicher.Holz % ratioHolz);
                                    }
                                    if (get <= 0)
                                        get = (int) ratioHolz;
                                    gettingwood += get;
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.SELL_WOOD_SELECTOR_XPATH)).Clear();
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.SELL_WOOD_SELECTOR_XPATH)).SendKeys(get.ToString());
                                } else if (handel.Ressource.Equals("Stone") && dorf.Dorfinformationen.Speicher.Lehm > dorf.HandelVerschickenLehmUeber && dorf.Dorfinformationen.Speicher.Lehm >= dorf.HandelMinRessourcen && ratioLehm > dorf.PremiumDepotHandelMinVerhaeltnis && ratioLehm < dorf.PremiumDepotHandelMaxVerhaeltnis) {
                                    var get = (dorf.Dorfinformationen.Speicher.Lehm - dorf.HandelVerschickenLehmUeber) * (dorf.PremiumDepotSellPercentage / 100);
                                    if (verfuegbarehaendler * 1000 < get)
                                        get = verfuegbarehaendler * 1000;
                                    if (get > 0) {
                                        get = get - (int) (get % ratioLehm) - (int) ratioLehm;
                                        if (get > dorf.Dorfinformationen.Speicher.Lehm)
                                            get = dorf.Dorfinformationen.Speicher.Lehm - dorf.HandelVerschickenLehmUeber - (int) (dorf.Dorfinformationen.Speicher.Lehm % ratioLehm);
                                    }
                                    if (get <= 0)
                                        get = (int) ratioLehm;
                                    gettingstone += get;
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.SELL_STONE_SELECTOR_XPATH)).Clear();
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.SELL_STONE_SELECTOR_XPATH)).SendKeys(get.ToString());
                                } else if (handel.Ressource.Equals("Iron") && dorf.Dorfinformationen.Speicher.Eisen > dorf.HandelVerschickenEisenUeber && dorf.Dorfinformationen.Speicher.Eisen >= dorf.HandelMinRessourcen && ratioEisen > dorf.PremiumDepotHandelMinVerhaeltnis && ratioEisen < dorf.PremiumDepotHandelMaxVerhaeltnis) {
                                    var get = (dorf.Dorfinformationen.Speicher.Eisen - dorf.HandelVerschickenEisenUeber) * (dorf.PremiumDepotSellPercentage / 100);
                                    if (verfuegbarehaendler * 1000 < get)
                                        get = verfuegbarehaendler * 1000;
                                    if (get > 0) {
                                        get = get - (int) (get % ratioEisen) - (int) ratioEisen;
                                        if (get > dorf.Dorfinformationen.Speicher.Eisen)
                                            get = dorf.Dorfinformationen.Speicher.Eisen - dorf.HandelVerschickenEisenUeber - (int) (dorf.Dorfinformationen.Speicher.Eisen % ratioEisen);
                                    }
                                    if (get <= 0)
                                        get = (int) ratioEisen;
                                    gettingiron += get;
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.SELL_IRON_SELECTOR_XPATH)).Clear();
                                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.SELL_IRON_SELECTOR_XPATH)).SendKeys(get.ToString());
                                }
                                if (gettingwood > 0 || gettingstone > 0 || gettingiron > 0) {
                                    var js = browser.WebDriver as IJavaScriptExecutor;
                                    js?.ExecuteScript("$('#chat-wrapper').hide();");
                                    browser.WebDriver.FindElement(By.XPath("//input[@class='btn float_right btn-premium-exchange-buy']")).Click();
                                    await Rndm.Sleep(300, 500);
                                    try {
                                        if (browser.WebDriver.FindElements(By.XPath("//td[@class='warn']")).Any()) {
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.CANCELBUTTON_SELECTOR_XPATH)).Click();
                                            await Rndm.Sleep(100, 200);
                                        }
                                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.Inventar.ITEM_BESTAETIGEN_BUTTON_SELECTOR_XPATH)).Click();
                                        await Rndm.Sleep(5000, 5001);
                                    } catch {
                                        await Rndm.Sleep(50, 100);
                                        if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Markt.CANCELBUTTON_SELECTOR_XPATH)).Any()) {
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Markt.CANCELBUTTON_SELECTOR_XPATH)).Click();
                                            await Rndm.Sleep(1000, 2000);
                                        }
                                    }
                                }
                            } while (browser.WebDriver.FindElements(By.XPath("//td[@class='warn']")).Any());
                    }
                    dorf.Dorfinformationen.Speicher.Holz -= gettingwood;
                    dorf.Dorfinformationen.Speicher.Lehm -= gettingstone;
                    dorf.Dorfinformationen.Speicher.Eisen -= gettingiron;
                } catch {
                    // ignored
                }
            }
        }
    }
}
