﻿namespace Ds_Bot_Reunited_NEWUI.Accountsettings
{
    [global::System.Reflection.Obfuscation(Exclude = true, Feature = "renaming")]
    public enum Accountmode {
        Botaccount,
        Mainaccount
    }
}