﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.AttackDetectionModul {
    public static class SeitenModul {
        public static List<int> GetSeiten(IWebDriver webDriver, bool forunterstuetzungeneinlesen = false) {
            SetPageSize(webDriver, forunterstuetzungeneinlesen);
            List<int> xx = new List<int>();
            try {
                List<IWebElement> element = webDriver.FindElements(By.ClassName("vis")).ToList();
                string[] seiten = element[7].Text.Trim().Split(' ');
                foreach (var link in seiten) {
                    string text = link.Replace("[", "").Replace("]", "").Replace(">", "").Replace("<", "");
                    if (!text.Contains(NAMECONSTANTS.GetNameForServer(Names.Alle))) {
                        xx.Add(int.Parse(text));
                    }
                }
            } catch {
                // ignored, keine 2 Seiten
                xx.Add(1);
            }
            if (!xx.Any(x => x.Equals(1))) {
                xx.Add(1);
            }
            return xx.OrderBy(x => x.ToString()).ToList();
        }

        public static void SetPage(Browser browser, int page) {
            try {
                IWebElement element = browser.WebDriver.FindElements(By.ClassName("vis"))[7];
                browser.ClickElement(element.FindElements(By.ClassName("paged-nav-item")).FirstOrDefault(x => x.Text.Contains(page.ToString())));
            } catch {
                // ignored
            }
        }

        private static void SetPageSize(IWebDriver webDriver, bool forunterstuetzungeneinlesen = false) {
            try {
                if (!webDriver.FindElement(By.Name("page_size")).GetAttribute("value").Contains(forunterstuetzungeneinlesen ? "25" : "100")) {
                    webDriver.FindElement(By.Name("page_size")).Clear();
                    Thread.Sleep(100);
                    webDriver.FindElement(By.Name("page_size")).SendKeys((forunterstuetzungeneinlesen ? "25" : "100") + "\n");
                    Thread.Sleep(100);
                    webDriver.Navigate().Refresh();
                    Thread.Sleep(500);
                }
            } catch {
                // ignored
            }
        }
    }
}
