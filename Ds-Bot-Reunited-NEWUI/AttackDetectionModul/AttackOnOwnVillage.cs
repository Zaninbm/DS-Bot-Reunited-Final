﻿using System;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.AttackDetectionModul {
    [Serializable]
    public class AttackOnOwnVillage {
        public DorfViewModel AttackerVillage { get; set; }
        public DorfViewModel DefenderVillage { get; set; }
        public DateTime Ankunftszeitpunkt { get; set; }
        public TimeSpan Laufzeit { get; set; }
        public string SlowestUnit { get; set; }
        public string AttackId { get; set; }
        public bool AlreadyPrevented { get; set; }
        public bool AlreadyRetimed { get; set; }
        public bool AlreadyZwischengetimed { get; set; }
        public bool AlreadyDefended { get; set; }
        public bool AlreadyRenamed { get; set; }

        // ReSharper disable once EmptyConstructor
        public AttackOnOwnVillage() { }
    }
}