﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.AttackDetectionModul;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.TimeModul;

namespace Ds_Bot_Reunited_NEWUI.AttackPreventionModul {
	public static class AttackPreventionModul {
		public static void PreventAttacks(Accountsetting accountsettings) {
			try {
				while (DorfinformationenModul.DorfinformationenModul.TruppeneinlesenInDoing) Thread.Sleep(500);
				var bewegungenToImport = new List<TruppenbewegungViewModel>();
				if (App.Settings.SendouttroopsActive)
					foreach (var attack in accountsettings
					                       .AngriffeAufAccount.Where(x => x.Ankunftszeitpunkt > DateTime.Now)
					                       .OrderBy(x => x.Ankunftszeitpunkt)) {
						var village =
							accountsettings.DoerferSettings.First(x => x.Dorf.PlayerId.Equals(attack.DefenderVillage
							                                                                        .PlayerId));
						var bewegung = new TruppenbewegungViewModel(attack.DefenderVillage
						                                            , App.VillagelistWholeWorld.First().Value
						                                            , attack.Ankunftszeitpunkt
						                                            , new TruppenTemplateViewModel(village
						                                                                           .Dorfinformationen
						                                                                           .Truppen)) {
							                                                                                      Rausstellen
								                                                                                      = true
						                                                                                      };
						bewegungenToImport.Add(bewegung);
					}

				if (App.Settings.RetimeActive) SetRetimes(accountsettings);
				//UpdateUnitsinterval NUTZEN
				if (App.Settings.EmptyRessourcesActive) {
					//Rohstoffe verbrauchen
					//RecruitToEmptyRessourcesActive
					//BuildToEmptyRessourcesActive
					//Timed Rohstoffverbrauch? Wie Bewegung????
				}

				var nobleBewegungen = PreventNobleAttacks(accountsettings);
				if (nobleBewegungen != null) bewegungenToImport.AddRange(nobleBewegungen);
				var bewegungen = PreventNormalAttacks(accountsettings);
				if (nobleBewegungen != null) bewegungenToImport.AddRange(bewegungen);
				if (bewegungenToImport.Any())
					foreach (var bewegung in bewegungenToImport)
						accountsettings.GeplanteBewegungen.Add(bewegung.Model);
			} catch (Exception) {
				App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
					               ? accountsettings.Uvname
					               : accountsettings.Accountname) + ": " +
				              " Attack Prevention" + " " + Resources.Failure);
			}
		}

		public static List<TruppenbewegungViewModel> SetRetimes(Accountsetting accountsettings) {
			var bewegungen = new List<TruppenbewegungViewModel>();
			var attacks = accountsettings.AngriffeAufAccount.Where(x => x.Ankunftszeitpunkt > DateTime.Now)
			                             .OrderBy(x => x.Ankunftszeitpunkt).ToList();
			foreach (var attack in attacks) {
				var ts = attack.Ankunftszeitpunkt - DateTime.Now;
				if (ts.TotalMinutes > App.Settings.RetimeMaxLaufzeit) {
					var list = GetNachbardoerfer(attack.AttackerVillage.XCoordinate, attack.AttackerVillage.YCoordinate
					                             , attack.Ankunftszeitpunkt, false, false, accountsettings
					                             , App.Settings.NeighbourMaxRadius);
					if (list.Any()) {
						var village = list.First();
						var dorf =
							accountsettings.DoerferSettings.First(x => x.Dorf.VillageId.Equals(village.VillageId));
						var templatetoSend = village.Templates.First();

						var bewegung = new TruppenbewegungViewModel(dorf.Dorf, attack.AttackerVillage
						                                            , attack.Ankunftszeitpunkt.Add(attack.Laufzeit)
						                                            , new TruppenTemplateViewModel(templatetoSend)) {
							                                                                                            GenauTimenIsActive
								                                                                                            = true
							                                                                                            , Angriffsinformationen
								                                                                                            = Resources
									                                                                                            .Retiming
							                                                                                            , BewegungsartSelected
								                                                                                            = Resources
									                                                                                            .Support
						                                                                                            };

						bewegungen.Add(bewegung);
					}
				}
			}

			return bewegungen;
		}

		public static List<TruppenbewegungViewModel> PreventNormalAttacks(Accountsetting accountsettings) {
			if (accountsettings.DoerferSettings.Count > 1) {
				var bewegungen = new List<TruppenbewegungViewModel>();
				var attacks = accountsettings.AngriffeAufAccount.Where(x => x.Ankunftszeitpunkt > DateTime.Now)
				                             .OrderBy(x => x.Ankunftszeitpunkt).ToList();
				var templateGebraucht =
					App.Settings.DefendingTemplates.First(x => x.TemplateName.Contains(Resources.Defending));
				foreach (var attack in attacks)
					if (!attack.AlreadyPrevented)
						if (attack.SlowestUnit != Resources.Snob && attack.SlowestUnit != Resources.Spy) {
							var nachbardoerfer = GetNachbardoerfer(attack.DefenderVillage.XCoordinate
							                                       , attack.DefenderVillage.YCoordinate
							                                       , attack.Ankunftszeitpunkt, false, true
							                                       , accountsettings, App.Settings.NeighbourMaxRadius);

							var gesamtAnkommendeTheoretischeTruppen =
								GetGesamteTruppenWelcheAnkommen(nachbardoerfer, accountsettings);

							if (nachbardoerfer.Any())
								if (CompareTemplates(gesamtAnkommendeTheoretischeTruppen, templateGebraucht)) {
									var gesamtversendete = new TruppenTemplate();
									AddOnTemplate(gesamtversendete
									              , GetLaufendeTruppenFuerDiesesAg(attack.DefenderVillage.VillageId
									                                               , attack.Ankunftszeitpunkt
									                                               , accountsettings));
									var counter = 0;
									while (!CompareTemplates(gesamtversendete, templateGebraucht) &&
									       nachbardoerfer.Count - 1 > counter) {
										var village = nachbardoerfer[counter++];
										var dorf =
											accountsettings.DoerferSettings.First(x =>
												                                      x
													                                      .Dorf.VillageId
													                                      .Equals(village.VillageId));
										var templatetoSend = village.Templates.First();

										if (templatetoSend.SpeertraegerAnzahl > 0 ||
										    templatetoSend.SchwertkaempferAnzahl > 0 ||
										    templatetoSend.BogenschuetzenAnzahl > 0 ||
										    templatetoSend.BeritteneBogenschuetzenAnzahl > 0 ||
										    templatetoSend.SchwereKavallerieAnzahl > 0) {
											var bewegung =
												new TruppenbewegungViewModel(dorf.Dorf, attack.DefenderVillage
												                             , attack.Ankunftszeitpunkt.AddSeconds(-10)
												                             , new
													                             TruppenTemplateViewModel(templatetoSend)) {
														                                                                       GenauTimenIsActive
															                                                                       = true
														                                                                       , Angriffsinformationen
															                                                                       = Resources
																                                                                       .Defending
														                                                                       , BewegungsartSelected
															                                                                       = Resources
																                                                                       .Support
													                                                                       };

											bewegungen.Add(bewegung);
											AddOnTemplate(gesamtversendete, templatetoSend);
										}
									}

									if (bewegungen.Any()) {
										attack.AlreadyPrevented = true;
										attack.AlreadyZwischengetimed = true;
									}
								}
						}

				if (bewegungen.Any())
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " +
					              bewegungen.Count + " Bewegungen erstellt");
				return bewegungen;
			}

			return null;
		}

		public static List<TruppenbewegungViewModel> PreventNobleAttacks(Accountsetting accountsettings) {
			if (accountsettings.DoerferSettings.Count > 1) {
				var bewegungen = new List<TruppenbewegungViewModel>();
				var attacks = accountsettings.AngriffeAufAccount.Where(x => x.Ankunftszeitpunkt > DateTime.Now)
				                             .OrderBy(x => x.Ankunftszeitpunkt).ToList();
				var templateGebraucht = App.Settings.TabbingTemplates
				                           .First(x => x.TemplateName.Contains(Resources.Tabbing)).Clone();
				AddOnTemplate(templateGebraucht, templateGebraucht);
				AddOnTemplate(templateGebraucht, templateGebraucht);
				AddOnTemplate(templateGebraucht, templateGebraucht);
				foreach (var attack in attacks)
					if (!attack.AlreadyPrevented)
						if (attack.SlowestUnit == Resources.Snob) {
							var aGattacksonthisVillage =
								accountsettings.AngriffeAufAccount
								               .Where(x => x.SlowestUnit == Resources.Snob &&
								                           x.DefenderVillage.VillageId.Equals(attack.DefenderVillage
								                                                                    .VillageId))
								               .OrderBy(x => x.Ankunftszeitpunkt).ToList();
							var index = aGattacksonthisVillage.IndexOf(attack);

							var nachbardoerfer = GetNachbardoerfer(attack.DefenderVillage.XCoordinate
							                                       , attack.DefenderVillage.YCoordinate
							                                       , attack.Ankunftszeitpunkt, true, true
							                                       , accountsettings, App.Settings.NeighbourMaxRadius);
							var gesamtAnkommendeTheoretischeTruppen =
								GetGesamteTruppenWelcheAnkommen(nachbardoerfer, accountsettings);
							if (nachbardoerfer.Any()) {
								var unterschied = index != 0
									                  ? (aGattacksonthisVillage[index].Ankunftszeitpunkt -
									                     aGattacksonthisVillage[index - 1].Ankunftszeitpunkt)
									                  .TotalMilliseconds
									                  : 0;
								if (CompareTemplates(gesamtAnkommendeTheoretischeTruppen, templateGebraucht)) {
									var gesamtversendete = new TruppenTemplate();
									AddOnTemplate(gesamtversendete
									              , GetLaufendeTruppenFuerDiesesAg(attack.DefenderVillage.VillageId
									                                               , attack.Ankunftszeitpunkt
									                                               , accountsettings));
									var counter = 0;
									while (!CompareTemplates(gesamtversendete, templateGebraucht) &&
									       nachbardoerfer.Count - 1 > counter) {
										var village = nachbardoerfer[counter++];
										var dorf =
											accountsettings.DoerferSettings.First(x =>
												                                      x
													                                      .Dorf.VillageId
													                                      .Equals(village.VillageId));
										var templatetoSend = village.Templates.First();
										if (templatetoSend.SpeertraegerAnzahl > 0 ||
										    templatetoSend.SchwertkaempferAnzahl > 0 ||
										    templatetoSend.BogenschuetzenAnzahl > 0 ||
										    templatetoSend.BeritteneBogenschuetzenAnzahl > 0 ||
										    templatetoSend.SchwereKavallerieAnzahl > 0) {
											var bewegung =
												new TruppenbewegungViewModel(dorf.Dorf, attack.DefenderVillage
												                             , attack.Ankunftszeitpunkt
												                                     .AddMilliseconds(unterschied > 199
													                                                      ? -110
													                                                      : -30)
												                             , new
													                             TruppenTemplateViewModel(templatetoSend)) {
														                                                                       GenauTimenIsActive
															                                                                       = true
														                                                                       , Angriffsinformationen
															                                                                       = Resources
																                                                                       .Tabbing
														                                                                       , BewegungsartSelected
															                                                                       = Resources
																                                                                       .Support
													                                                                       };

											bewegungen.Add(bewegung);
											AddOnTemplate(gesamtversendete, templatetoSend);
										}
									}

									if (bewegungen.Any()) {
										attack.AlreadyPrevented = true;
										attack.AlreadyZwischengetimed = true;
									}
								}
							}
						}

				if (bewegungen.Any())
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " +
					              bewegungen.Count + " Bewegungen erstellt");
				return bewegungen;
			}

			return null;
		}

		private static TruppenTemplate GetLaufendeTruppenFuerDiesesAg(int villageid
		                                                              , DateTime date
		                                                              , Accountsetting accountsettings) {
			var template = new TruppenTemplate();
			foreach (var befehl in
				accountsettings.GeplanteBewegungen.Where(x => x.Ankunftszeitpunkt.AddMilliseconds(100) > date &&
				                                              x.Ankunftszeitpunkt.AddMilliseconds(-100) < date &&
				                                              x.EmpfaengerDorf.VillageId.Equals(villageid))) {
				template.SpeertraegerAnzahl += befehl.SpeertraegerAnzahl;
				template.SchwertkaempferAnzahl += befehl.SchwertkaempferAnzahl;
				template.BogenschuetzenAnzahl += befehl.BogenschuetzenAnzahl;
				template.SpaeherAnzahl += befehl.SpaeherAnzahl;
				template.SchwertkaempferAnzahl += befehl.SchwereKavallerieAnzahl;
				template.PaladinAnzahl += befehl.PaladinAnzahl;
			}

			foreach (var befehl in accountsettings.Befehle.Where(x => x.Ankunftszeitpunkt.AddMilliseconds(100) > date &&
			                                                          x.Ankunftszeitpunkt.AddMilliseconds(-100) <
			                                                          date && x.EmpfaengerVillageId.Equals(villageid) &&
			                                                          x.Befehlsart == Resources.Support)) {
				template.SpeertraegerAnzahl += befehl.SpeertraegerAnzahl;
				template.SchwertkaempferAnzahl += befehl.SchwertkaempferAnzahl;
				template.BogenschuetzenAnzahl += befehl.BogenschuetzenAnzahl;
				template.SpaeherAnzahl += befehl.SpaeherAnzahl;
				template.SchwertkaempferAnzahl += befehl.SchwereKavallerieAnzahl;
				template.PaladinAnzahl += befehl.PaladinAnzahl;
			}

			return template;
		}

		public static bool BothAttacksAreSameAbsendeDorf(List<AttackOnOwnVillage> attacks, int index) {
			if (index != 0)
				if (!attacks[index].AttackerVillage.PlayerId.Equals(attacks[index - 1].AttackerVillage.PlayerId))
					return false;
			if (index == 0) return false;
			return true;
		}

		public static bool CompareTemplates(TruppenTemplate hat, TruppenTemplate soll) {
			if (
				hat.SpeertraegerAnzahl <= soll.SpeertraegerAnzahl &&
				soll.SpeertraegerAnzahl >
				0 /*&& (hat.SchwertkaempferAnzahl <= soll.SchwertkaempferAnzahl && soll.SchwertkaempferAnzahl > 0|| hat.BogenschuetzenAnzahl <= soll.BogenschuetzenAnzahl && soll.BogenschuetzenAnzahl > 0|| hat.SchwereKavallerieAnzahl <= soll.SchwereKavallerieAnzahl && soll.SchwereKavallerieAnzahl > 0)*/
			)
				return false;
			if (hat.SchwertkaempferAnzahl <= soll.SchwertkaempferAnzahl &&
			    soll.SchwertkaempferAnzahl >
			    0 /*&&(hat.BogenschuetzenAnzahl <= soll.BogenschuetzenAnzahl && soll.BogenschuetzenAnzahl > 0|| hat.SchwereKavallerieAnzahl <= soll.SchwereKavallerieAnzahl && soll.SchwereKavallerieAnzahl > 0)*/
			)
				return false;
			if (hat.BogenschuetzenAnzahl <= soll.BogenschuetzenAnzahl &&
			    soll.BogenschuetzenAnzahl >
			    0 /*&& hat.SchwereKavallerieAnzahl <= soll.SchwereKavallerieAnzahl && soll.SchwereKavallerieAnzahl > 0*/
			)
				return false;
			if (hat.SchwereKavallerieAnzahl <= soll.SchwereKavallerieAnzahl && soll.SchwereKavallerieAnzahl > 0)
				return false;

			return true;
		}

		public static TruppenTemplate UnterschiedTemplates(TruppenTemplate hat, TruppenTemplate soll) {
			var template = new TruppenTemplate {
				                                   SpeertraegerAnzahl = hat.SpeertraegerAnzahl - soll.SpeertraegerAnzahl
				                                   , SchwertkaempferAnzahl =
					                                   hat.SchwertkaempferAnzahl - soll.SchwertkaempferAnzahl
				                                   , BogenschuetzenAnzahl =
					                                   hat.BogenschuetzenAnzahl - soll.BogenschuetzenAnzahl
				                                   , AxtkaempferAnzahl = hat.AxtkaempferAnzahl - soll.AxtkaempferAnzahl
				                                   , SpaeherAnzahl = hat.SpaeherAnzahl - soll.SpaeherAnzahl
				                                   , LeichteKavallerieAnzahl =
					                                   hat.LeichteKavallerieAnzahl - soll.LeichteKavallerieAnzahl
				                                   , BeritteneBogenschuetzenAnzahl =
					                                   hat.BeritteneBogenschuetzenAnzahl -
					                                   soll.BeritteneBogenschuetzenAnzahl
				                                   , SchwereKavallerieAnzahl =
					                                   hat.SchwereKavallerieAnzahl - soll.SchwereKavallerieAnzahl
				                                   , PaladinAnzahl = hat.PaladinAnzahl - soll.PaladinAnzahl
			                                   };
			if (template.SpeertraegerAnzahl < 0) template.SpeertraegerAnzahl = 0;
			if (template.SchwertkaempferAnzahl < 0) template.SchwertkaempferAnzahl = 0;
			if (template.AxtkaempferAnzahl < 0) template.AxtkaempferAnzahl = 0;
			if (template.BogenschuetzenAnzahl < 0) template.BogenschuetzenAnzahl = 0;
			if (template.SpaeherAnzahl < 0) template.SpaeherAnzahl = 0;
			if (template.LeichteKavallerieAnzahl < 0) template.LeichteKavallerieAnzahl = 0;
			if (template.BeritteneBogenschuetzenAnzahl < 0) template.BeritteneBogenschuetzenAnzahl = 0;
			if (template.SchwereKavallerieAnzahl < 0) template.SchwereKavallerieAnzahl = 0;
			return template;
		}

		private static TruppenTemplate GetGesamteTruppenWelcheAnkommen(List<AttackPreventionVillage> doerfer
		                                                               , Accountsetting accountsettings) {
			var template = new TruppenTemplate {TemplateName = ""};
			foreach (var dorf in doerfer) {
				var dorff =
					accountsettings.DoerferSettings.FirstOrDefault(x => x.Dorf.VillageId.Equals(dorf.VillageId));
				if (dorff != null) {
					template.SpeertraegerAnzahl += dorff.Dorfinformationen.Truppen.SpeertraegerAnzahl;
					template.SchwertkaempferAnzahl += dorff.Dorfinformationen.Truppen.SchwertkaempferAnzahl;
					template.BogenschuetzenAnzahl += dorff.Dorfinformationen.Truppen.BogenschuetzenAnzahl;
					template.BeritteneBogenschuetzenAnzahl +=
						dorff.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl;
					template.SchwereKavallerieAnzahl += dorff.Dorfinformationen.Truppen.SchwereKavallerieAnzahl;
				}
			}

			return template;
		}

		public static void AddOnTemplate(TruppenTemplate template, TruppenTemplate add) {
			template.SpeertraegerAnzahl += add.SpeertraegerAnzahl;
			template.SchwertkaempferAnzahl += add.SchwertkaempferAnzahl;
			template.BogenschuetzenAnzahl += add.BogenschuetzenAnzahl;
			template.BeritteneBogenschuetzenAnzahl += add.BeritteneBogenschuetzenAnzahl;
			template.SchwereKavallerieAnzahl += add.SchwereKavallerieAnzahl;
		}

		public static void RemoveFromTemplate(TruppenTemplate template, TruppenTemplate remove) {
			template.SpeertraegerAnzahl -= remove.SpeertraegerAnzahl;
			template.SchwertkaempferAnzahl -= remove.SchwertkaempferAnzahl;
			template.BogenschuetzenAnzahl -= remove.BogenschuetzenAnzahl;
			template.BeritteneBogenschuetzenAnzahl -= remove.BeritteneBogenschuetzenAnzahl;
			template.SchwereKavallerieAnzahl -= remove.SchwereKavallerieAnzahl;
		}

		public static List<AttackPreventionVillage> GetNachbardoerfer(int x
		                                                              , int y
		                                                              , DateTime spaetesteAnkunft
		                                                              , bool noble
		                                                              , bool defend
		                                                              , Accountsetting account
		                                                              , double maxFelder) {
			var nachbardoerfer = new List<AttackPreventionVillage>();

			foreach (var dorf in account.DoerferSettings) {
				var tempx = (x - dorf.Dorf.Dorf.XCoordinate) * (x - dorf.Dorf.Dorf.XCoordinate);
				var tempy = (y - dorf.Dorf.Dorf.YCoordinate) * (y - dorf.Dorf.Dorf.YCoordinate);
				var felder = Math.Sqrt(tempx + tempy);
				if (felder < maxFelder) {
					var searchTuple = new VillageTuple<int, int>(dorf.Dorf.XCoordinate, dorf.Dorf.YCoordinate);
					var unit = GetSlowestUnitThatIsPossibleForSupport(dorf.Dorf.Dorf
					                                                  , App.VillagelistWholeWorld[searchTuple].Dorf
					                                                  , spaetesteAnkunft);
					if (unit != Resources.Spy) {
						var village = new AttackPreventionVillage {
							                                          VillageId = dorf.Dorf.Dorf.VillageId
							                                          , LangsamsteEinheit = unit
							                                          , Felder = felder
							                                          , Templates = new List<TruppenTemplate>()
						                                          };

						var templatequeue = new Queue<TruppenTemplate>();


						var template = defend
							               ? (noble
								                  ? App.Settings.TabbingTemplates.First()
								                  : App.Settings.DefendingTemplates.First(k =>
									                                                          k
										                                                          .TemplateName
										                                                          .Equals(Resources
											                                                                  .DefendMinTemplate))
							                 )
							               : App.Settings.RetimeTemplates.First();
						var templateMax = defend
							                  ? (noble
								                     ? null
								                     : App.Settings.DefendingTemplates.First(k =>
									                                                             k
										                                                             .TemplateName
										                                                             .Equals(Resources
											                                                                     .DefendMaxTemplate))
							                    )
							                  : null;
						var truppenverfuegbar = DorfinformationenModul
						                        .DorfinformationenModul.GetTroopsOfVillage(dorf.Dorf.VillageId).Clone();
						var holdback = App.Settings.HoldBackTemplates.First();
						RemoveFromTemplate(truppenverfuegbar, holdback);

						bool hasenoughtroops;
						do {
							hasenoughtroops =
								(village.LangsamsteEinheit.Equals(Resources.Ram) ||
								 village.LangsamsteEinheit.Equals(Resources.Sword) ||
								 village.LangsamsteEinheit.Equals(Resources.Spear)) &&
								truppenverfuegbar.SpeertraegerAnzahl > template.SpeertraegerAnzahl &&
								truppenverfuegbar.SpeertraegerAnzahl > 0 && template.SpeertraegerAnzahl != 0 ||
								(village.LangsamsteEinheit.Equals(Resources.Ram) ||
								 village.LangsamsteEinheit.Equals(Resources.Sword)) &&
								truppenverfuegbar.SchwertkaempferAnzahl > template.SchwertkaempferAnzahl &&
								truppenverfuegbar.SchwertkaempferAnzahl > 0 && template.SchwertkaempferAnzahl != 0 ||
								(village.LangsamsteEinheit.Equals(Resources.Ram) ||
								 village.LangsamsteEinheit.Equals(Resources.Sword) ||
								 village.LangsamsteEinheit.Equals(Resources.Spear)) &&
								truppenverfuegbar.BogenschuetzenAnzahl > template.BogenschuetzenAnzahl &&
								truppenverfuegbar.BogenschuetzenAnzahl > 0 && template.BogenschuetzenAnzahl != 0 ||
								(village.LangsamsteEinheit.Equals(Resources.Ram) ||
								 village.LangsamsteEinheit.Equals(Resources.Heavy) ||
								 village.LangsamsteEinheit.Equals(Resources.Marcher) ||
								 village.LangsamsteEinheit.Equals(Resources.Spear) ||
								 village.LangsamsteEinheit.Equals(Resources.Sword)) &&
								truppenverfuegbar.SpaeherAnzahl > template.SpaeherAnzahl &&
								truppenverfuegbar.SpaeherAnzahl > 0 && template.SpaeherAnzahl != 0 ||
								(village.LangsamsteEinheit.Equals(Resources.Ram) ||
								 village.LangsamsteEinheit.Equals(Resources.Sword) ||
								 village.LangsamsteEinheit.Equals(Resources.Spear)) &&
								truppenverfuegbar.SchwereKavallerieAnzahl > template.SchwereKavallerieAnzahl &&
								truppenverfuegbar.SchwereKavallerieAnzahl > 0 &&
								template.SchwereKavallerieAnzahl != 0 ||
								(village.LangsamsteEinheit.Equals(Resources.Ram) ||
								 village.LangsamsteEinheit.Equals(Resources.Heavy) ||
								 village.LangsamsteEinheit.Equals(Resources.Marcher) ||
								 village.LangsamsteEinheit.Equals(Resources.Spear) ||
								 village.LangsamsteEinheit.Equals(Resources.Sword)) &&
								truppenverfuegbar.PaladinAnzahl > template.PaladinAnzahl &&
								truppenverfuegbar.PaladinAnzahl > 0 && template.PaladinAnzahl != 0;
							if (hasenoughtroops) {
								var template2 = new TruppenTemplate {
									                                    SpeertraegerAnzahl =
										                                    templateMax != null &&
										                                    truppenverfuegbar.SpeertraegerAnzahl >
										                                    templateMax.SpeertraegerAnzahl
											                                    ? templateMax.SpeertraegerAnzahl
											                                    : truppenverfuegbar.SpeertraegerAnzahl
									                                    , SchwertkaempferAnzahl =
										                                    templateMax != null &&
										                                    truppenverfuegbar.SchwertkaempferAnzahl >
										                                    templateMax.SchwertkaempferAnzahl
											                                    ? templateMax.SchwertkaempferAnzahl
											                                    : truppenverfuegbar
												                                    .SchwertkaempferAnzahl
									                                    , BogenschuetzenAnzahl =
										                                    templateMax != null &&
										                                    truppenverfuegbar.BogenschuetzenAnzahl >
										                                    templateMax.BogenschuetzenAnzahl
											                                    ? templateMax.BogenschuetzenAnzahl
											                                    : truppenverfuegbar.BogenschuetzenAnzahl
									                                    , SpaeherAnzahl =
										                                    templateMax != null &&
										                                    truppenverfuegbar.SpaeherAnzahl >
										                                    templateMax.SpaeherAnzahl
											                                    ? templateMax.SpaeherAnzahl
											                                    : truppenverfuegbar.SpaeherAnzahl
									                                    , SchwereKavallerieAnzahl =
										                                    templateMax != null &&
										                                    truppenverfuegbar.SchwereKavallerieAnzahl >
										                                    templateMax.SchwereKavallerieAnzahl
											                                    ? templateMax.SchwereKavallerieAnzahl
											                                    : truppenverfuegbar
												                                    .SchwereKavallerieAnzahl
									                                    , PaladinAnzahl =
										                                    templateMax != null &&
										                                    truppenverfuegbar.PaladinAnzahl >
										                                    templateMax.PaladinAnzahl
											                                    ? templateMax.PaladinAnzahl
											                                    : truppenverfuegbar.PaladinAnzahl
								                                    };

								templatequeue.Enqueue(template2);
								truppenverfuegbar.SpeertraegerAnzahl -= template2.SpeertraegerAnzahl;
								truppenverfuegbar.SchwertkaempferAnzahl -= template2.SchwertkaempferAnzahl;
								truppenverfuegbar.BogenschuetzenAnzahl -= template2.BogenschuetzenAnzahl;
								truppenverfuegbar.SpaeherAnzahl -= template2.SpaeherAnzahl;
								truppenverfuegbar.SchwereKavallerieAnzahl -= template2.SchwereKavallerieAnzahl;
								truppenverfuegbar.PaladinAnzahl -= template2.PaladinAnzahl;
							}
						} while (hasenoughtroops);


						if (templatequeue.Any()) {
							foreach (var templat in templatequeue) village.Templates.Add(templat);
							nachbardoerfer.Add(village);
						}
					}
				}
			}


			return nachbardoerfer.OrderByDescending(x2 => x2.Felder).ToList();
		}

		private static string GetSlowestUnitThatIsPossibleForSupport(Dorf dorfeins, Dorf dorfzwei, DateTime ankunft) {
			if (DateTime.Now.AddMinutes(20)
			            .Add(Laufzeit.GetTravelTime(dorfeins, dorfzwei, new TruppenTemplate {RammboeckeAnzahl = 1})) <
			    ankunft)
				return Resources.Ram;
			if (DateTime.Now.AddMinutes(20)
			            .Add(Laufzeit.GetTravelTime(dorfeins, dorfzwei
			                                        , new TruppenTemplate {SchwertkaempferAnzahl = 1})) < ankunft)
				return Resources.Sword;
			if (DateTime.Now.AddMinutes(20)
			            .Add(Laufzeit.GetTravelTime(dorfeins, dorfzwei
			                                        , new TruppenTemplate {SpeertraegerAnzahl = 1})) < ankunft)
				return Resources.Spear;
			if (DateTime.Now.AddMinutes(20)
			            .Add(Laufzeit.GetTravelTime(dorfeins, dorfzwei
			                                        , new TruppenTemplate {SchwereKavallerieAnzahl = 1})) < ankunft)
				return Resources.Heavy;
			if (DateTime.Now.AddMinutes(20)
			            .Add(Laufzeit.GetTravelTime(dorfeins, dorfzwei
			                                        , new TruppenTemplate {BeritteneBogenschuetzenAnzahl = 1})) <
			    ankunft)
				return Resources.Marcher;
			return Resources.Spy;
		}
	}
}