﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;

namespace Ds_Bot_Reunited_NEWUI.BauenModul {
    [Serializable]
    public class Bauelement : INotifyPropertyChanged {
        private int _targetLevel;
        private int _priority;
        private bool _isSelected;
        private Gebaeude _gebaeude;
        private string _imageSource;

        public int TargetLevel {
            get => _targetLevel;
            set {
                _targetLevel = value;
                OnPropertyChanged();
            }
        }

        public int Priority {
            get => _priority;
            set {
                _priority = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected {
            get => _isSelected;
            set {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public Gebaeude Gebaeude {
            get => _gebaeude;
            set {
                _gebaeude = value;
                OnPropertyChanged();
            }
        }

        public string ImageSource {
            get {
                if(string.IsNullOrEmpty(_imageSource))
                    _imageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude);
                return _imageSource;
            }
            set {
                _imageSource = value;
                OnPropertyChanged();
            }
        }

        public Bauelement() { }

        public Bauelement(int priority, int targetLevel, Gebaeude gebaeude) {
            Priority = priority;
            TargetLevel = targetLevel;
            Gebaeude = gebaeude;
        }

        public Bauelement Clone() {
            Bauelement newBauelement = new Bauelement (Priority, TargetLevel,Gebaeude);
            return newBauelement;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public static class MaxStufe {
        public static int GetMaxStufeOfGebaeude(Gebaeude gebaeude) {
            switch (gebaeude) {
                case Gebaeude.Hauptgebaeude:
                    return 30;
                case Gebaeude.Kaserne:
                    return 25;
                case Gebaeude.Stall:
                    return 20;
                case Gebaeude.Werkstatt:
                    return 15;
                case Gebaeude.Schmiede:
                    return 20;
                case Gebaeude.Adelshof:
                    if (App.Settings.Weltensettings.AhTillLevel3)
                        return 3;
                    return 1;
                case Gebaeude.Versammlungsplatz:
                    return 1;
                case Gebaeude.Statue:
                    return 1;
                case Gebaeude.Marktplatz:
                    return 25;
                case Gebaeude.Holzfaeller:
                    return 30;
                case Gebaeude.Lehmgrube:
                    return 30;
                case Gebaeude.Eisenmine:
                    return 30;
                case Gebaeude.Erstekirche:
                    return 1;
                case Gebaeude.Kirche:
                    return 3;
                case Gebaeude.Speicher:
                    return 30;
                case Gebaeude.Bauernhof:
                    return 30;
                case Gebaeude.Versteck:
                    return 10;
                case Gebaeude.Wall:
                    return 20;
                case Gebaeude.WatchTower:
                    return 15;
            }
            return 0;
        }
    }

    public static class GebaeudeLinks {
        public static string GetImageSourceOfGebaeude(Gebaeude gebaeude) {
            switch (gebaeude) {
                case Gebaeude.Hauptgebaeude:
                    return "pack://siteoforigin:,,,/TroopImages\\Main.png";
                case Gebaeude.Kaserne:
                    return "pack://siteoforigin:,,,/TroopImages\\Barracks.png";
                case Gebaeude.Stall:
                    return "pack://siteoforigin:,,,/TroopImages\\Stable.png";
                case Gebaeude.Werkstatt:
                    return "pack://siteoforigin:,,,/TroopImages\\Garage.png";
                case Gebaeude.Schmiede:
                    return "pack://siteoforigin:,,,/TroopImages\\Smith.png";
                case Gebaeude.Adelshof:
                    return "pack://siteoforigin:,,,/TroopImages\\Snob.png";
                case Gebaeude.Versammlungsplatz:
                    return "pack://siteoforigin:,,,/TroopImages\\Place.png";
                case Gebaeude.Statue:
                    return "pack://siteoforigin:,,,/TroopImages\\Statue.png";
                case Gebaeude.Marktplatz:
                    return "pack://siteoforigin:,,,/TroopImages\\Market.png";
                case Gebaeude.Holzfaeller:
                    return "pack://siteoforigin:,,,/TroopImages\\Wood.png";
                case Gebaeude.Lehmgrube:
                    return "pack://siteoforigin:,,,/TroopImages\\Stone.png";
                case Gebaeude.Eisenmine:
                    return "pack://siteoforigin:,,,/TroopImages\\Iron.png";
                case Gebaeude.Erstekirche:
                    return "pack://siteoforigin:,,,/TroopImages\\Church_f1.png";
                case Gebaeude.Kirche:
                    return "pack://siteoforigin:,,,/TroopImages\\Church.png";
                case Gebaeude.Speicher:
                    return "pack://siteoforigin:,,,/TroopImages\\Storage.png";
                case Gebaeude.Bauernhof:
                    return "pack://siteoforigin:,,,/TroopImages\\Farm.png";
                case Gebaeude.Versteck:
                    return "pack://siteoforigin:,,,/TroopImages\\Hide.png";
                case Gebaeude.Wall:
                    return "pack://siteoforigin:,,,/TroopImages\\Wall.png";
                case Gebaeude.WatchTower:
                    return "";
            }
            return "";
        }
    }


    public enum Gebaeude {
        [Description("Hauptgebäude")] Hauptgebaeude = 0,
        [Description("Kaserne")] Kaserne = 1,
        [Description("Stall")] Stall = 2,
        [Description("Werkstatt")] Werkstatt = 3,
        [Description("Kirche")] Kirche = 4,
        [Description("Erste Kirche")] Erstekirche = 5,
        [Description("Adelshof")] Adelshof = 6,
        [Description("Schmiede")] Schmiede = 7,
        [Description("Versammlungsplatz")] Versammlungsplatz = 8,
        [Description("Statue")] Statue = 9,
        [Description("Marktplatz")] Marktplatz = 10,
        [Description("Holzfäller")] Holzfaeller = 11,
        [Description("Lehmgrube")] Lehmgrube = 12,
        [Description("Eisenmine")] Eisenmine = 13,
        [Description("Speicher")] Speicher = 14,
        [Description("Bauernhof")] Bauernhof = 15,
        [Description("Versteck")] Versteck = 16,
        [Description("Wall")] Wall = 17,
        [Description("WatchTower")] WatchTower = 18
    }
}