﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

// ReSharper disable EmptyGeneralCatchClause
#pragma warning disable 168

namespace Ds_Bot_Reunited_NEWUI.BauenModul {
	public class BauenModul {
		private bool _continuewithbuilding = true;
		private string _buildready = "";

		public async Task Bauen(Browser browser, Accountsetting accountsettings) {
			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + Resources.BuildingAccount + " " +
			              Resources.Started);
			if (accountsettings.BauenActive) {
				foreach (var dorf in
					accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) &&
					                                           x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
					await BuildVillage(browser, accountsettings, dorf);
				}

				App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
					               ? accountsettings.Uvname
					               : accountsettings.Accountname) + ": " + Resources.BuildingAccount + " " +
				              Resources.Ended);
				accountsettings.LastBauenZeitpunkt = DateTime.Now;
			}
		}

		public async Task BuildVillage(Browser browser
		                               , Accountsetting accountsettings
		                               , AccountDorfSettings dorf
		                               , Bauelement bauenMust = null) {
			if (dorf.BauenActive && (dorf.Bauschleife.Any() || bauenMust != null)) {
				App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
					               ? accountsettings.Uvname
					               : accountsettings.Accountname) + ": " + Resources.BuildingVillage + ": " +
				              dorf.Dorf.VillageId + " " + Resources.Name + ": " + dorf.Dorf.Name + " " +
				              Resources.Started);
				await App.WaitForAttacks();

				int baueninterval = dorf.Baueninterval;
				DateTime now = DateTime.Now;
				if (dorf.NextAvailableBauschleife < now &&
				    dorf.Dorfinformationen.LastBauenZeitpunkt < now.AddMinutes(-baueninterval) ||
				    accountsettings.MultiaccountingDaten.Accountmode.Equals(Accountmode.Botaccount)) {
					await DorfinformationenModul
					      .DorfinformationenModul.GetDorfinformationenDorfUebersicht(browser, accountsettings, dorf);
					string link = App.Settings.OperateLink +
					              // ReSharper disable once ConditionIsAlwaysTrueOrFalse
					              (dorf != null ? "village=" + dorf.Dorf.VillageId : "") +
					              accountsettings.UvZusatzFuerLink + "&screen=main";
					browser.WebDriver.Navigate().GoToUrl(link);
					browser.WebDriver.WaitForPageload();

					if (!browser.WebDriver.Url.Contains("main")) {
						browser.WebDriver.Navigate().GoToUrl(link);
					}

					await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
					await DorfinformationenModul
					      .DorfinformationenModul.GetDorfinformationenHauptgebaeude(browser, accountsettings, dorf);
					List<IWebElement> elements = browser
					                             .WebDriver
					                             .FindElements(By.XPath(SpielOberflaeche
					                                                    .Hauptgebaeude.BAUSCHLEIFE_SELECTOR_XPATH))
					                             .ToList();
					if (elements.Count > 0) {
						for (int i = 0; i < elements.Count; i++) {
							string[] temp = elements[i].GetAttribute("class").Split('_');
							string building = temp[temp.Length - 1];
							temp = elements[i].Text.Split(' ');
							int level = 0;
							for (int k = 0; k < temp.Length; k++) {
								try {
									level = int.Parse(temp[k]);
									k = temp.Length;
								} catch { }
							}

							switch (building) {
								case "main":
									dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude = level;
									break;
								case "barracks":
									dorf.Dorfinformationen.GebaeudeStufen.Kaserne = level;
									break;
								case "stable":
									dorf.Dorfinformationen.GebaeudeStufen.Stall = level;
									break;
								case "garage":
									dorf.Dorfinformationen.GebaeudeStufen.Werkstatt = level;
									break;
								case "church":
									dorf.Dorfinformationen.GebaeudeStufen.Kirche = level;
									break;
								case "firstchurch":
									dorf.Dorfinformationen.GebaeudeStufen.ErsteKirche = level;
									break;
								case "smith":
									dorf.Dorfinformationen.GebaeudeStufen.Schmiede = level;
									break;
								case "snob":
									dorf.Dorfinformationen.GebaeudeStufen.Adelshof = level;
									break;
								case "statue":
									dorf.Dorfinformationen.GebaeudeStufen.Statue = level;
									break;
								case "place":
									dorf.Dorfinformationen.GebaeudeStufen.Versammlungsplatz = level;
									break;
								case "market":
									dorf.Dorfinformationen.GebaeudeStufen.Marktplatz = level;
									break;
								case "wood":
									dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller = level;
									break;
								case "stone":
									dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube = level;
									break;
								case "iron":
									dorf.Dorfinformationen.GebaeudeStufen.Eisenmine = level;
									break;
								case "farm":
									dorf.Dorfinformationen.GebaeudeStufen.Bauernhof = level;
									break;
								case "storage":
									dorf.Dorfinformationen.GebaeudeStufen.Speicher = level;
									break;
								case "hide":
									dorf.Dorfinformationen.GebaeudeStufen.Versteck = level;
									break;
								case "wall":
									dorf.Dorfinformationen.GebaeudeStufen.Wall = level;
									break;
							}
						}
					}

					if (dorf.BhAutomatischBauenActive &&
					    dorf.Dorfinformationen.FarmMax * 0.8 < dorf.Dorfinformationen.FarmActual) {
						await BuildBuilding(Gebaeude.Bauernhof, dorf.Dorfinformationen.GebaeudeStufen.Bauernhof + 1
						                    , dorf, browser);
					}

					if (dorf.SpeicherAutomatischBauenActive && dorf.Dorfinformationen.StorageMax * 0.8 <
					    dorf.Dorfinformationen.Speicher.Holz ||
					    dorf.Dorfinformationen.StorageMax * 0.8 < dorf.Dorfinformationen.Speicher.Lehm ||
					    dorf.Dorfinformationen.StorageMax * 0.8 < dorf.Dorfinformationen.Speicher.Eisen) {
						await BuildBuilding(Gebaeude.Speicher, dorf.Dorfinformationen.GebaeudeStufen.Speicher + 1, dorf
						                    , browser);
					}

					await Rndm.Sleep(400, 600);
					foreach (Bauelement x in dorf.Bauschleife.OrderBy(x => x.Priority)) {
						await BuildBuildElement(x, browser, dorf, accountsettings);
					}

					if (bauenMust == null) {
						foreach (Bauelement x in dorf.Bauschleife.OrderBy(x => x.Priority)) {
							await BuildBuildElement(x, browser, dorf, accountsettings);
						}
					} else {
						await BuildBuildElement(bauenMust, browser, dorf, accountsettings);
					}

					elements = browser
					           .WebDriver
					           .FindElements(By.XPath(SpielOberflaeche.Hauptgebaeude.BAUSCHLEIFE_SELECTOR_XPATH))
					           .ToList();
					if (elements.Count == App.Settings.MaxBuildingqueue) {
						string[] temp2 = elements[0].Text.Split(' ');
						string time = temp2[temp2.Length - 2];
						int hour = int.Parse(time.Split(':')[0]);
						int minute = int.Parse(time.Split(':')[1]);
						int second = int.Parse(time.Split(':')[2]);
						DateTime dt = DateTime.Now;
						dt = dt.AddHours(hour - DateTime.Now.Hour);
						dt = dt.AddMinutes(minute - DateTime.Now.Minute);
						dt = dt.AddSeconds(second - DateTime.Now.Second);
						if (bauenMust == null) dorf.NextAvailableBauschleife = dt;
					} else if (!_continuewithbuilding) {
						if (_buildready.Contains("-20%")) {
							try {
								string[] temp3 = _buildready.Split('\n');
								string temp = temp3[3];
								DateTime dt;
								if (temp.Contains(NAMECONSTANTS.GetNameForServer(Names.Heute))) {
									string[] temp2 = temp.Split(' ');
									dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day
									                  , int.Parse(temp2[4].Substring(0, 2))
									                  , int.Parse(temp2[4].Substring(3, 2)), 50);
								} else if (temp.Contains(NAMECONSTANTS.GetNameForServer(Names.Morgen))) {
									string[] temp2 = temp.Split(' ');
									dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1
									                  , int.Parse(temp2[4].Substring(0, 2))
									                  , int.Parse(temp2[4].Substring(3, 2))
									                  , int.Parse(temp2[4].Substring(6, 2)));
								} else {
									string[] temp2 = temp.Split(' ');
									dt = new DateTime(DateTime.Now.Year, int.Parse(temp2[3].Substring(3, 2))
									                  , int.Parse(temp2[3].Substring(0, 2))
									                  , int.Parse(temp2[5].Substring(0, 2))
									                  , int.Parse(temp2[5].Substring(3, 2))
									                  , int.Parse(temp2[5].Substring(6, 2)));
								}

								if (bauenMust == null) dorf.NextAvailableBauschleife = dt;
							} catch {
								if (bauenMust == null) dorf.NextAvailableBauschleife = DateTime.Now.AddMinutes(5);
							}
						} else {
							try {
								string[] temp3 = _buildready.Split('\n');
								string temp = temp3[1];
								DateTime dt;
								if (temp.Contains(NAMECONSTANTS.GetNameForServer(Names.Heute))) {
									string[] temp2 = temp.Split(' ');
									dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day
									                  , int.Parse(temp2[11].Substring(0, 2))
									                  , int.Parse(temp2[11].Substring(3, 2)), 50);
								} else if (temp.Contains(NAMECONSTANTS.GetNameForServer(Names.Morgen))) {
									string[] temp2 = temp.Split(' ');
									dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1
									                  , int.Parse(temp2[11].Substring(0, 2))
									                  , int.Parse(temp2[11].Substring(3, 2)), 0);
								} else {
									string[] temp2 = temp.Split(' ');
									dt = new DateTime(DateTime.Now.Year, int.Parse(temp2[10].Substring(3, 2))
									                  , int.Parse(temp2[10].Substring(0, 2))
									                  , int.Parse(temp2[12].Substring(0, 2))
									                  , int.Parse(temp2[12].Substring(3, 2)), 0);
								}

								if (bauenMust == null) dorf.NextAvailableBauschleife = dt;
							} catch {
								if (bauenMust == null) dorf.NextAvailableBauschleife = DateTime.Now.AddMinutes(5);
							}
						}
					} else {
						if (bauenMust == null) dorf.NextAvailableBauschleife = DateTime.Now;
					}
				}

				if (dorf.NextAvailableBauschleife <= DateTime.Now) {
					if (bauenMust == null) dorf.NextAvailableBauschleife = DateTime.Now.AddMinutes(dorf.Baueninterval);
				}

				dorf.Dorfinformationen.LastBauenZeitpunkt = DateTime.Now;
				if (accountsettings.NextAvailableBauschleife <= dorf.NextAvailableBauschleife ||
				    accountsettings.NextAvailableBauschleife == DateTime.MinValue)
					accountsettings.NextAvailableBauschleife = dorf.NextAvailableBauschleife;
				App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
					               ? accountsettings.Uvname
					               : accountsettings.Accountname) + ": " + Resources.BuildingVillage + ": " +
				              dorf.Dorf.VillageId + " " + Resources.FarmlistVillageName + ": " + dorf.Dorf.Name + " " +
				              Resources.Ended);
			}

			await Rndm.Sleep(500, 1000);
		}

		public async Task BuildBuildElement(Bauelement x
		                                    , Browser browser
		                                    , AccountDorfSettings dorf
		                                    , Accountsetting accountsettings) {
			for (int i = dorf.Dorfinformationen.GetGebaeudeStufe(x.Gebaeude); i < x.TargetLevel; i++) {
				if (_continuewithbuilding) {
					Beute benoetigteRessourcen =
						GebaeudeBenoetigteRessourcen.Get(x.Gebaeude
						                                 , dorf.Dorfinformationen.GebaeudeStufen.Stufe(x.Gebaeude) + 1);
					if (dorf.Dorfinformationen.Speicher.Holz - benoetigteRessourcen.Holz > dorf.BauenHolzUebrigLassen &&
					    dorf.Dorfinformationen.Speicher.Lehm - benoetigteRessourcen.Lehm > dorf.BauenLehmUebrigLassen &&
					    dorf.Dorfinformationen.Speicher.Eisen - benoetigteRessourcen.Eisen >
					    dorf.BauenEisenUebrigLassen) {
						int gebaeude = (int) x.Gebaeude;
						if ((x.Gebaeude == Gebaeude.Holzfaeller || x.Gebaeude == Gebaeude.Lehmgrube ||
						     x.Gebaeude == Gebaeude.Eisenmine) && x.TargetLevel > 10) {
							if (accountsettings.MultiaccountingDaten.Accountmode.Equals(Accountmode.Botaccount)) {
								if (dorf.BauenMinenAusgleichen) {
									if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller <
									    dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube &&
									    dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller <
									    dorf.Dorfinformationen.GebaeudeStufen.Eisenmine) {
										gebaeude = 11;
									}

									if (dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube <
									    dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller &&
									    dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube <
									    dorf.Dorfinformationen.GebaeudeStufen.Eisenmine) {
										gebaeude = 12;
									}

									if (dorf.Dorfinformationen.GebaeudeStufen.Eisenmine <
									    dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube &&
									    dorf.Dorfinformationen.GebaeudeStufen.Eisenmine <
									    dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller) {
										gebaeude = 13;
									}
								}
							}
						}

						if (await BuildBuilding((Gebaeude) gebaeude, x.TargetLevel, dorf, browser)) {
							if (App.Settings.Weltensettings.VerkuerzenActive) {
								await CheckforInstantBuild(browser);
							}

							dorf.Dorfinformationen.Speicher.Holz =
								int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
								                                                 .Dorfuebersicht
								                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
							dorf.Dorfinformationen.Speicher.Lehm =
								int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
								                                                 .Dorfuebersicht
								                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
							dorf.Dorfinformationen.Speicher.Eisen =
								int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
								                                                 .Dorfuebersicht
								                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						}
					}
				}
			}
		}

		private async Task<bool> BuildBuilding(Gebaeude buildelement
		                                       , int level
		                                       , AccountDorfSettings dorf
		                                       , Browser browser) {
			if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Hauptgebaeude.BAUSCHLEIFE_SELECTOR_XPATH))
			           .Count < App.Settings.MaxBuildingqueue) {
				if (buildelement == Gebaeude.Hauptgebaeude &&
				    level > dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude) {
					try {
						_continuewithbuilding =
							!browser.WebDriver.FindElement(By.Id("main_buildrow_main")).Text
							        .Contains(NAMECONSTANTS.GetNameForServer(Names.Genug));
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_main")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Hauptgebaeude
					                , dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Kaserne && level > dorf.Dorfinformationen.GebaeudeStufen.Kaserne) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_barracks")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Kaserne, dorf.Dorfinformationen.GebaeudeStufen.Kaserne + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Kaserne++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Stall && level > dorf.Dorfinformationen.GebaeudeStufen.Stall) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_stable")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Stall, dorf.Dorfinformationen.GebaeudeStufen.Stall + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Stall++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Werkstatt &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Werkstatt) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_garage")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Werkstatt, dorf.Dorfinformationen.GebaeudeStufen.Werkstatt + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Werkstatt++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Erstekirche &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.ErsteKirche) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_church_f")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Erstekirche, dorf.Dorfinformationen.GebaeudeStufen.ErsteKirche + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.ErsteKirche++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Kirche && level > dorf.Dorfinformationen.GebaeudeStufen.Kirche) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_church")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Kirche, dorf.Dorfinformationen.GebaeudeStufen.Kirche + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Kirche++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Adelshof &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Adelshof) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_snob")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Adelshof, dorf.Dorfinformationen.GebaeudeStufen.Adelshof + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Adelshof++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Schmiede &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Schmiede) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_smith")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Schmiede, dorf.Dorfinformationen.GebaeudeStufen.Schmiede + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Schmiede++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Statue && level > dorf.Dorfinformationen.GebaeudeStufen.Statue) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_statue")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Statue, dorf.Dorfinformationen.GebaeudeStufen.Statue + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Statue++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Versammlungsplatz &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Versammlungsplatz) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_place")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Versammlungsplatz
					                , dorf.Dorfinformationen.GebaeudeStufen.Versammlungsplatz + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Versammlungsplatz++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Marktplatz &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Marktplatz) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_market")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Marktplatz, dorf.Dorfinformationen.GebaeudeStufen.Marktplatz + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Marktplatz++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Holzfaeller &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_wood")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Holzfaeller, dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Lehmgrube &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_stone")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Lehmgrube, dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Eisenmine &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Eisenmine) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_iron")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Eisenmine, dorf.Dorfinformationen.GebaeudeStufen.Eisenmine + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Eisenmine++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Speicher &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Speicher) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_storage")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Speicher, dorf.Dorfinformationen.GebaeudeStufen.Speicher + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Speicher++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Bauernhof &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Bauernhof) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_farm")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Bauernhof, dorf.Dorfinformationen.GebaeudeStufen.Bauernhof + 1
					                , browser, _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Bauernhof++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Versteck &&
				           level > dorf.Dorfinformationen.GebaeudeStufen.Versteck) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_hide")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Versteck, dorf.Dorfinformationen.GebaeudeStufen.Versteck + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Versteck++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				} else if (buildelement == Gebaeude.Wall && level > dorf.Dorfinformationen.GebaeudeStufen.Wall) {
					try {
						_buildready = browser.WebDriver.FindElement(By.Id("main_buildrow_wall")).Text;
					} catch (Exception e) { }

					if (await Build((int) Gebaeude.Wall, dorf.Dorfinformationen.GebaeudeStufen.Wall + 1, browser
					                , _continuewithbuilding)) {
						dorf.Dorfinformationen.GebaeudeStufen.Wall++;
						dorf.Dorfinformationen.Speicher.Holz =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_HOLZ_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Lehm =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_LEHM_SELECTOR_XPATH)).Text);
						dorf.Dorfinformationen.Speicher.Eisen =
							int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
							                                                 .Dorfuebersicht
							                                                 .SPEICHER_EISEN_SELECTOR_XPATH)).Text);
						return true;
					}

					_continuewithbuilding = false;
				}

				try { } catch (Exception) { }
			} else {
				_continuewithbuilding = false;
			}

			return false;
		}

		private static async Task<bool> Build(int building, int level, Browser browser, bool continuewithbuilding) {
			await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
			string buildingtobuild = "";
			switch (building) {
				case 0:
					buildingtobuild = "main_buildlink_main_" + level;
					break;
				case 1:
					buildingtobuild = "main_buildlink_barracks_" + level;
					break;
				case 2:
					buildingtobuild = "main_buildlink_stable_" + level;
					break;
				case 3:
					buildingtobuild = "main_buildlink_garage_" + level;
					break;
				case 4:
					buildingtobuild = "main_buildlink_church_" + level;
					break;
				case 5:
					buildingtobuild = "main_buildlink_church1_" + level;
					break;
				case 6:
					buildingtobuild = "main_buildlink_snob_" + level;
					break;
				case 7:
					buildingtobuild = "main_buildlink_smith_" + level;
					break;
				case 8:
					buildingtobuild = "main_buildlink_place_" + level;
					break;
				case 9:
					buildingtobuild = "main_buildlink_statue_" + level;
					break;
				case 10:
					buildingtobuild = "main_buildlink_market_" + level;
					break;
				case 11:
					buildingtobuild = "main_buildlink_wood_" + level;
					break;
				case 12:
					buildingtobuild = "main_buildlink_stone_" + level;
					break;
				case 13:
					buildingtobuild = "main_buildlink_iron_" + level;
					break;
				case 14:
					buildingtobuild = "main_buildlink_storage_" + level;
					break;
				case 15:
					buildingtobuild = "main_buildlink_farm_" + level;
					break;
				case 16:
					buildingtobuild = "main_buildlink_hide_" + level;
					break;
				case 17:
					buildingtobuild = "main_buildlink_wall_" + level;
					break;
			}

			if (browser.WebDriver.FindElements(By.Id(buildingtobuild)).Count > 0 && continuewithbuilding) {
				if (browser.WebDriver.FindElement(By.Id(buildingtobuild)).Displayed) {
					browser.ClickByXpath("//a[@id='" + buildingtobuild + "']");
					await Task.Delay(500);
					if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Allgemein.FENSTER_SELECTOR_XPATH))
					           .Count > 0) {
						browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
						                                       .Quest.QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH))
						       .Click();
						await Rndm.Sleep(200, 400);
					}

					App.LogString(Resources.Build + " " + buildingtobuild);
					return true;
				}
			}

			return false;
		}

		private static async Task CheckforInstantBuild(Browser browser) {
			while (browser
			       .WebDriver.FindElements(By.XPath(SpielOberflaeche
			                                        .Hauptgebaeude.HAUPTGEBAEUDE_VERKUERZEN_BUTTON_SELECTOR_XPATH))
			       .Any(x => x.Displayed)) {
				await Rndm.Sleep(200);
				await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
				try {
					browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                       .Hauptgebaeude
					                                       .HAUPTGEBAEUDE_VERKUERZEN_BUTTON_SELECTOR_XPATH)).Click();
				} catch { }

				await Rndm.Sleep(500, 800);
				await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
			}
		}
	}
}