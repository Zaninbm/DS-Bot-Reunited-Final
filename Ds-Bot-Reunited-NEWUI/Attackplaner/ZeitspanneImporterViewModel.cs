﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class ZeitspanneImporterViewModel : INotifyPropertyChanged {
        private DateTime _von;
        private DateTime _bis;
        private string _art;
        private ICommand _importCommand;
        private bool _isSynced;
        public Action CloseAction { get; set; }
        public List<string> Arten { get; set; }

        public bool IsSynced {
            get => _isSynced;
            set {
                _isSynced = value;
                OnPropertyChanged("");
            }
        }

        public string Art {
            get => _art;
            set {
                _art = value;
                OnPropertyChanged();
            }
        }

        public string Von {
            get => _von.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                 CultureInfo.InvariantCulture);
            set {
                try {
                    string date = value.Split('\n')[0];
                    _von = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                } catch {
                    // ignored
                }
                if (IsSynced) {
                    Bis = Von;
                }
                OnPropertyChanged();
            }
        }

        public string Bis {
            get => _bis.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                 CultureInfo.InvariantCulture);
            set {
                try {
                    string date = value.Split('\n')[0];
                    _bis = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                } catch {
                    // ignored
                }
                OnPropertyChanged();
            }
        }

        public ZeitspanneViewModel ZeitSpanneToImport { get; set; }


        public ICommand ImportCommand => _importCommand ?? (_importCommand = new RelayCommand(Import));

        private void Import() {
            if (!string.IsNullOrEmpty(Art) && !string.IsNullOrEmpty(Von) &&
                !string.IsNullOrEmpty(Bis)) {
                ZeitSpanneToImport = new ZeitspanneViewModel(Art, _von, _bis);
            }
            CloseAction();
        }

        public ZeitspanneImporterViewModel() {
            Arten = new List<string> {"Absenden", "Empfangen"};
            Von = DateTime.Now.AddMinutes(30)
                .ToString("dd.MM.yyyy HH:mm:ss.fff",
                    CultureInfo.InvariantCulture);
            Bis = DateTime.Now.AddMinutes(30)
                .ToString("dd.MM.yyyy HH:mm:ss.fff",
                    CultureInfo.InvariantCulture);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}