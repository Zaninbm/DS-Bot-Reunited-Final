﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class DorfKoordinatenImporterViewModel : INotifyPropertyChanged {
        private AsyncObservableCollection<SpielerViewModel> _accountlist;
        private readonly AsyncObservableCollection<DorfViewModel> _villagelistSelectedAccount;
        private string _text;
        private SpielerViewModel _selectedAccount;
        private ICollectionView _villagelistSelectedAccountView;
        private string _importInformationen;
        private ICommand _einlesenCommand;
        private bool _profilIsChecked;
        private bool _bbCodesIsChecked;
        private Window Window { get; set; }
        private ICollectionView _gruppenView;

        public bool BbCodesIsChecked {
            get => _bbCodesIsChecked;
            set {
                _bbCodesIsChecked = value;
                _profilIsChecked = false;
                OnPropertyChanged("");
            }
        }

        public bool ProfilIsChecked {
            get => _profilIsChecked;
            set {
                _bbCodesIsChecked = false;
                _profilIsChecked = value;
                OnPropertyChanged("");
            }
        }

        public AsyncObservableCollection<DorfViewModel> DoerferToImport { get; set; }
        public object DoerferToImportLock { get; set; } = new object();

        public string Text {
            get => _text;
            set {
                _text = value;
                OnPropertyChanged();
            }
        }

        public SpielerViewModel SelectedAccount {
            get => _selectedAccount;
            set {
                _selectedAccount = value;
                _villagelistSelectedAccount.Clear();
                foreach (var dorf in App.VillagelistWholeWorld.Where(x => x.Value.PlayerId == value.PlayerId)) {
                    _villagelistSelectedAccount.Add(dorf.Value);
                }
            }
        }

        public DorfViewModel SelectedVillage { get; set; }

        public AsyncObservableCollection<SpielerViewModel> Accountlist {
            get => _accountlist;
            set {
                _accountlist = value;
                OnPropertyChanged();
            }
        }

        public string ImportInformationen {
            get => _importInformationen;
            set {
                _importInformationen = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView VillagelistSelectedAccount {
            get => _villagelistSelectedAccountView;
            set {
                _villagelistSelectedAccountView = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView GruppenView {
            get => _gruppenView;
            set {
                _gruppenView = value;
                OnPropertyChanged();
            }
        }

        public ICommand EinlesenCommand
            =>
                _einlesenCommand ??
                (_einlesenCommand =
                    new RelayCommand(Einlesen,
                        () =>
                            (BbCodesIsChecked && !string.IsNullOrEmpty(Text)) ||
                            (ProfilIsChecked && !string.IsNullOrEmpty(Text))));


        private async void Einlesen() {
            DoerferToImport.Clear();
            // ReSharper disable once CollectionNeverUpdated.Local
            var doerferToImport = new List<DorfViewModel>();
            await Task.Run(() => {
                if (BbCodesIsChecked) {
                    if (!string.IsNullOrEmpty(Text)) {
                        string[] lines = Text.Split('\n');
                        foreach (var line in lines) {
                            if (line.Contains("Angriff") || line.Contains("Fake")) {
                                int x;
                                int y;
                                string[] splitted = line.Split(' ');
                                string[] coords = splitted[0].TrimEnd(':').Split('|');
                                int.TryParse(coords[0], out x);
                                int.TryParse(coords[1], out y);
                                if (x > 0 && y > 0) {
                                    DorfViewModel dorf =
                                        App.VillagelistWholeWorld.FirstOrDefault(
                                                k => k.Value.XCoordinate.Equals(x) && k.Value.YCoordinate.Equals(y))
                                            .Value;
                                    DoerferToImport.Add(dorf);
                                }
                            }
                        }
                    }
                }
                else if (ProfilIsChecked) {
                    if (!string.IsNullOrEmpty(Text)) {
                        string[] lines = Text.Split('\n');
                        foreach (var line in lines) {
                            if (!line.Contains("Dörfer") && !line.Contains("Koordinaten")) {
                                string[] splitted = line.Trim().Split(' ');
                                string[] coords = splitted[0].Split('|');
                                int x = 0;
                                int y = 0;
                                for (int i = 0; i < 2; i++) {
                                    try {
                                        if (i == 0)
                                            int.TryParse(coords[i], out x);
                                        else
                                            int.TryParse(coords[i], out y);
                                    } catch {
                                        // ignored
                                    }
                                }
                                if (x > 0 && y > 0) {
                                    DorfViewModel dorf =
                                        App.VillagelistWholeWorld.FirstOrDefault(
                                                k => k.Value.XCoordinate.Equals(x) && k.Value.YCoordinate.Equals(y))
                                            .Value;
                                    if (dorf != null) {
                                        DoerferToImport.Add(dorf);
                                    }
                                }
                            }
                        }
                    }
                }
            });
            ImportInformationen = "Dörfer in Liste: " + DoerferToImport.Count;
        }

        public DorfKoordinatenImporterViewModel(Window window) {
            Window = window;
            Accountlist = new AsyncObservableCollection<SpielerViewModel>(App.PlayerlistWholeWorld);
            DoerferToImport = new AsyncObservableCollection<DorfViewModel>();
            _villagelistSelectedAccount = new AsyncObservableCollection<DorfViewModel>();
            VillagelistSelectedAccount = CollectionViewSource.GetDefaultView(_villagelistSelectedAccount);
            BindingOperations.EnableCollectionSynchronization(DoerferToImport,
                DoerferToImportLock);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}