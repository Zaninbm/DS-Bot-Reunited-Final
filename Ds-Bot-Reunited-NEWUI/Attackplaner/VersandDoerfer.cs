﻿using System.Collections.Generic;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class VersandDoerfer {
        public int EmpfaengerVillageId { get; set; }
        public List<PassendeZeitspanne> Zeitspannen { get; set; }
        public Queue<TruppenTemplate> Templates { get; set; }
    }
}