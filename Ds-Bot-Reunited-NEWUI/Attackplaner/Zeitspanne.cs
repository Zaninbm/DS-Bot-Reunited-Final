﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class Zeitspanne {
        public string Zeitart { get; set; }
        public DateTime Von { get; set; }
        public DateTime Bis { get; set; }
        public string Notiz { get; set; }

        public Zeitspanne() { }

        public Zeitspanne(string zeitart, DateTime von, DateTime bis) {
            Zeitart = zeitart;
            Von = von;
            Bis = bis;
            Notiz = zeitart.Equals("Absenden") ? "->" : "<-";
        }
    }
}