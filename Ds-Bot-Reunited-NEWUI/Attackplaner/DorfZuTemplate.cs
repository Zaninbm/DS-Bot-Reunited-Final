﻿using System.Collections.Generic;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class DorfZuTemplate {
        public int TemplatePassReinAmount { get; set; }
        public DorfViewModel Dorf { get; set; }
        public Queue<TruppenTemplate> Templates { get; set; }
    }
}