﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public partial class DorfKoordinatenImporterView {
        public DorfKoordinatenImporterView(Window owner) {
            Owner = owner;
            DataContext = new DorfKoordinatenImporterViewModel(Owner);
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            this.Close();
        }
    }
}