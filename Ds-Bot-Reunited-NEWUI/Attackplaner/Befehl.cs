﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class Befehl {
        public string Befehlsart { get; set; }
        public int EmpfaengerVillageId { get; set; }
        public int SenderVillageId { get; set; }
        public DateTime Ankunftszeitpunkt { get; set; }
        public int SpeertraegerAnzahl { get; set; }
        public int SchwertkaempferAnzahl { get; set; }
        public int AxtkaempferAnzahl { get; set; }
        public int BogenschuetzenAnzahl { get; set; }
        public int SpaeherAnzahl { get; set; }
        public int LeichteKavallerieAnzahl { get; set; }
        public int BeritteneBogenschuetzenAnzahl { get; set; }
        public int SchwereKavallerieAnzahl { get; set; }
        public int RammboeckeAnzahl { get; set; }
        public int KatapulteAnzahl { get; set; }
        public int PaladinAnzahl { get; set; }
        public int AdelsgeschlechtAnzahl { get; set; }

        // ReSharper disable once EmptyConstructor
        public Befehl() { }
    }
}