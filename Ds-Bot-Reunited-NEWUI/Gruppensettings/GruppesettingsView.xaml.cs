﻿using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Gruppensettings {
    /// <summary>
    /// Interaktionslogik für GruppesettingsView.xaml
    /// </summary>
    public partial class GruppesettingsView : UserControl {
        public GruppesettingsView() {
            InitializeComponent();
        }

        void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((GruppesettingsViewModel) DataContext)?.SelectedDoerfer != null) {
                ((GruppesettingsViewModel) DataContext).SelectedDoerfer.Clear();
                foreach (var item in DataGrid.SelectedItems) {
                    ((GruppesettingsViewModel) DataContext).SelectedDoerfer.Add((DorfViewModel) item);
                }
            }
        }
    }
}
