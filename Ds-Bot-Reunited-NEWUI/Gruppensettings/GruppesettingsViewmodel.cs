﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.OwnVillageSelector;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Gruppensettings {
    public class GruppesettingsViewModel : INotifyPropertyChanged {
        private ICollectionView _doerferView;
        private ICommand _removeCommand;
        private ICommand _addCommand;

        public Window Window { get; set; }
        public GruppeDoerfer GruppenSettings { get; set; }

        public string Name {
            get => GruppenSettings.Gruppenname;
            set {
                GruppenSettings.Gruppenname = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<DorfViewModel> Doerfer { get; set; }
        public object DoerferLock { get; set; } = new object();

        public AsyncObservableCollection<DorfViewModel> SelectedDoerfer { get; set; }

        public ICollectionView DoerferView {
            get => _doerferView;
            set {
                _doerferView = value;
                OnPropertyChanged();
            }
        }

        public DorfViewModel SelectedDorf { get; set; }


        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand(async () => { await Remove(); }));
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public GruppesettingsViewModel(GruppeDoerfer gruppe, Window owner) {
            Window = owner;
            GruppenSettings = gruppe;
            Doerfer = new AsyncObservableCollection<DorfViewModel>();
            List<DorfViewModel> doerfer = new List<DorfViewModel>();
            foreach (var dorf in gruppe.DoerferIds) {
                try {
                    var xx = App.VillagelistWholeWorld.FirstOrDefault(x => x.Value.VillageId.Equals(dorf));
                    if (xx.Value != null) {
                        doerfer.Add(xx.Value);
                    }
                } catch {
                    // ignored
                }
            }
            Doerfer = new AsyncObservableCollection<DorfViewModel>(doerfer.OrderBy(x => x.Name));
            BindingOperations.EnableCollectionSynchronization(Doerfer,
                DoerferLock);
            DoerferView = CollectionViewSource.GetDefaultView(Doerfer);
            SelectedDoerfer = new AsyncObservableCollection<DorfViewModel>();
        }

        private async Task Remove() {
            await Task.Run(() => {
                    if (SelectedDoerfer.Any()) {
                        int count = SelectedDoerfer.Count;
                        for (int i = 0; i < count; i++) {
                            GruppenSettings.DoerferIds.Remove(SelectedDoerfer[i].VillageId);
                            Doerfer.Remove(SelectedDoerfer[i]);
                        }
                }
            });
        }

        private void Add() {
            OwnVillageSelectorViewModel vm = new OwnVillageSelectorViewModel(Window);
            OwnVillageSelectorView selector = new OwnVillageSelectorView(Window, vm);
            selector.ShowDialog();
            if (vm.SelectedVillages.Any()) {
                foreach (var dorf in vm.SelectedVillages) {
                    if (!Doerfer.Any(x => x.VillageId.Equals(dorf.VillageId))) {
                        Doerfer.Add(dorf);
                        GruppenSettings.DoerferIds.Add(dorf.VillageId);
                    }
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}