﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Attackplaner;
using Ds_Bot_Reunited_NEWUI.BerichtModul;
using Ds_Bot_Reunited_NEWUI.Botmessages;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Microsoft.Win32;
using Polenter.Serialization;
using Truppenbewegung = Ds_Bot_Reunited_NEWUI.AngreifenModul.Truppenbewegung;

// ReSharper disable UnusedVariable
//773713
namespace Ds_Bot_Reunited_NEWUI.Save {
    public class XmlVerwaltung {
        public static void CheckForSettingsFolder() {
            if (!Directory.Exists("Settings")) Directory.CreateDirectory("Settings");
        }

        public static void CheckForWorlddataFolder() {
            if (!Directory.Exists("Worlddata")) Directory.CreateDirectory("Worlddata");
        }

        public static void XmlSpeichernBotmessages(AsyncObservableCollection<Botmessage> botmessages, int zahl = 0) {
            CheckForSettingsFolder();
            try {
                var serializer = new SharpSerializer(true);
                var farmliste = new AsyncObservableCollection<Botmessage>(botmessages.Select(x => x.Clone()));
                serializer.Serialize(farmliste, "Settings/Botmessages" + (zahl != 0 ? zahl.ToString() : "") + ".xml");
                App.LogString("Botmessages saved!");
            }
            catch {
                App.LogString("ERROR Saving Botmessages, next Try");
            }
        }

        public static AsyncObservableCollection<Botmessage> XmlLadenBotmessages() {
            var farmliste = new AsyncObservableCollection<Botmessage>();
            var serializer = new SharpSerializer(true);
            if (File.Exists("Settings/Botmessages.xml")) {
                try {
                    farmliste =
                        (AsyncObservableCollection<Botmessage>) serializer.Deserialize("Settings/Botmessages.xml");
                }
                catch {
                    farmliste =
                        (AsyncObservableCollection<Botmessage>) serializer.Deserialize("Settings/Botmessages1.xml");
                    XmlSpeichernBotmessages(farmliste);
                }
            }
            else if (File.Exists("Settings/Botmessages1.xml")) {
                farmliste = (AsyncObservableCollection<Botmessage>) serializer.Deserialize("Settings/Botmessages1.xml");
                XmlSpeichernBotmessages(farmliste);
            }
            App.LogString("Botmessages loaded!");
            return farmliste;
        }


        public static void XmlSpeichernFarmliste(AsyncObservableCollection<FarmtargetViewModel> farmlist,
                                                 int zahl = 0) {
            CheckForSettingsFolder();
            try {
                var serializer = new SharpSerializer(true);
                var farmliste = new AsyncObservableCollection<FarmtargetViewModel>(farmlist.Select(x => x.Clone()));
                serializer.Serialize(farmliste, "Settings/Farmlist" + (zahl != 0 ? zahl.ToString() : "") + ".xml");
                App.LogString("Farmlist saved!");
            }
            catch {
                App.LogString("ERROR Saving Farmlist, next Try");
            }
        }

        public static AsyncObservableCollection<FarmtargetViewModel> XmlLadenFarmliste() {
            var farmliste = new AsyncObservableCollection<FarmtargetViewModel>();
            var serializer = new SharpSerializer(true);
            if (File.Exists("Settings/Farmlist.xml")) {
                try {
                    farmliste =
                        (AsyncObservableCollection<FarmtargetViewModel>) serializer
                            .Deserialize("Settings/Farmlist.xml");
                }
                catch {
                    farmliste =
                        (AsyncObservableCollection<FarmtargetViewModel>)
                        serializer.Deserialize("Settings/Farmlist1.xml");
                    XmlSpeichernFarmliste(farmliste);
                }
            }
            else if (File.Exists("Settings/Farmlist1.xml")) {
                farmliste =
                    (AsyncObservableCollection<FarmtargetViewModel>) serializer.Deserialize("Settings/Farmlist1.xml");
                XmlSpeichernFarmliste(farmliste);
            }
            App.LogString("Farmlist loaded!");
            return farmliste;
        }

        public static void XmlSpeichernBerichteliste(AsyncObservableCollection<Bericht> farmlist, int zahl = 0) {
            CheckForSettingsFolder();
            try {
                var serializer = new SharpSerializer(true);
                var farmliste = new AsyncObservableCollection<Bericht>(farmlist.Select(x => x.Clone()));
                serializer.Serialize(farmliste, "Settings/Reportlist" + (zahl != 0 ? zahl.ToString() : "") + ".xml");
                App.LogString("Reportlist saved!");
            }
            catch {
                App.LogString("ERROR Saving Reportlist, next Try");
            }
        }

        public static AsyncObservableCollection<Bericht> XmlLadenBerichteliste() {
            var berichteliste = new AsyncObservableCollection<Bericht>();
            var serializer = new SharpSerializer(true);
            if (File.Exists("Settings/Reportlist.xml")) {
                try {
                    berichteliste =
                        (AsyncObservableCollection<Bericht>) serializer.Deserialize("Settings/Reportlist.xml");
                }
                catch {
                    berichteliste =
                        (AsyncObservableCollection<Bericht>) serializer.Deserialize("Settings/Reportlist1.xml");
                    XmlSpeichernBerichteliste(berichteliste);
                }
            }
            else if (File.Exists("Settings/Reportlist1.xml")) {
                berichteliste = (AsyncObservableCollection<Bericht>) serializer.Deserialize("Settings/Reportlist1.xml");
                XmlSpeichernBerichteliste(berichteliste);
            }
            App.LogString("Farmlist loaded!");
            return berichteliste;
        }


        public static void XmlSpeichernSettings(Settings.Settings settings, int zahl = 0) {
            CheckForSettingsFolder();
            try {
                var serializer = new SharpSerializer(true);
                var settings2 = settings.Clone();
                foreach (var account in settings2.Accountsettingslist) {
                    var xx =
                        settings2.Accountsettingslist.FirstOrDefault(
                                                                     x => !string.IsNullOrEmpty(x.Accountname) &&
                                                                          x.Accountname.Equals(account.Accountname));
                    if (!string.IsNullOrEmpty(account.Passwort) && xx != null) xx.Passwort = Caesar(xx.Passwort, -5);
                }
                serializer.Serialize(settings2, "Settings/Settings" + (zahl != 0 ? zahl.ToString() : "") + ".xml");
                App.LogString("Settings saved!");
            }
            catch (Exception e) {
                App.LogString("ERROR Saving, next Try " + e);
            }
        }

        public static Settings.Settings XmlLadenSettings() {
            var settings = new Settings.Settings();
            var serializer = new SharpSerializer(true);
            if (File.Exists("Settings/Settings.xml")) {
                try {
                    settings = (Settings.Settings) serializer.Deserialize("Settings/Settings.xml");
                    foreach (var account in settings.Accountsettingslist)
                        if (!string.IsNullOrEmpty(account.Passwort)) account.Passwort = Caesar(account.Passwort, 5);
                }
                catch {
                    settings = (Settings.Settings) serializer.Deserialize("Settings/Settings1.xml");
                    foreach (var account in settings.Accountsettingslist)
                        if (!string.IsNullOrEmpty(account.Passwort))
                            account.Passwort = Caesar(account.Passwort, 5);
                    XmlSpeichernSettings(settings);
                }
                XmlSpeichernSettings(settings, 1);
            }
            else if (File.Exists("Settings/Settings1.xml")) {
                settings = (Settings.Settings) serializer.Deserialize("Settings/Settings1.xml");
                foreach (var account in settings.Accountsettingslist)
                    if (!string.IsNullOrEmpty(account.Passwort)) account.Passwort = Caesar(account.Passwort, 5);
                XmlSpeichernSettings(settings);
            }
            if (settings.Accountsettingslist == null)
                settings.Accountsettingslist = new AsyncObservableCollection<Accountsetting>();
            App.LogString("Settings loaded!");
            return settings;
        }

        private static string Caesar(string value, int shift) {
            var buffer = value.ToCharArray();
            for (var i = 0; i < buffer.Length; i++) {
                var letter = buffer[i];
                letter = (char) (letter + shift);
                buffer[i] = letter;
            }
            return new string(buffer);
        }

	    // ReSharper disable once UnusedMember.Global
	    public static void XmlSpeichernAblaufQueue(List<ActionModul.ActionModul> templateList) {
            CheckForSettingsFolder();
            var serializer = new XmlSerializer(typeof(List<ActionModul.ActionModul>));
            using (var writer = new StreamWriter("Settings/AblaufQueue.xml")) {
                serializer.Serialize(writer, templateList);
            }
        }

	    // ReSharper disable once UnusedMember.Global
	    public static List<ActionModul.ActionModul> XmlLadenAblaufQueue() {
            var templateList = new List<ActionModul.ActionModul>();
            var serializer = new XmlSerializer(typeof(List<ActionModul.ActionModul>));
            if (File.Exists("Settings/AblaufQueue.xml"))
                using (TextReader reader = new StreamReader("Settings/AblaufQueue.xml")) {
                    templateList = (List<ActionModul.ActionModul>) serializer.Deserialize(reader);
                }
            return templateList;
        }

        public static void XmlSpeichernWeltdaten(List<DorfViewModel> spielerList, int zahl = 0) {
            CheckForWorlddataFolder();
            try {
                var serializer = new SharpSerializer(true);
                serializer.Serialize(spielerList, "Worlddata/Worlddata" + (zahl != 0 ? zahl.ToString() : "") + ".xml");
                App.LogString("Gamedata saved!");
            }
            catch {
                App.LogString("ERROR Saving Gamedata, next Try");
            }
        }

        public static List<DorfViewModel> XmlLadenWeltdaten() {
            var spielerList = new List<DorfViewModel>();
            var serializer = new SharpSerializer(true);
            if (File.Exists("Worlddata/Worlddata.xml")) {
                try {
                    spielerList = (List<DorfViewModel>) serializer.Deserialize("Worlddata/Worlddata.xml");
                }
                catch {
                    spielerList = (List<DorfViewModel>) serializer.Deserialize("Worlddata/Worlddata1.xml");
                    XmlSpeichernWeltdaten(spielerList);
                }
                XmlSpeichernWeltdaten(spielerList, 1);
            }
            else if (File.Exists("Worlddata/Worlddata1.xml")) {
                spielerList = (List<DorfViewModel>) serializer.Deserialize("Worlddata/Worlddata1.xml");
                XmlSpeichernWeltdaten(spielerList);
            }
            App.LogString("Gamedata loaded!");
            return spielerList;
        }

        //public static void XmlSpeichernDorfdaten(List<DorfViewModel> dorfList) {
        //    var serializer = new SharpSerializer(true);
        //    serializer.Serialize(dorfList, "Dorfdaten.xml");
        //}

        //public static List<DorfViewModel> XmlLadenDorfdaten() {
        //    List<DorfViewModel> dorfList = new List<DorfViewModel>();
        //    var serializer = new SharpSerializer(true);
        //    if (File.Exists("Dorfdaten.xml")) {
        //        dorfList = (List<DorfViewModel>) serializer.Deserialize("Dorfdaten.xml");
        //    }
        //    return dorfList;
        //}

        //public static void XmlSpeichernFarmTruppenTemplates(List<FarmenTruppenTemplate> templateList)
        //{
        //    var serializer = new XmlSerializer(typeof(List<FarmenTruppenTemplate>));
        //    using (var writer = new StreamWriter("FarmenTrooptemplates.xml"))
        //    {
        //        serializer.Serialize(writer, templateList);
        //    }
        //}

        //public static List<FarmenTruppenTemplate> XmlLadenFarmTruppenTemplates()
        //{
        //    List<FarmenTruppenTemplate> templateList = new List<FarmenTruppenTemplate>();
        //    var serializer = new XmlSerializer(typeof(List<FarmenTruppenTemplate>));
        //    if (File.Exists("FarmenTrooptemplates.xml"))
        //    {
        //        using (TextReader reader = new StreamReader("FarmenTrooptemplates.xml"))
        //        {
        //            templateList = (List<FarmenTruppenTemplate>)serializer.Deserialize(reader);
        //        }
        //    }
        //    return templateList;
        //}

        //public static void XmlSpeichernFarmenSettingsTemplates(List<FarmenSettingsTemplate> templateList)
        //{
        //    var serializer = new XmlSerializer(typeof(List<FarmenSettingsTemplate>));
        //    using (var writer = new StreamWriter("FarmenSettingstemplates.xml"))
        //    {
        //        serializer.Serialize(writer, templateList);
        //    }
        //}

        //public static List<FarmenSettingsTemplate> XmlLadenFarmenSettingsTemplates()
        //{
        //    List<FarmenSettingsTemplate> templateList = new List<FarmenSettingsTemplate>();
        //    var serializer = new XmlSerializer(typeof(List<FarmenSettingsTemplate>));
        //    if (File.Exists("FarmenSettingstemplates.xml"))
        //    {
        //        using (TextReader reader = new StreamReader("FarmenSettingstemplates.xml"))
        //        {
        //            templateList = (List<FarmenSettingsTemplate>)serializer.Deserialize(reader);
        //        }
        //    }
        //    return templateList;
        //}

        //public static void XmlSpeichernBauenSettingsTemplates(List<BauenSettingsTemplate> templateList)
        //{
        //    var serializer = new XmlSerializer(typeof(List<BauenSettingsTemplate>));
        //    using (var writer = new StreamWriter("BauenSettingsTemplates.xml"))
        //    {
        //        serializer.Serialize(writer, templateList);
        //    }
        //}

        //public static List<BauenSettingsTemplate> XmlLadenBauenSettingsTemplates()
        //{
        //    List<BauenSettingsTemplate> templateList = new List<BauenSettingsTemplate>();
        //    var serializer = new XmlSerializer(typeof(List<BauenSettingsTemplate>));
        //    if (File.Exists("BauenSettingsTemplates.xml"))
        //    {
        //        using (TextReader reader = new StreamReader("BauenSettingsTemplates.xml"))
        //        {
        //            templateList = (List<BauenSettingsTemplate>)serializer.Deserialize(reader);
        //        }
        //    }
        //    return templateList;
        //}

        //public static void XmlSpeichernRekrutierenSettingsTemplates(List<RekrutierenSettingsTemplate> templateList)
        //{
        //    var serializer = new XmlSerializer(typeof(List<RekrutierenSettingsTemplate>));
        //    using (var writer = new StreamWriter("RekrutierenSettingsTemplates.xml"))
        //    {
        //        serializer.Serialize(writer, templateList);
        //    }
        //}

        //public static List<RekrutierenSettingsTemplate> XmlLadenRekrutierenSettingsTemplates()
        //{
        //    List<RekrutierenSettingsTemplate> templateList = new List<RekrutierenSettingsTemplate>();
        //    var serializer = new XmlSerializer(typeof(List<RekrutierenSettingsTemplate>));
        //    if (File.Exists("RekrutierenSettingsTemplates.xml"))
        //    {
        //        using (TextReader reader = new StreamReader("RekrutierenSettingsTemplates.xml"))
        //        {
        //            templateList = (List<RekrutierenSettingsTemplate>)serializer.Deserialize(reader);
        //        }
        //    }
        //    return templateList;
        //}

        public static async Task<BewegungenPlan> XmlLadenPlan() {
            BewegungenPlan plan = null;
            var serializer = new XmlSerializer(typeof(BewegungenPlan));
            string filename;
            await Task.Run(() => {
                         var filedialog = new OpenFileDialog();
                         filedialog.ShowDialog();
                         filename = filedialog.FileName;
                if (!string.IsNullOrEmpty(filename)) {
                                   using (TextReader reader = new StreamReader(filename)) {
                                       plan = (BewegungenPlan)serializer.Deserialize(reader);
                                   }
                                   plan.FileName = filename;
                               }
            });
            return plan;
        }

        public static void XmlSpeichernPlan(BewegungenPlan plan) {
            var serializer = new XmlSerializer(typeof(BewegungenPlan));
            if (string.IsNullOrEmpty(plan.FileName)) {
                var counter = 0;
                for (var i = 0; i < 30; i++) if (!File.Exists("BewegungenPlan" + i + ".xml")) counter = i;
                plan.FileName = "BewegungenPlan" + counter + ".xml";
            }
            using (var writer = new StreamWriter(plan.FileName)) {
                serializer.Serialize(writer, plan);
            }
        }

	    // ReSharper disable once UnusedMember.Global
	    public static void XmlSpeichernTemplates(List<TruppenTemplate> templateList) {
            CheckForSettingsFolder();
            var serializer = new XmlSerializer(typeof(List<TruppenTemplate>));
            using (var writer = new StreamWriter("Trooptemplates.xml")) {
                serializer.Serialize(writer, templateList);
            }
        }

	    // ReSharper disable once UnusedMember.Global
	    public static List<TruppenTemplate> XmlLadenTemplates() {
            var templateList = new List<TruppenTemplate>();
            var serializer = new XmlSerializer(typeof(List<TruppenTemplate>));
            if (File.Exists("Trooptemplates.xml"))
                using (TextReader reader = new StreamReader("Trooptemplates.xml")) {
                    templateList = (List<TruppenTemplate>) serializer.Deserialize(reader);
                }
            return templateList;
        }

	    // ReSharper disable once UnusedMember.Global
	    public static void XmlSpeichernBewegungen(List<Truppenbewegung> bewegungenList) {
            var templist = bewegungenList.ToList();
            var serializer = new XmlSerializer(typeof(List<Truppenbewegung>));
            using (var writer = new StreamWriter("BewegungenList.xml")) {
                serializer.Serialize(writer, templist);
            }
        }

	    // ReSharper disable once UnusedMember.Global
	    public static List<Truppenbewegung> XmlLadenBewegungen() {
            var bewegungenList = new List<Truppenbewegung>();
            var serializer = new XmlSerializer(typeof(List<Truppenbewegung>));
            if (File.Exists("BewegungenList.xml"))
                using (TextReader reader = new StreamReader("BewegungenList.xml")) {
                    bewegungenList = new List<Truppenbewegung>((List<Truppenbewegung>) serializer.Deserialize(reader));
                }
            return bewegungenList;
        }
    }
}
