﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.ActionModul;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Save;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI {
    [Obfuscation(Exclude = true, Feature = "renaming")]
    public class MainWindowViewModel : INotifyPropertyChanged {
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();
        private string _datum;
        private ICommand _hideBotWindowsCommand;
        private string _uhrzeit;
        private string _zeit;

        public string CurrentVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public MainWindowViewModel(MainWindow window) {
            InitializePermissions();

            App.Window = window;
            if (App.PriorityQueue == null)
                App.PriorityQueue = new AsyncObservableCollection<ActionModulViewModel>();
            App.Settings = XmlVerwaltung.XmlLadenSettings();
            App.Berichte = XmlVerwaltung.XmlLadenBerichteliste();
            App.Farmliste = XmlVerwaltung.XmlLadenFarmliste();
            App.Botmessages = XmlVerwaltung.XmlLadenBotmessages();
            BindingOperations.EnableCollectionSynchronization(App.Berichte, App.BerichteLock);
            BindingOperations.EnableCollectionSynchronization(App.Farmliste, App.FarmlisteLock);
            BindingOperations.EnableCollectionSynchronization(App.Botmessages, App.BotmessagesLock);
            Worlddata.LoadWorldData().GetAwaiter().GetResult();
            DataHandler.GetWorldSettings(App.Settings.Weltensettings).GetAwaiter().GetResult();

            // ReSharper disable once UnusedVariable
            string[] cultures = {"en-US", "en-GB", "fr-FR", "de-DE", "ru-RU"};
            string culturname;
            switch (App.Settings.Sprachversion) {
                case Sprache.Sprache.Deutsch:
                    culturname = "de-DE";
                    break;
                case Sprache.Sprache.English:
                    culturname = "en-US";
                    break;
                default:
                    culturname = "en-US";
                    break;
            }
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culturname);
            Window = window;
            foreach (var farm in App.Farmliste)
                farm.GetsUsed = false;
            Saveloop();
            TimeCycle();
            //App.Settings.LastTimeSynchronisation = DateTime.MinValue;
            App.Settings.TimeDifferenceByMachinePerMinute = 0.0;
            TileView = new TileViewModel {Window = window};
            if (Debugger.IsAttached)
                if (App.Settings.Accountsettingslist.Any(x => x.MultiaccountingDaten.Accountmode == Accountmode.Mainaccount))
                    App.Settings.Accountsettingslist.First(x => x.MultiaccountingDaten.Accountmode == Accountmode.Mainaccount).LastLogins = new ConcurrentBag<DateTime>();
        }

        public string Title2 => "DS-Bot Reunited " + Assembly.GetExecutingAssembly().GetName().Version;

        public ICommand HideBotWindowsCommand => _hideBotWindowsCommand ?? (_hideBotWindowsCommand = new RelayCommand(HideBotWindows, () => App.Permissions.IsBeta));

        public MainWindow Window { get; set; }

        public TileViewModel TileView { get; set; }

        public string Uhrzeit {
            get => _uhrzeit;
            set {
                _uhrzeit = value;
                OnPropertyChanged();
            }
        }

        public string Datum {
            get => _datum;
            set {
                _datum = value;
                OnPropertyChanged();
            }
        }

        public string Zeit {
            get => _zeit;
            set {
                _zeit = value;
                OnPropertyChanged();
            }
        }

        public int PremiumPunkteGesammelt => App.Settings.Accountsettingslist != null && App.Settings.Accountsettingslist.Any() ? App.Settings.Accountsettingslist.Sum(x => x.MultiaccountingDaten.PremiumPunkte) : 0;

        public bool BotWindowsHidden { get; set; } = true;

        public string Accountname => App.Settings.Accountname;

        public event PropertyChangedEventHandler PropertyChanged;

        private void InitializePermissions() {
            App.Permissions.AttackplanerIsEnabled = true;
            App.Permissions.BuildingIsEnabled = true;
            App.Permissions.DefendingIsEnabled = true;
            App.Permissions.FarmingIsEnabled = true;
            App.Permissions.MarketingIsEnabled = true;
            App.Permissions.MultiaccountingIsEnabled = true;
            App.Permissions.PremiumDepotIsEnabled = true;
            App.Permissions.RecruitingIsEnabled = true;
            App.Permissions.SendingIsEnabled = true;
            App.Permissions.IsBeta = true;
        }

        private void HideBotWindows() {
            if (!BotWindowsHidden) {
                if (BrowserManager.BrowserManager.HideBotWindows())
                    BotWindowsHidden = true;
            } else {
                if (BrowserManager.BrowserManager.ShowBotWindows())
                    BotWindowsHidden = false;
            }
        }

        private void TimeCycle() {
            var again = false;
            do {
                var task = new Task(async () => {
                                        for (;;)
                                            try {
                                                Zeit = _stopwatch.Elapsed.TotalMinutes.ToString("0.0");
                            Uhrzeit = DateTime.Now.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
                            Datum = DateTime.Now.ToString("dd.MM.yy", CultureInfo.InvariantCulture);
                            await Task.Delay(1000);
                                            } catch {
                                                // ignored
                                            }
                                        //ReSharper disable once FunctionNeverReturns
                                    }, TaskCreationOptions.LongRunning);
                try {
                    task.Start();
                } catch (Exception e) {
                    App.LogString("Timeloop Error: " + e);
                    again = true;
                }
            } while (again);
        }

        private void Save() {
            if (!App.SaveIsGoingOn) {
                App.SaveIsGoingOn = true;
                XmlVerwaltung.XmlSpeichernSettings(App.Settings.Clone(), 1);
                XmlVerwaltung.XmlSpeichernSettings(App.Settings.Clone());
                XmlVerwaltung.XmlSpeichernFarmliste(App.Farmliste, 1);
                XmlVerwaltung.XmlSpeichernFarmliste(App.Farmliste);
                XmlVerwaltung.XmlSpeichernBerichteliste(App.Berichte, 1);
                XmlVerwaltung.XmlSpeichernBerichteliste(App.Berichte);
                XmlVerwaltung.XmlSpeichernBotmessages(App.Botmessages, 1);
                XmlVerwaltung.XmlSpeichernBotmessages(App.Botmessages);
                if (App.VillagelistWholeWorld != null && App.VillagelistWholeWorld.Any()) {
                    XmlVerwaltung.XmlSpeichernWeltdaten(App.VillagelistWholeWorld.Select(x => x.Value).ToList(), 1);
                    XmlVerwaltung.XmlSpeichernWeltdaten(App.VillagelistWholeWorld.Select(x => x.Value).ToList());
                }
                App.SaveIsGoingOn = false;
            }
        }

        public void CloseAction() {
            Save();
            App.Active = false;
            BrowserManager.BrowserManager.DestroyAll();
            App.NotificationIcon.Visible = false;
            App.NotificationIcon.Dispose();
        }

        private void Saveloop() {
            var again = false;
            do {
                var task = new Task(async () => {
                                        for (;;) {
                                            try {
                                                Save();
                                                await Task.Delay(300000);
                                            } catch (Exception e) {
                                                App.LogString(Resources.SavingError + " " + e);
                                            }
                                        }
                                        // ReSharper disable once FunctionNeverReturns
                                    }, TaskCreationOptions.LongRunning);
                try {
                    task.Start();
                } catch (Exception e) {
                    App.LogString("Saveloop Error: " + e);
                    again = true;
                }
            } while (again);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
