﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;

namespace Ds_Bot_Reunited_NEWUI.Farmen {
    [Serializable]
    public class Beute : INotifyPropertyChanged {
        private int _holz;
        private int _eisen;
        private int _lehm;

        public int Holz {
            get => _holz;
            set {
                _holz = value;
                OnPropertyChanged();
            }
        }

        public int Lehm {
            get => _lehm;
            set {
                _lehm = value;
                OnPropertyChanged();
            }
        }

        public int Eisen {
            get => _eisen;
            set {
                _eisen = value;
                OnPropertyChanged();
            }
        }

        public int Gesamt => Holz + Lehm + Eisen;

        public Beute() { }

        public Beute(int holz, int lehm, int eisen) {
            Holz = holz;
            Lehm = lehm;
            Eisen = eisen;
        }

        public Beute Clone() {
            Beute newBeute = new Beute {
                Eisen = Eisen,
                Holz = Holz,
                Lehm = Lehm
            };
            return newBeute;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}