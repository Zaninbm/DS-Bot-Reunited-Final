﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Farmliste {
    public static class FarmlistenVerwaltung {
        public static void RemoveFromFarmliste(FarmtargetViewModel farm) {
            App.Farmliste.Remove(farm);
        }

        public static void Refresh() {
            foreach (var farm in App.Farmliste.Where(x => x.SetActiveAfter != DateTime.MinValue || x.SetInactiveAfter != DateTime.MinValue)) {
                if (farm.SetInactiveAfter != DateTime.MinValue && farm.SetInactiveAfter < DateTime.Now) {
                    farm.Active = false;
                    farm.SetInactiveAfter = DateTime.MinValue;
                }
                if (farm.SetActiveAfter != DateTime.MinValue && farm.SetActiveAfter < DateTime.Now) {
                    farm.Active = true;
                    farm.SetActiveAfter = DateTime.MinValue;
                }
            }
        }

        public static void AddToFarmliste(int zielXCoordinate, int zielYCoordinate, Gebaeude gebaeude, TruppenTemplate erspaehteTruppenAußerhalb, TruppenTemplate erspaehteTruppen, DorfViewModel dorf = null) {
            DorfViewModel ausgewaehltesDorf;
            if (dorf != null)
                ausgewaehltesDorf = dorf;
            else {
                var searchTuple = new VillageTuple<int, int>(zielXCoordinate, zielYCoordinate);
                ausgewaehltesDorf = App.VillagelistWholeWorld[searchTuple];
            }
            if (ausgewaehltesDorf != null) {
                var ausgewaehlterSpieler = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerId.Equals(ausgewaehltesDorf.PlayerId));
                App.Farmliste.Add(new FarmtargetViewModel {
                                                              Active = true,
                                                              Dorf = ausgewaehltesDorf.Dorf,
                                                              ErbeutetGesamt = new Beute(),
                                                              Erfolgsquotient = 100,
                                                              IsSelected = false,
                                                              VillageId = ausgewaehltesDorf.VillageId,
                                                              Spieler = ausgewaehlterSpieler != null ? ausgewaehlterSpieler.Spieler : new Spieler {PlayerId = 0},
                                                              PlayerId = ausgewaehlterSpieler?.PlayerId ?? 0,
                                                              Gebaeude = gebaeude ?? new Gebaeude(),
                                                              VorhandeneTruppen = erspaehteTruppen ?? new TruppenTemplate(),
                                                              AusserhalbTruppen = erspaehteTruppenAußerhalb ?? new TruppenTemplate()
                                                          });
            }
        }

        public static void AddAllVillagesToFarmliste(int zielXCoordinate, int zielYCoordinate, Gebaeude gebaeude, TruppenTemplate erspaehteTruppenAußerhalb, TruppenTemplate erspaehteTruppen, DorfViewModel dorf = null) {
            DorfViewModel ausgewaehltesDorf;
            if (dorf != null)
                ausgewaehltesDorf = dorf;
            else {
                var searchTuple = new VillageTuple<int, int>(zielXCoordinate, zielYCoordinate);
                ausgewaehltesDorf = App.VillagelistWholeWorld[searchTuple];
            }
            if (ausgewaehltesDorf != null)
                App.Farmliste.Add(new FarmtargetViewModel(dorf, gebaeude, erspaehteTruppenAußerhalb, erspaehteTruppen));
        }

        public static List<DorfViewModel> GetFarmableVillagesList(double maxentfernung,
                                                                  int xcoord,
                                                                  int ycoord,
                                                                  int minpoints = 0,
                                                                  int maxpoints = 12600,
                                                                  bool players = false,
                                                                  int minplayerpoints = 0,
                                                                  int maxplayerpoints = 0,
                                                                  int minafkdays = 0,
                                                                  double minentfernung = 0) {
            var farmableVillages = new List<DorfViewModel>();
            var x = xcoord;
            var y = ycoord;
            foreach (var village in App.VillagelistWholeWorld)
                if (!App.Farmliste.Any(k => k.VillageId.Equals(village.Value.VillageId))) {
                    var weitermachen = false;
                    if (players && !village.Value.PlayerId.Equals(0) && village.Value.VillagePoints > minplayerpoints && village.Value.VillagePoints < maxplayerpoints && village.Value.Dorf.History.Any()) {
                        if (village.Value.Dorf.History.Last().Zeitpunkt <= DateTime.Now.AddDays(-minafkdays))
                            weitermachen = true;
                    } else if (village.Value.PlayerId.Equals(0))
                        weitermachen = true;
                    if (weitermachen && village.Value.VillagePoints > minpoints && village.Value.VillagePoints < maxpoints) {
                        var entfernung = Math.Sqrt((x - village.Value.XCoordinate) * (x - village.Value.XCoordinate) + (y - village.Value.YCoordinate) * (y - village.Value.YCoordinate));
                        if (entfernung < maxentfernung && entfernung > minentfernung && village.Value.PlayerId.Equals(0) && !App.Farmliste.Any(yx => yx.VillageId.Equals(village.Value.VillageId)))
                            farmableVillages.Add(village.Value);
                    }
                }
            return farmableVillages;
        }
    }
}
