﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Framework.UI.Controls;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.LoginView {
	public class LoginViewModel : INotifyPropertyChanged {
		public Window Window { get; set; }

		private ICommand _loginCommand;
		private ICommand _cancelCommand;
		private ICommand _buyCommand;
		private string _oldSerialKey;

		public string SerialKey { get; set; }

		public string OldSerialKey {
			get => _oldSerialKey;
			set {
				_oldSerialKey = value;
				OnPropertyChanged();
			}
		}

		public ICommand LoginCommand => _loginCommand ?? (_loginCommand = new RelayCommand(DoLogin));
		public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));
		public ICommand BuyCommand => _buyCommand ?? (_buyCommand = new RelayCommand(Buy));

		public LoginViewModel(Window window) {
			Window = window;
			MessageBox.Show("Please insert a valid Serialkey for Activation", "",
			                MessageBoxButton.OK);
		}

		private async void Cancel() {
			if (await MessageDialog.ShowAsync("Close the App?", "",
			                                  MessageBoxButton.YesNo,
			                                  MessageDialogType.Accent,
			                                  App.Window) == MessageBoxResult.Yes) {
				Environment.Exit(0);
			}
		}

		private async void Buy() {
			if (await MessageDialog.ShowAsync("Open your Browser to buy a Key?", "",
			                                  MessageBoxButton.YesNo,
			                                  MessageDialogType.Accent,
			                                  App.Window) == MessageBoxResult.Yes) {
				Process.Start("");
			}
		}

		private async void DoLogin() {
			if (!string.IsNullOrEmpty(SerialKey)) {
				try {
					await Task.Run(async () => {
					               });
				} catch { }

				await MessageDialog.ShowAsync("Activated!", "",
				                              MessageBoxButton.OK,
				                              MessageDialogType.Accent,
				                              App.Window);
				Window.Close();
			} else {
				await MessageDialog.ShowAsync("No Serialkey!", "",
				                              MessageBoxButton.OK,
				                              MessageDialogType.Accent,
				                              App.Window);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}