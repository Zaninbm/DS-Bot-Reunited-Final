﻿using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.BauensettingsDorf;

namespace Ds_Bot_Reunited_NEWUI.Bauensetting {
    public partial class BauensettingsDorfView : UserControl {
        public BauensettingsDorfView() {
            InitializeComponent();
        }


        private void Gebaeude_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((BauensettingsDorfViewModel) DataContext)?.SelectedBauschleife != null) {
                ((BauensettingsDorfViewModel) DataContext).SelectedBauschleife.Clear();
                foreach (var item in Bauschleife.SelectedItems)
                    ((BauensettingsDorfViewModel) DataContext).SelectedBauschleife.Add((Bauelement) item);
            }
        }
    }
}
