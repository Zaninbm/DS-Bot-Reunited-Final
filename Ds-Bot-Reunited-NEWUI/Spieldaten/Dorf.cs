﻿using System;
using System.Collections.Generic;

namespace Ds_Bot_Reunited_NEWUI.Spieldaten {
    [Serializable]
    public class Dorf {
        public int VillageId { get; set; }
        public string VillageName { get; set; }
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }
        public int VillagePoints { get; set; }
        public List<PunkteHistorie> History { get; set; }
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }

        //ReSharper disable once EmptyConstructor         
        public Dorf() {
            History = new List<PunkteHistorie>();
        }

        public Dorf Clone() {
            Dorf newDorf = new Dorf {
                PlayerId = PlayerId,
                VillageId = VillageId,
                VillageName = VillageName,
                YCoordinate = YCoordinate,
                XCoordinate = XCoordinate,
                VillagePoints = VillagePoints,
                History = new List<PunkteHistorie>(),
                PlayerName = PlayerName
            };
            foreach (var innn in History) {
                newDorf.History.Add(innn.Clone());
            }
            return newDorf;
        }
    }
}