﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;

namespace Ds_Bot_Reunited_NEWUI.Botsettings {
	public class BotsettingsViewModel : INotifyPropertyChanged {
		private string _loginStatus;
		private ICommand _resetSynchronizeCommand;
		public Window Window { get; set; }
		public AsyncObservableCollection<string> TroopsAvailable { get; set; }

		public bool MinimizeToTray {
			get => App.Settings.MinimizeToTray;
			set {
				App.Settings.MinimizeToTray = value;
				OnPropertyChanged();
			}
		}

		public bool MultiaccountingActive {
			get => App.Settings.MultiaccountingActive;
			set {
				App.Settings.MultiaccountingActive = value;
				OnPropertyChanged();
			}
		}

		public bool EinzelaccountingActive {
			get => App.Settings.EinzelaccountingActive;
			set {
				App.Settings.EinzelaccountingActive = value;
				OnPropertyChanged();
			}
		}

		public Botmode.Botmode Botmode {
			get => App.Settings.Botmode;
			set {
				App.Settings.Botmode = value;
				OnPropertyChanged();
			}
		}

		public int MaxConcurrentWindowsOpen {
			get => App.Settings.MaxConcurrentWindowsOpen;
			set {
				App.Settings.MaxConcurrentWindowsOpen = value;
				OnPropertyChanged();
			}
		}

		public int Difference {
			get => App.Settings.Difference;
			set {
				App.Settings.Difference = value;
				OnPropertyChanged();
			}
		}

		public string LoginStatus {
			get => _loginStatus;
			set {
				_loginStatus = value;
				OnPropertyChanged();
			}
		}

		public int MinActionDelay {
			get => App.Settings.MinActionDelay;
			set {
				App.Settings.MinActionDelay = value;
				OnPropertyChanged();
			}
		}

		public int MaxActionDelay {
			get => App.Settings.MaxActionDelay;
			set {
				App.Settings.MaxActionDelay = value;
				OnPropertyChanged();
			}
		}

		public int RandomIntervalMax {
			get => App.Settings.RandomIntervalMax;
			set {
				App.Settings.RandomIntervalMax = value;
				OnPropertyChanged();
			}
		}

		public int RandomIntervalMin {
			get => App.Settings.RandomIntervalMin;
			set {
				App.Settings.RandomIntervalMin = value;
				OnPropertyChanged();
			}
		}

		public string TwoCaptchaApiKey {
			get => App.Settings.TwoCaptchaApiKey;
			set {
				App.Settings.TwoCaptchaApiKey = value;
				OnPropertyChanged();
			}
		}

        public string SubscriptionStatus => "";

		public Sprache.Sprache Sprache {
			get => App.Settings.Sprachversion;
			set {
				App.Settings.Sprachversion = value;
				OnPropertyChanged();
			}
		}

		public string UseTroopForSynchronize {
			get => App.Settings.UseTroopForSynchronize;
			set {
				App.Settings.UseTroopForSynchronize = value;
				OnPropertyChanged();
			}
		}

		public int SynchronizeTroopCount {
			get => App.Settings.SynchronizeTroopCount;
			set {
				App.Settings.SynchronizeTroopCount = value;
				OnPropertyChanged();
			}
		}

		public int MaxBuildingqueue {
			get => App.Settings.MaxBuildingqueue;
			set {
				App.Settings.MaxBuildingqueue = value;
				OnPropertyChanged();
			}
		}

		public double TimeDifferenceByMachinePerMinute {
			get => App.Settings.TimeDifferenceByMachinePerMinute;
			set {
				App.Settings.TimeDifferenceByMachinePerMinute = value;
				OnPropertyChanged();
			}
		}

		public ICommand ResetSynchronizeCommand =>
			_resetSynchronizeCommand ?? (_resetSynchronizeCommand = new RelayCommand(ResetSynchronize));

		public BotsettingsViewModel(Window window) {
			Window = window;
			TroopsAvailable = new AsyncObservableCollection<string> {
				                                                        Truppeb.Speertraeger
				                                                        , Truppeb.Schwertkaempfer
				                                                        , Truppeb.Axtkaempfer
				                                                        , Truppeb.Bogenschuetzen
				                                                        , Truppeb.Spaeher
				                                                        , Truppeb.LeichteKavallerie
				                                                        , Truppeb.BeritteneBogenschuetzen
				                                                        , Truppeb.SchwereKavallerie
				                                                        , Truppeb.Rammboecke
				                                                        , Truppeb.Katapulte
			                                                        };
		}

		private void ResetSynchronize() {
			App.Settings.LastTimeSynchronisation = DateTime.MinValue;
			App.Settings.Difference = 0;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}