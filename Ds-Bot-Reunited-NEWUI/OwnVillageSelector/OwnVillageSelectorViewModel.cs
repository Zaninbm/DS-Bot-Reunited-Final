﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.OwnVillageSelector {
    public class OwnVillageSelectorViewModel : INotifyPropertyChanged {
        public System.Windows.Window Window { get; set; }
        private DorfViewModel _selectedVillage;
        private ICollectionView _villages;
        private GruppeDoerfer _selectedGruppe;
        private bool _progressRingActive;
        private readonly bool _selectFirstVillage;
		
        public AsyncObservableCollection<DorfViewModel> SelectedVillages { get; set; }

        public AsyncObservableCollection<DorfViewModel> Villagess { get; set; }
        public List<GruppeDoerfer> Groups => App.Settings.Gruppen;
        public object VillagessLock { get; set; } = new object();

        public ICollectionView Villages {
            get => _villages;
            set {
                _villages = value;
                OnPropertyChanged();
            }
        }

        public GruppeDoerfer SelectedGruppe {
            get => _selectedGruppe;
            set {
                _selectedGruppe = value;
#pragma warning disable CS4014 // Da dieser Aufruf nicht abgewartet wird, wird die Ausführung der aktuellen Methode fortgesetzt, bevor der Aufruf abgeschlossen ist
                LoadPlayerVillages();
#pragma warning restore CS4014 // Da dieser Aufruf nicht abgewartet wird, wird die Ausführung der aktuellen Methode fortgesetzt, bevor der Aufruf abgeschlossen ist
                OnPropertyChanged("");
            }
        }

        public bool ProgressRingActive {
            get => _progressRingActive;
            set {
                _progressRingActive = value;
                OnPropertyChanged();
            }
        }

        private async Task LoadPlayerVillages() {
            await Task.Run(() => {
                               ProgressRingActive = true;
                               try {
                                   Villagess.Clear();
                                   foreach (var liste in App.Settings.Accountsettingslist.Select(x => x.DoerferSettings.Where(y => y.Dorf.PlayerId.Equals(x.Playerid) && (SelectedGruppe?.DoerferIds.Any(k => k.Equals(y.Dorf.VillageId)) ?? true)))) {
                                       foreach (var dorf in liste.OrderBy(x => x.Dorf.Name)) {
                                           Villagess.Add(dorf.Dorf);
                                       }
                                   }
                               } catch {
                                       App.LogString(Resources.FailureLoadingPlayervillages);
                               }
                               if (_selectFirstVillage && Villagess.Any())
                                   SelectedVillage = Villagess.FirstOrDefault();
                               ProgressRingActive = false;
                           });
        }

        public DorfViewModel SelectedVillage {
            get => _selectedVillage;
            set {
                _selectedVillage = value;
                OnPropertyChanged();
            }
        }

        public OwnVillageSelectorViewModel(System.Windows.Window window, bool selectFirstVillage = false) {
            _selectFirstVillage = selectFirstVillage;
            Villagess = new AsyncObservableCollection<DorfViewModel>();
            Villages = CollectionViewSource.GetDefaultView(Villagess);
            SelectedVillages = new AsyncObservableCollection<DorfViewModel>();
            BindingOperations.EnableCollectionSynchronization(Villagess, VillagessLock);
            SelectedGruppe = Groups.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
