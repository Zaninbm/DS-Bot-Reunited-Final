﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Nachrichten {
    [Serializable]
    public class Nachricht {
        public string Betreff { get; set; }
        public string Id { get; set; }
        public string NachrichtPartner { get; set; }
        public string Antwort { get; set; }
        public string Accountname { get; set; }

        public Nachricht() { }

        public Nachricht Clone() {
            Nachricht newNachricht = new Nachricht {
                Betreff = Betreff,
                Antwort = Antwort,
                Id = Id,
                NachrichtPartner = NachrichtPartner,
                Accountname = Accountname
            };
            return newNachricht;
        }
    }
}