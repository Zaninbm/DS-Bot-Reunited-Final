﻿using System;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.TimeModul {
    public class TimeModulAttackDetection {
        public static string GetSlowestUnitOfAttack(DorfViewModel sender, DorfViewModel empfaenger, DateTime ankunft) {
            double unterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {AdelsgeschlechterAnzahl = 1})) - ankunft).TotalMinutes;
            double actualunterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {RammboeckeAnzahl = 1})) - ankunft).TotalMinutes;
            var unit = Resources.Snob;
            if (actualunterschied < unterschied && actualunterschied > 0) {
                unterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {RammboeckeAnzahl = 1})) - ankunft).TotalMinutes;
                unit = Resources.Ram;
            }
            actualunterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SchwertkaempferAnzahl = 1})) - ankunft).TotalMinutes;
            if (actualunterschied < unterschied && actualunterschied > 0) {
                unterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SchwertkaempferAnzahl = 1})) - ankunft).TotalMinutes;
                unit = Resources.Sword;
            }
            actualunterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SpeertraegerAnzahl = 1})) - ankunft).TotalMinutes;
            if (actualunterschied < unterschied && actualunterschied > 0) {
                unterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SpeertraegerAnzahl = 1})) - ankunft).TotalMinutes;
                unit = Resources.Spear;
            }
            actualunterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SchwereKavallerieAnzahl = 1})) - ankunft).TotalMinutes;
            if (actualunterschied < unterschied && actualunterschied > 0) {
                unterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SchwereKavallerieAnzahl = 1})) - ankunft).TotalMinutes;
                unit = Resources.Heavy;
            }
            actualunterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {BeritteneBogenschuetzenAnzahl = 1})) - ankunft).TotalMinutes;
            if (actualunterschied < unterschied && actualunterschied > 0) {
                unterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {BeritteneBogenschuetzenAnzahl = 1})) - ankunft).TotalMinutes;
                unit = Resources.Marcher;
            }
            actualunterschied = (DateTime.Now.Add(Laufzeit.GetTravelTime(sender.Dorf, empfaenger.Dorf, new TruppenTemplate {SpaeherAnzahl = 1})) - ankunft).TotalMinutes;
            if (actualunterschied < unterschied && actualunterschied > 0) {
                unit = Resources.Spy;
            }
            return unit;
        }

        public static DateTime GetTimeOfString(string timeString) {
            DateTime time;
            string[] temp2 = timeString.Split(' ');
            if (timeString.Contains(NAMECONSTANTS.GetNameForServer(Names.Heute))) {
                time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, int.Parse(temp2[2].Substring(0, 2)), int.Parse(temp2[2].Substring(3, 2)), int.Parse(temp2[2].Substring(6, 2)));
                if (App.Settings.Weltensettings.MillisecondsActive)
                    time = time.AddMilliseconds(int.Parse(temp2[2].Substring(9, 3)));
            } else if (timeString.Contains(NAMECONSTANTS.GetNameForServer(Names.Morgen))) {
                time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1, int.Parse(temp2[2].Substring(0, 2)), int.Parse(temp2[2].Substring(3, 2)), int.Parse(temp2[2].Substring(6, 2)));
                if (App.Settings.Weltensettings.MillisecondsActive)
                    time = time.AddMilliseconds(int.Parse(temp2[2].Substring(9, 3)));
            } else {
                time = new DateTime(DateTime.Now.Year, int.Parse(temp2[1].Substring(3, 2)), int.Parse(temp2[1].Substring(0, 2)), int.Parse(temp2[3].Substring(0, 2)), int.Parse(temp2[3].Substring(3, 2)), int.Parse(temp2[3].Substring(6, 2)));
                if (App.Settings.Weltensettings.MillisecondsActive)
                    time = time.AddMilliseconds(int.Parse(temp2[3].Substring(9, 3)));
            }
            return time;
        }
    }
}
