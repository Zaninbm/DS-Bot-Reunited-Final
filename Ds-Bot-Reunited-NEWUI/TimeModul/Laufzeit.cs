﻿using System;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.TimeModul {
    public static class Laufzeit {
        public static TimeSpan GetTravelTime(Dorf dorfeins, Dorf dorfzwei, TruppenTemplate template) {
            if (dorfzwei != null) {
                double felder = Felder(dorfeins, dorfzwei);
                var minutes = felder * GetSlowestLaufzeit(template);
                var hours = (int) minutes / 60;
                minutes = minutes - hours * 60;
                var minutestotal = (int) minutes;
                minutes = minutes - minutestotal;
                var seconds = (int) Math.Round(minutes * 60, 0, MidpointRounding.ToEven); // ReSharper disable once PossibleLossOfFraction   
                minutes = minutes - seconds / 60;
                var millis = (int) minutes * 1000;
                var ts = new TimeSpan(0, hours, minutestotal, seconds, millis);
                return ts;
            }
            return new TimeSpan();
        }

        public static double Felder(Dorf dorfeins, Dorf dorfzwei) {
            try {
                return Math.Sqrt((dorfeins.XCoordinate - dorfzwei.XCoordinate) * (dorfeins.XCoordinate - dorfzwei.XCoordinate) + (dorfeins.YCoordinate - dorfzwei.YCoordinate) * (dorfeins.YCoordinate - dorfzwei.YCoordinate));
            } catch {
                return 0;
            }
        }

        private static double GetSlowestLaufzeit(TruppenTemplate template) {
            if (template.AdelsgeschlechterAnzahl > 0)
                return App.Settings.Weltensettings.AdelsgeschlechtLaufzeit;
            if (template.KatapulteAnzahl > 0 || template.RammboeckeAnzahl > 0)
                return App.Settings.Weltensettings.RammboeckeLaufzeit;
            if (template.SchwertkaempferAnzahl > 0)
                return App.Settings.Weltensettings.SchwertkaempferLaufzeit;
            if (template.SpeertraegerAnzahl > 0 || template.AxtkaempferAnzahl > 0 || template.BogenschuetzenAnzahl > 0)
                return App.Settings.Weltensettings.SpeertraegerLaufzeit;
            if (template.SchwereKavallerieAnzahl > 0)
                return App.Settings.Weltensettings.SchwereKavallerieLaufzeit;
            if (template.BeritteneBogenschuetzenAnzahl > 0 || template.LeichteKavallerieAnzahl > 0)
                return App.Settings.Weltensettings.LeichteKavallerieLaufzeit;
            if (template.SpaeherAnzahl > 0)
                return App.Settings.Weltensettings.SpaeherLaufzeit;
            return -1;
        }
    }
}